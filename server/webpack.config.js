'use strict';

const webpack = require('webpack');

let externals = _externals();

module.exports = {
    entry: ['babel-polyfill', './index.js'],
    target: 'node',
    output: {
        filename: './task.js'
    },
    resolve: {
        extensions: ['err', '.js']
    },
    externals: externals,
    node: {
        console: true,
        global: true,
        process: true,
        Buffer: true,
        __filename: true,
        __dirname: true,
        setImmediate: true
    },
    module: {
        loaders: [
            {
                test: /.js$/,
                loader: 'babel-loader',
                  query: {
                     presets: ['env','stage-2']
                 },
                exclude: /node_modules/
            }
        ]
    },
       plugins: [
         new webpack.optimize.UglifyJsPlugin()
     ]  
};

function _externals() {
    let manifest = require('./package.json');
    let dependencies = manifest.dependencies;
    let externals = {};
    for (let p in dependencies) {
        externals[p] = 'commonjs ' + p;
    }
    return externals;
}