
const express = require('express');
const app = express();
const path = require('path');
const fs = require('fs');
const config = eval(fs.readFileSync("./config/config.js",'utf8'));
const http = require('http').Server(app);
const io = require('socket.io')(http);
module.exports = {
    'io': io,
    'config':config
};
const API = require('./API');
//打开数据库连接,同时启动log日志
require('./common/db');
const log = require('./common/log');
//初始化数据库（第一次部署时）
require('./common/init').initialize();
//加载API事件
require('./common/event').eventListent();
//加载schedule列队
require('./common/schedule').interval();
//加载API
app.use(API.middleware);
app.use('/api/internal', API.commonAPI);
app.use('/api/user',API.userAPI);
app.use('/api/project',API.projectAPI);
app.use('/api/task',API.taskAPI);
app.use('/api/chat',API.chatAPI);
app.use('/api/role',API.roleAPI);
app.use('/api/user',API.userAPI);

//将页面文件进行静态路由处理
app.use(express.static(path.resolve(__dirname, './dist')));
//静态数据、图片存储文件呈递
app.use('/storage', express.static(path.resolve(__dirname, './storage')))

//直接访问服务器端口，默认跳转至前台登录页面
app.get('/', (req, res, next) => {
    res.redirect('/front');
});


// 前台页面front请求路由,返回单页应用index.html
app.get('/front', (req, res, next) => {
    const html = fs.readFileSync(path.resolve(__dirname, './dist/index.html'), 'utf-8');
    res.send(html)
});

//404捕获
app.use((req, res, next) => {
    var err = new Error('404,没有这个页面:' + req.path);
    err.status = 404;
    next(err);
});

//错误处理
app.use((err, req, res, next) => {
    if(res.status === 500) console.error(err);
    log.custom('operate-error','用户操作错误：',err.message)
    if (err.status == 301) {
        return res.send('authority fiald please redirect to index:'+err.message);
    }
    res.status(err.status || 210);
    res.send({message:err.message,type:"error"});
});



let port = config.port;
//服务器总端口
http.listen(port, (err) => {
    log.custom('launch', "服务器启动，监听：", port);
    log.custom('launch', "日志启动，sockect监听：", port+'/log');
});