# task_server

一个使用nodejs+mongodb+mongoosejs+expressjs实现的task任务管理系统后端服务程序，使用forever进行程序启动管理。

### 1. 依赖

nodejs: v8.3或更高；

mongodb:v3.2或更高；

### 2. 目录文件

```tex
	——actions       //业务控制
	——API      		//api控制
  	——common      	//其他脚本文件
	——config	    //配置文件
	——dist      	//前端静态文
	——models		//mongoose schema文件
	——test			//测试用脚本文件
	——index.js		//程序主入口
```

### 3. 配置文件

配置文件位于./config目录下的config.js中

```js
module.exports = {
    //服务端口
    port:3000,
    //jwt加密盐
    salt:'task_server',
    //jwt过期时间,单位秒
    jwt_expires:3600,
    //mongodb数据库连接地址
    mongodb:'mongodb://localhost/task_server',
    //任务文件上传路径
    task_upload: {
        path: process.cwd() + '/storage/file'
    },
    //空文件检查间隔时间，单位秒
    check_file_interval:5,
  	//任务超时检测间隔时间，单位秒
    check_label_interval:5
}
```

### 4. 命令

在进入工程目录下使用命令可以管理服务的运行。

```bash
# npm install                //安装工程依赖node包
# npm run need               //运行安装系统依赖（将安装webpack以及forever）
# npm run pack               //进行项目打包生成task.js脚本
# npm run start              //启动项目服务
# npm run restart            //重新启动服务
# npm run stop               //停止服务
# tail -f ./task_info.log    //查看监听日志
# tail -f ./task_error.log   //查看监听错误日志
```

服务运行后，将在目录输下的task_info.log出标准日志，在task_error.log中输出错误日志。

服务运行前的必须操作:

1. 确保系统npm全局安装forever，可运行 npm run need(连带webpack安装)
2. 确保安装项目依赖，可运行 npm run install
3. 确保项目已经打包，可运行 npm run pack（需要npm全局安装webpack）
4. 确保前端打包静态文件已经放入dist文件夹中
5. 根据命令运行启动