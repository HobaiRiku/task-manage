
const mongoose = require('mongoose');

let ChatSchema = new mongoose.Schema(
    {
        "userName" : {type:String},
        "roleName" : {type:String},
        "task":{type:mongoose.SchemaTypes.ObjectId,ref:"Task"},
        "text":{type:String},
        "createAt" :{type:Date ,default: Date.now}
    }
);

//显示schema类型
ChatSchema.methods.showType = function(){
    return "Chat";
};
let Chat =mongoose.model("Chat",ChatSchema);

module.exports={"model":Chat};