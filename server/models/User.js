
const mongoose = require('mongoose');
const encrypt = require("hobai-nodejs-tool").common.encrypt;

let UserSchema = new mongoose.Schema(
    {
        "name" : {type:String},
        "account":{type:String},
        "password" : {type:String},
        "email":{type:String},
        "phone":{type:String},
        "role":{type:mongoose.SchemaTypes.ObjectId,ref:"Role"},
        "state":{type:Number , default:1},
        "department":{type:String,default:"未设定部门"},
        "jwtList":[{type:Number}],
        "createAt" :{type:Date ,default: Date.now}
    },{ usePushEach: true }
);

//验证密码
UserSchema.methods.verifyPassword = function(password){
    return encrypt(password) === this.password;
};
//显示schema类型
UserSchema.methods.showType = function(){
    return "User";
};

let User =mongoose.model("User",UserSchema);

module.exports={"model":User};