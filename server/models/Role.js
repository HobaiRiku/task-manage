
const mongoose = require('mongoose');

let RoleSchema = new mongoose.Schema(
    {
        "name":{type:String},
        "state":{type:Number , default:1},
        "createAt" :{type:Date ,default: Date.now}
    }
);

//显示schema类型
RoleSchema.methods.showType = function(){
    return "Role";
};

let Role =mongoose.model("Role",RoleSchema);

module.exports={"model":Role};