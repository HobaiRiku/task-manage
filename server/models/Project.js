
const mongoose = require('mongoose');

let ProjectSchema = new mongoose.Schema(
    {
        "name" : {type:String},
        "desc" : {type:String},
        "creator":{type:mongoose.SchemaTypes.ObjectId,ref:"User"},
        "createAt" :{type:Date ,default: Date.now}
    }
);
//显示schema类型
ProjectSchema.methods.showType = function(){
    return "Project";
};

let Project =mongoose.model("Project",ProjectSchema);

module.exports={"model":Project};