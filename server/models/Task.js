
const mongoose = require('mongoose');

let TaskSchema = new mongoose.Schema(
    {
        "creator":{type:mongoose.SchemaTypes.ObjectId,ref:"User"},
        "topic" : {type:String},
        "remark":{type:String},
        "project":{type:mongoose.SchemaTypes.ObjectId,ref:"Project"},
        "executor":{type:mongoose.SchemaTypes.ObjectId,ref:"User"},
        "deadLine":{type:Date},
        "label":{type:String,default:'沟通中'},
        "file":{type:String},
        "priority":{type:Number},
        "createAt" :{type:Date ,default: Date.now}
    }
);
//显示schema类型
TaskSchema.methods.showType = function(){
    return "Task";
};

let Task =mongoose.model("Task",TaskSchema);

module.exports={"model":Task};