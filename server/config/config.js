module.exports = {
    //服务端口
    port:3000,
    //jwt加密盐
    salt:'task_server',
    //jwt过期时间,单位秒
    jwt_expires:3600,
    //mongodb数据库连接地址
    mongodb:'mongodb://localhost/task_server',
    //任务文件上传路径
    task_upload: {
        path: process.cwd() + '/storage/file'
    },
    //空文件检查间隔时间，单位秒
    check_file_interval:5,
    //任务超时检测间隔时间，单位秒
    check_label_interval:5
}