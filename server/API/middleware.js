
const express = require('express');
const bodyParser = require('body-parser');
const validator = require('express-validator');
const router = express.Router();
const modle = require('../models');
const jwt = require('jsonwebtoken');
const config = require('../index').config;

const hobai_tool = require('hobai-nodejs-tool')
//设置hobai_tool
//消除查询子文档_id模糊化
hobai_tool.application.getQuery.ignore.push('role','project','task');
//hobai_tool.application.getQuery.exclude.push('term');

//解析body中间件
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({
    extended: false
}));

const m_e_tool = require('monpress-generator');
//m&e_tool请求初始化
router.use(m_e_tool.action.reqinit());
//jwt拦截获取
router.use(function(req,res,next){
    let token;
    try {
        token = req.body.token || req.query.token || req.headers['token'];
        if(!token) {
            req.jwt = {type:'error',name:'jwt no found',message:' no jwt found in request',token:token}
            return  next();
        }
        let decode = jwt.verify(token,config.salt);
        req.jwt = {type:'verified',decode:decode,token:token}
        next();
    } catch (error) {
        //jwt 过期或错误
        if(error.name==='TokenExpiredError'||error.name==='JsonWebTokenError'){
            req.jwt = {type:'error',name:error.name,message:error.message,token:token}
            next();
        }else
        next(error)
    }
})
//根据jwt反序列化用户信息入req.user
const user_deserialize = require('../common/user_deserialize')
router.use(user_deserialize.user);



//后端表单验证validator中间件，参数为自定义返回错误格式。不使用此中间件无法使用checkbody
router.use(validator({
    errorFormatter: function (param, msg, value) {
        let namespace = param.split('.'),
            root = namespace.shift(),
            formParam = root;

        while (namespace.length) {
            formParam += '[' + namespace.shift() + ']';
        }
        return {
            param: formParam,
            msg: msg,
            value: value
        };
    }
}));



module.exports = router;