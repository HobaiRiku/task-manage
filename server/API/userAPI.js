
const router = require('express').Router();
const me_tool = require('monpress-generator');
const model = require('../models');
const authority = require('../common/anthority')
const action = require('../actions')
router.use(authority.requireLogin)
//post限制为全局管理员
router.post('/',authority.requireRootAdmin)
//强调用户中的jwtList属性必须初始化
router.post('/',function(req,res,next){
    if(!req.entity.jwtList){
      req.entity.jwtList = [];
    }
    next();
})
router.get('/',action.userAction.lsit_auth);
router.get('/:_id',authority.requireRootAdmin);
router.put('/:_id',authority.requireRootAdmin);
router.delete('/:_id',authority.requireRootAdmin);

//restful API生成
router.use(me_tool.router.RESTful(model.User,"role"));


module.exports = router;