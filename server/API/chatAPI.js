
const router = require('express').Router();
const me_tool = require('monpress-generator');
const model = require('../models');
const action = require('../actions');
const authority = require('../common/anthority')
//要求登录
router.use(authority.requireLogin);
//设置list权限
router.get('/',action.chatAction.list_auth);
//设置post权限
router.post('/',action.chatAction.post_auth);
//设置单对象访问权限
router.get('/:_id',action.chatAction.get_auth);
router.put('/:_id',action.chatAction.get_auth);
router.delete('/:_id',action.chatAction.get_auth);

//restful API生成
router.use(me_tool.router.RESTful(model.Chat));


module.exports = router;