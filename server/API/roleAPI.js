
const router = require('express').Router();
const me_tool = require('monpress-generator');
const model = require('../models');
const authority = require('../common/anthority')
router.use(authority.requireRootAdmin)
//restful API生成
router.use(me_tool.router.RESTful(model.Role));


module.exports = router;