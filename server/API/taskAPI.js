
const router = require('express').Router();
const me_tool = require('monpress-generator');
const model = require('../models');
const action = require('../actions');
const authority = require('../common/anthority')
const config = require('../index').config
let upload = require('../common/fileUpload')(config.task_upload.path)
//要求登录
router.use(authority.requireLogin);
//设置list权限
router.get('/',action.taskAction.list_auth);
//设置post权限
router.post('/',action.taskAction.post_auth);
//设置单对象访问权限
router.get('/:_id',action.taskAction.get_auth);
router.put('/:_id',action.taskAction.get_auth);
router.delete('/:_id',action.taskAction.get_auth);
//上传任务文件接口
router.post('/:_id/file',action.taskAction.uploadFile_auth,upload.single('taskFile'),action.taskAction.uploadFile)
//restful API生成
router.use(me_tool.router.RESTful(model.Task,['project','executor','creator']));


module.exports = router;