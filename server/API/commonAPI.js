
const router = require('express').Router();
const action = require('../actions');
const anthority = require('../common/anthority');
const me_tool = require('monpress-generator');
const model = require('../models');
//登录
router.post('/login',action.commonAction.login);
//获取当前user信息
router.get('/profile',anthority.requireLogin,function(req,res,next){
    req.params._id = req.jwt.decode._id;
    next();
},me_tool.action.get(model.User,"role"))
//登出
router.post('/logout',anthority.requireLogin,action.commonAction.logout);
router.get('/isLogin',anthority.requireLogin,function (req,res,next) {
    res.send({isLogin:true})
})
router.get('/isRootAdmin',anthority.requireRootAdmin,function (req,res,next) {
    res.send({isRootAdmin:true})
})
module.exports = router;