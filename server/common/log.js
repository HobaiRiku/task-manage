const mongoose = require('mongoose');
const app = require('express')();
const http = require('http').Server(app);
//const io = require('socket.io')(http);
let io = require('../index').io;
const util = require('util')
let ignore_console_Out = ['request_out'];

let LogSchema = new mongoose.Schema(
    {
        "level" : {type:String,default:'info'},
        "info" : {type:String,default:''},
        "message" : {type:String,default:''},
        "stack" : {type:String,default:''},
        "create_date" :{type:Date ,default: Date.now},
    }
);

let Log =mongoose.model("Log",LogSchema);

/**
 * /**
 * 错误日志输出
 * @param {Error} err 
 */
let error=function(err){
    if(arguments.length>1)
    return error(new Error('function error() can only have one argument '));
    if(!(err.message&&err.stack))
    return error(new Error('function error()\'s argument can only be Error '));
    console.log(new Date()+ ':' +'Log-error:'+err);
    new Log({level:'error',info:'error occurred',message:err.message,stack:err.stack}).save().catch((err)=>{error(err);});
    io.sockets.emit('error',err.message);
}
let print =function(){
    let out = "";
    if (typeof (arguments[0]) != 'string')
        return error(new Error('function print()\'s  first argument must be level(String)')); 
    for(let i=1;i<arguments.length;i++){
        if (typeof (arguments[i]) == 'object')
            out +=util.inspect(arguments[i],util._extend({customInspect: false}));
        else out += arguments[i];
        /* if(arguments[i].message&&arguments[i].stack)
        return error(arguments[i]); */
    }
    if (ignore_console_Out.indexOf(arguments[0])==-1) {
        console.log(new Date()+ ':' +'Log-' + arguments[0] + ':' + out);
        io.sockets.emit(arguments[0],out);
    }
    new Log({info:out,level:arguments[0]}).save().catch((err)=>{error(err);});
    
}
/**
 * info日志输出
 */
let out = function () {
    let arr = [];
    arr[0] = 'info';
    for (let i = 0; i < arguments.length; i++) { 
        arr[i + 1] = arguments[i];
    }
    print.apply(print,arr);
}
/**
 * 自定义日志输出
 * @param {String} level 日志等级
 */
let custom = function (level) {
    if (typeof (level) != 'string')
    return error(new Error('argument "level" must be String')); 
    let arr = [];
    arr[0] = level;
    for (let i = 1; i < arguments.length; i++) { 
        arr[i] = arguments[i];
    }
    print.apply(print,arr);
}
/**
 * 打印请求信息
 * @param {'Express.req'} req 
 */
let requestLog = function(req){
    if(!req.hostname||!req.originalUrl) 
    return error(new Error('function requestLog()\'s argument can only be \'express:req\' '));
    if(req.method=='GET')
    //custom('request_in','\n收到http请求:\npath:',req.originalUrl,'\nip:',req.hostname,'\nmethod:',req.method,'\nquery:',req.query,'\nparams:',req.params,'\n');
    custom('request_in','path:',req.originalUrl,',ip:',req.ip.match(/\d+\.\d+\.\d+\.\d+/)?req.ip.match(/\d+\.\d+\.\d+\.\d+/)[0]:'localhost',',method:',req.method,',query:',req.query,',params:',req.params);
    else 
    //custom('request_in','\n收到http请求:\npath:',req.originalUrl,'\nip:',req.hostname,'\nmethod:',req.method,'\nbody:',req.body,'\nparams:',req.params,'\n');
    custom('request_in','path:',req.originalUrl,',ip:',req.ip.match(/\d+\.\d+\.\d+\.\d+/)?req.ip.match(/\d+\.\d+\.\d+\.\d+/)[0]:'localhost',',method:',req.method,',body:',req.body,',params:',req.params);
}
/* 
io.on("connection",function(socket){
    out('一个客户端成功连接日志log_socket.',socket.handshake.address);
   socket.emit('wellcome','欢迎连接日志');
	}); */

/* http.listen(4455,(err)=>{
    custom("launch","log日志启动,socket监听:4455");
}); */


module.exports={'io':io ,'out':out,'error':error,'requestLog':requestLog,'custom':custom};