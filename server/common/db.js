const mongoose = require('mongoose');
const config = require('../index').config;
mongoose.connect(config.mongodb,{ useMongoClient: true });
const log=require('./log');
//使用本地Promise
mongoose.Promise = global.Promise;
let db = mongoose.connection;
db.once('open', function (callback) {
    log.custom('launch',"数据库成功打开");
});

module.exports = db;