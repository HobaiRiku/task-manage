
const model = require('../models');
const jwt  = require('jsonwebtoken');
const config = require('../index').config;
exports.user = async function(req,res,next){
    try {
    if(req.jwt.type==='error'){
        req.user=null;
        return next()
    }
        let _id = req.jwt.decode._id;
        let user = await model.User.findById(_id).populate('role');
        if(user.jwtList.indexOf(req.jwt.decode.iat)==-1){
            let err= Error("jwt error: "+"jwt has been deprecated");
            err.status=210;
            return next(err);
        }
        for(let i=0 ;i<user.jwtList.length;i++){
            if(( new Date().getTime()/1000-user.jwtList[i])> config.jwt_expires){
                   user.jwtList.splice(i,1);
            }
        }
        await user.save();
        req.user = user;
        next();
    } catch (error) {
        next(error)
    }
}