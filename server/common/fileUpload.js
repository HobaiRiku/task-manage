let multer = require('multer');
let md5 = require('md5');

/**
 * 
 * 给定文件路径，获取上传中间件
 * @param {String} filePath 
 * @returns 
 */
function getUpload(filePath){
let storage = multer.diskStorage({
    //设置上传文件路径,以后可以扩展成上传至七牛,文件服务器等等
    //Note:如果你传递的是一个函数，你负责创建文件夹，如果你传递的是一个字符串，multer会自动创建
    destination: filePath,
    //TODO:文件区分目录存放
    //获取文件MD5，重命名，添加后缀,文件重复会直接覆盖
    filename: function (req, file, cb) {
        let fileFormat =(file.originalname).split(".");
        let prop ={
            originalname:file.originalname,
            encoding:file.encoding,
            mimetype:file.mimetype
        } 
        cb(null, file.originalname.split(".")[0] + '-' + md5(JSON.stringify(prop)) + "." + fileFormat[fileFormat.length - 1]);
    }
});

//添加配置文件到muler对象。
let upload = multer({
    storage: storage,
    //其他设置请参考multer的limits
    //limits:{}
});
return upload;
}
//导出对象
module.exports = getUpload;