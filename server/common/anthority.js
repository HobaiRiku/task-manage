
//要求存在登录ID
 exports.requireLogin=function(req, res, next){
    //ID存在 放行
    if(req.user){
        next();
    }else{
        let err = Error("No Access: login user not found.");
        err.status=211;
        next(err)
        
    }
}   
//要求携带JWT
exports.requireJWT = function(req,res,next){
    //jwt信息错误
    if(req.jwt.type==='error'){
        let err= Error("No Access: "+req.jwt.message);
        err.status=211;
        return next(err);
    }
    next();
}
//要求全局管理员
exports.requireRootAdmin = function(req,res,next){
    if(req.user.role.name!= 'rootadmin'){
        let err= Error("No Access: require rootadmin authority");
        err.status=212;
        return next(err);
    }
    next();
}