
const event = require('monpress-generator').event
const model = require('../models')
const log = require('./log')
const fs = require('fs')
const tool =require('hobai-nodejs-tool')
const io = require('../index').io
exports.eventListent = function(){
    //请求事件
    event.on('request',function(req){
        log.requestLog(req);
    })
    //添加任务事件
    event.on('add',async function(req,obj_new){
        if(obj_new.showType() =='Task'){
            try {
                let chat_new = new model.Chat({
                    "userName":req.user.name,
                    "roleName":req.user.role.name,
                    "task":obj_new._id,
                    "text":obj_new.remark
                });
                chat_new.save();
            } catch (error) {
                console.error(error)
            }
        }
    })
       //添加聊天事件
       event.on('add',function(req,obj_new){
        if(obj_new.showType() =='Chat'){
            try {
             io.emit('task/'+obj_new.task,'new Chat in')
            } catch (error) {
                console.error(error)
            }
        }
    })
     //删除任务事件
     event.on('del',async function (req,obj_del) {
        if(obj_del.showType() =='Task'){
            try {
                //删除聊天
                await model.Chat.remove({_id:obj_del._id});
                //删除文件
                if(!tool.common.isN(obj_del.file)){
                fs.unlinkSync(process.cwd() + '/storage'+obj_del.file)
                }
            } catch (error) {
                console.error(error)
            }
        } 
    })
    //删除项目事件
    event.on('del',async function (req,obj_del) {
        if(obj_del.showType() =='Project'){
           try {
               //获得任务列表
               let task_list = await model.Task.find({project:obj_del._id})
               for(let task of task_list){
                    //删除下属所有聊天
                    await model.Chat.remove({_id:task._id});
                    //删除文件
                    if(!tool.common.isN(task.file)){
                        fs.unlinkSync(process.cwd() + '/storage'+task.file)
                        }
               }
           } catch (error) {
              console.error(error)
           }
                
                
           
        } 
    })
    
}