

const model = require('../models');
const config = require('../index').config
exports.interval = function () {
    setInterval(async function () {
        
    },config.check_file_interval*1000);
    setInterval(async function(){
        try {
            let taskList = await model.Task.find();
            for(let task of taskList){
                let date_now = new Date();
                if(task.deadLine<date_now&&task.label ==='沟通中'){
                    task.label="已超时";
                    await task.save();
                }
                if(task.deadLine>date_now&&task.label ==='已超时'){
                    task.label="沟通中";
                    await task.save();
                }
                
            }
        } catch (error) {
            console.error(error)
        }
    },config.check_label_interval*1000);
}