const init = require("./init.json");
const model= require('../models');
const log = require('./log');

//导入模型(作为字典)
const Models =model;

//首次初始化工程数据库
exports.initialize = function () {
    model.User.find(function(error,find){
      if(error) return log.error(error);
     if(find.length<1){
        log.custom('launch','项目首次运行，数据库初始化中....');
        Promise.all(init.map(item => new Models[item.type](item).save()))
          .then(() => log.custom('launch','初始化成功..'))
          .catch((error) => log.custom('launch','初始化失败.'+error))
      } 
    });
    };
