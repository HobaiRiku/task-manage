const log = require('../common/log');
const model = require('../models');
const config = require('../index').config;

exports.post_auth = async function post_auth(req,res,next){
    let user = req.user;
    let role = user.role;
    let id =user._id.toString();
     //必须传入task与text
     if(!req.entity.task) {
        return res.status(209).json({
            message:"error:‘task’ not found"
        })
    }
    if(!req.entity.text) {
        return res.status(209).json({
            message:"error:‘text’ not found"
        })
    }
    let task = await model.Task.findById(req.entity.task);
    if(!task) {
        return res.status(209).json({
            message:"error:invalid ‘task’"
        })
    }
    req.entity.userName = user.name;
    req.entity.roleName = role.name;
    //全局管理员
    if(role.name ==='rootadmin'){
        return next();
    } 
    //普通管理员情况
    if(role.name ==='admin'){
        //判断task是否为本人创建
        if(task.creator!= id){
            return res.status(209).json({
                message:"error: no authority to create item"
            })
        }
        return next();
    }
    if(role.name === 'user'){
        //判断task执行者是否为本人
        if(task.executor!= id){
            return res.status(209).json({
                message:"error: no authority to create item"
            })
        }
        return next();
    }
    //其他
    res.status(209).json({
        message:"error: no authority to create item"
    })
}
exports.list_auth = async function list_auth(req,res,next) {
        let user = req.user;
        let role = user.role;
        let id = user._id.toString();
        req.sort = 'createAt'
        //全局管理员拥有查看所有权限
        if(role.name ==='rootadmin') return next();
        //普通管理员情况
        if(role.name ==='admin'){
            //限制为必须传入task
            if(!req.condition.task){
                res.status(209).json({
                    message:"error:'task' no found"
                })
            }
            //检测task创建者是否为本人
            let task = await model.Task.findById(req.condition.task);
            if(task.creator!=id){
                res.status(209).json({
                    message:"error:no authority to item"
                })
            }
            return next();
        } 
        //普通用户情况
        if(role.name ==='user'){
           //限制必须传入task
           if(!req.condition.task){
            res.status(209).json({
                message:"error:'task' no found"
            })
        }
        //检测task执行者是否为本人
        let task = await model.Task.findById(req.condition.task);
        if(task.executor!=id){
            res.status(209).json({
                message:"error:no authority to item"
            })
        }
          return  next();
        }
        next(new Error('error: no authority'))
}
exports.get_auth = async function get_auth (req,res,next){
    let user = req.user;
        let role = user.role;
        let chat_id = req.params._id;
        let chat = await model.Chat.findById(chat_id);
        let task_id = chat.task;
        let id = user._id.toString();
        //全局管理员拥有查看所有权限
        if(role.name ==='rootadmin') return next();

        //当非全局管理员进行chat删除，检测是否已经超过2分钟
        if(req.method=="DELETE"){
            let time_now = new Date();
            let time_chat = new Date(chat.createAt);
            if((time_now.getTime()-time_chat.getTime())>1000*120){
                return res.status(209).json({
                    message:"error:It has been 2 minute since chat created,operation denied"
                })
            }
        }
        //普通管理员情况
        if(role.name ==='admin'){
            //限制为只能get和delete
            if(req.method!="GET"||req.method!="DELETE"){
                return res.status(209).json({
                    message:"error: no authority to modify item"
                })
            }
            //判断task的创始人是否为该普通管理员
            let task = await model.Task.findById(task_id);
            if(task.creator != id){
                return res.status(209).json({
                    message:"error: no authority to item"
                })
            }
            return next();
        } 
        //普通用户情况
        if(role.name ==='user'){
            //限制为只能get和delete
            if(req.method!="GET"||req.method!="DELETE"){
                return res.status(209).json({
                    message:"error: no authority to modify item"
                })
            }
            //判断task的执行人是否为该用户
            let task = await model.task.findById(task_id);
            if(task.executor != id){
                return res.status(209).json({
                    message:"error: no authority to item"
                })
            }
            
           return next();
        }
        next(new Error('error: no authority'))      
}