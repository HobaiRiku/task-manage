
const log = require('../common/log');
const model = require('../models');
const config = require('../index').config;
const tool = require('hobai-nodejs-tool');
const fs = require('fs');
const md5 = require('md5')
exports.post_auth = async function post_auth(req, res, next) {
    let user = req.user;
    let role = user.role;
    let id = user._id.toString()
    //全局管理员
    if (role.name === 'rootadmin') {
        if (!req.entity.project) {
            return res.status(209).json({
                message: "error:‘project’ not found"
            })
        }
        if (!req.entity.creator) {
            return res.status(209).json({
                message: "error:‘creator’ not found"
            })
        }
        if (!req.entity.executor) {
            return res.status(209).json({
                message: "error:‘executor’ not found"
            })
        }
        if (!req.entity.remark) {
            return res.status(209).json({
                message: "error:‘remark’ not found"
            })
        }
        return next();
    }
    //普通管理员情况
    if (role.name === 'admin') {
        req.entity.creator = user._id;
        if (!req.entity.project) {
            return res.status(209).json({
                message: "error:‘project’ not found"
            })
        }
        if (!req.entity.executor) {
            return res.status(209).json({
                message: "error:‘executor’ not found"
            })
        }
        if (!req.entity.remark) {
            return res.status(209).json({
                message: "error:‘remark’ not found"
            })
        }
        //判断project是否为本人创建
        let project = await model.Project.findById(req.entity.project);
        if (project.creator != id) {
            return res.status(209).json({
                message: "error: no authority to create item"
            })
        }
        return next();
    }
    //普通用户无权限
    res.status(209).json({
        message: "error: no authority to create item"
    })
}
exports.list_auth = async function list_auth(req, res, next) {
    let user = req.user;
    let role = user.role;
    //全局管理员拥有查看所有权限
    if (role.name === 'rootadmin') return next();
    //普通管理员情况
    if (role.name === 'admin') {
        //限制为自身创建的任务
        req.condition.creator = user._id;
        return next();
    }
    //普通用户情况
    if (role.name === 'user') {
        //限制为自身执行的任务
        req.condition.executor = user._id
        return next();
    }
    next(new Error('error: no authority'))
}
exports.get_auth = async function get_auth(req, res, next) {
    let user = req.user;
    let role = user.role;
    let task_id = req.params._id;
    let id = user.id.toString();
    //全局管理员拥有查看所有权限
    if (role.name === 'rootadmin') return next();
    //普通管理员情况
    if (role.name === 'admin') {
        //判断task的创始人是否为该普通管理员
        let task = await model.Task.findById(task_id);
        if (!task) {
            return res.status(209).json({
                message: "error:item not found"
            })
        }
        if (task.creator != id) {
            return res.status(209).json({
                message: "error: no authority to item"
            })
        }
        return next();
    }
    //普通用户情况
    if (role.name === 'user') {
        //限制该用户只能进行get请求
        if (req.method != "GET") {
            return res.status(209).json({
                message: "error: no authority to modifiy item"
            })
        }
        //判断task的执行人是否为该用户
        let task = await model.Task.findById(task_id);
        if (task.executor != id) {
            return res.status(209).json({
                message: "error: no authority to item"
            })
        }
        return next();
    }

    next(new Error('error: no authority'))
}
exports.uploadFile_auth = async function uploadFile_auth(req, res, next) {
    let user = req.user;
    let role = user.role;
    let task_id = req.params._id;
    let id = user.id.toString();
    if (role.name === 'rootadmin') return next();
    if (role.name === 'admin') {
        //判断task的创始人是否为该普通管理员
        let task = await model.Task.findById(task_id);
        if (!task) {
            return res.status(209).json({
                message: "error:item not found"
            })
        }
        if (task.creator != id) {
            return res.status(209).json({
                message: "error: no authority to item"
            })
        }
        return next();
    }
    return res.status(209).json({
        message: "error: no authority to item"
    })
}
exports.uploadFile = async function uploadFile(req, res, next) {
    log.requestLog(req);
    let file = req.file;
    let taskId = req.params._id;
    let prop = {
        originalname: file.originalname,
        encoding: file.encoding,
        mimetype: file.mimetype,
    }
    let fileFormat = (file.originalname).split(".");
    let file_name = "/file/" + file.originalname.split(".")[0] + '-' + md5(JSON.stringify(prop)) + "." + fileFormat[fileFormat.length - 1];
    let task = await model.Task.findById(taskId);
    //判断任务是否已经存在文件
    if(!tool.common.isN(task.file)){
        try {
            fs.unlinkSync(process.cwd() + '/storage'+task.file)
        } catch (error) {
            log.error(error)
        }
    }
    task.file = file_name;
    task.save();
    res.status(201).send({
        message: "file uploaded"
    });
}