
const log = require('../common/log');
const model = require('../models');
const config = require('../index').config;

exports.lsit_auth = async function lsit_auth(req,res,next) {
    let user = req.user;
    let role = user.role;
    //全局管理员
    if(role.name ==='rootadmin'){
        return next();
    } 
    //普通管理员情况
    if(role.name ==='admin'){
        //限制为只能列出user级别用户
        let role = await model.Role.findOne({name:'user'});
        req.condition.role = role._id;
        return next();
    }
    //普通用户无权限
    res.status(209).json({
        message:"error: no authority to item"
    })
}