const log = require('../common/log');
const model = require('../models');
const config = require('../index').config;

exports.post_auth = function post_auth(req,res,next){
    let user = req.user;
    let role = user.role;
    //全局管理员
    if(role.name ==='rootadmin'){
        if(!req.entity.creator) {
            return res.status(209).json({
                message:"error: ‘creator’ not found "
            })
        }
        return next();
    } 
    //普通管理员情况
    if(role.name ==='admin'){
        req.entity.creator = user._id;
        return next();
    }
    //普通用户无权限
    res.status(209).json({
        message:"error: no authority to create item"
    })
}
exports.list_auth = async function list_auth(req,res,next) {
        let user = req.user;
        let role = user.role;
        //全局管理员拥有查看所有权限
        if(role.name ==='rootadmin') return next();
        //普通管理员情况
        if(role.name ==='admin'){
            //列出改普通管理员创建的项目列表
            let project_list = await model.Project.find({creator:user._id});
            //设置where
            let _in =[];
            for(let project of project_list){
                _in.push(project._id);
            }
            let where={name:'_id',in:_in}
            req.where = where;
            return next();
        } 
        //普通用户情况
        if(role.name ==='user'){
            //列出该用户的所有任务
            let task_list = await model.Task.find({executor:user._id})
            //统计任务属于的项目
            let project_list =[];
            for(let task of task_list){
                let flag = true
                if (project_list.length==0) project_list.push(task.project)
                for(let project of project_list){
                    if(project == task.project) {
                        flag = false;
                        break
                    }
                }
                if(flag) project_list.push(task.project);
            }
            //设置where
            let where = {name:"_id",in:project_list};
            req.where=where;
            next();
        }
}
exports.get_auth = async function get_auth (req,res,next){
    let user = req.user;
        let role = user.role;
        let project_id = req.params._id;
        let id = user._id.toString()
        //全局管理员拥有查看所有权限
        if(role.name ==='rootadmin') return next();
        //普通管理员情况
        if(role.name ==='admin'){
            //判断project的创始人是否为该普通管理员
            let project = await model.Project.findById(project_id);
            if(project.creator != id){
                return res.status(209).json({
                    message:"error: no authority to item"
                })
            }
            return next();
        } 
        //普通用户情况
        if(role.name ==='user'){
            //列出该用户的所有任务
            let task_list = await model.Task.find({executor:user._id})
            //统计任务属于的项目
            let project_list =[];
            for(let task of task_list){
                let flag = true
                if (project_list.length==0) project_list.push(task.project)
                for(let project of project_list){
                    if(project == task.project) {
                        flag = false;
                        break
                    }
                }
                if(flag) project_list.push(task.project);
            }
            
            //判断访问的项目是否在允许列表中
            if(project_list.indexOf(project_id)==-1){
                return res.status(209).json({
                    message:"error: no authority to item"
                })
            }
            next();
        }
}