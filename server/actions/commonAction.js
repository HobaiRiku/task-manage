
const log = require('../common/log');
const model = require('../models');
const jwt = require('jsonwebtoken');
const config = require('../index').config;
const tool = require('hobai-nodejs-tool');
exports.login = async function (req,res,next){
    log.requestLog(req);
    try {
        if(tool.common.isN(req.body.account))throw new Error('请输入账号');
        if(tool.common.isN(req.body.password))throw new Error('请输入密码');
        let account = req.body.account;
        let password = req.body.password;
        let condition = {account:account};
        if(/^1[3|4|5|8][0-9]\d{4,8}$/.test(account)) condition = {phone:account};
        if(/^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/.test(account)) condition = {email:account};
        let user_find = await model.User.findOne(condition);
        if(!user_find) throw new Error('用户不存在');
        if(!user_find.verifyPassword(password)) throw new Error('账号与密码不匹配');
        let user = {_id:user_find._id,name:user_find.name,type:'user'};
        let  token = jwt.sign(user, config.salt, { expiresIn: config.jwt_expires });
        let decode = jwt.decode(token);
      user_find.jwtList.push(decode.iat);
        await user_find.save();
        res.json({message:'登录成功',token:token});
    } catch (error) {
       next(error)
    }
}
exports.logout = async function(req,res,next){
    log.requestLog(req);
    try {
        let user = await model.User.findById(req.user._id);
         let index = user.jwtList.indexOf(req.jwt.decode.iat);
         user.jwtList.splice(index,1);
         await user.save();
         res.status(200).json({message:'用户登出'})
    } catch (error) {
        next(error)
    }
}
