import axios from 'axios'
import qs from 'qs';
let apiName = '/api'

/**
 * 获取所有用户列表
 * @param {*} query 
 */
export const listUser = query => {
  return axios.get(apiName+"/user?" + qs.stringify(query)).then(res => res)
};
/**
 * 获取一个用户
 * @param {*} _id 
 */
export const getUser = _id => {
  return axios.get(apiName+"/user/" + _id).then(res => res)
};
/**
 * 添加一个用户
 * @param {*} body 
 */
export const addUser = body => {
  return axios.post(apiName+"/user",body).then(res => res)
};

/**
 * 更新一个用户
 * @param {*} _id 
 * @param {*} body 
 */
export const updateUser = (_id,body) => {
  return axios.put(apiName+"/user/" + _id, body).then(res => res)
};

/**
 * 删除一个用户
 * @param {*} _id 
 */
export const deleteUser = _id => {
  return axios.delete(apiName+"/user/"+_id).then(res => res)
};