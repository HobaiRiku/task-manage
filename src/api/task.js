import axios from 'axios'
import qs from 'qs';
let apiName = '/api'


/**
 * 获取所有task列表
 * @param {*} query 
 */
export const listTask = query => {
  return axios.get(apiName+"/task?" + qs.stringify(query)).then(res => res)
};
/**
 * 获取一个task
 * @param {*} _id 
 */
export const getTask = _id => {
  return axios.get(apiName+"/task/" + _id).then(res => res)
};
/**
 * 添加一个task
 * @param {*} body 
 */
export const addTask = body => {
  return axios.post(apiName+"/task",body).then(res => res)
};

/**
 * 更新一个task
 * @param {*} _id 
 * @param {*} body 
 */
export const updateTask = (_id,body) => {
  return axios.put(apiName+"/task/" + _id, body).then(res => res)
};

/**
 * 删除一个task
 * @param {*} _id 
 */
export const deleteTask = _id => {
  return axios.delete(apiName+"/task/"+_id).then(res => res)
};
/**
 * 向一个任务上传一个文件
 * @param {*} _id 
 * @param {*} file 
 */
export const uploadFile = (_id,file)=>{
  let config={
    headers:{}
  };
  let token  = localStorage.getItem("myToken");
    if (token) { 
        config.headers.token = token;
    }
    config.headers['Content-Type']='multipart/form-data';
    let formData = new FormData();
    formData.append('taskFile',file)
    return axios.post(apiName+"/task/"+_id+'/file',formData,config).then(res => res)
}