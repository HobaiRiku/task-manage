import axios from 'axios'
import qs from 'qs';
let apiName ='/api'


/**
 * 获取所有peoject列表
 * @param {*} query 
 */
export const listProject = query => {
  return axios.get(apiName+"/project?" + qs.stringify(query)).then(res => res)
};
/**
 * 获取一个project
 * @param {*} _id 
 */
export const getProject = _id => {
  return axios.get(apiName+"/project/" + _id).then(res => res)
};
/**
 * 添加一个project
 * @param {*} body 
 */
export const addProject = body => {
  return axios.post(apiName+"/project",body).then(res => res)
};

/**
 * 更新一个project
 * @param {*} _id 
 * @param {*} body 
 */
export const updateProject = (_id,body) => {
  return axios.put(apiName+"/project/" + _id, body).then(res => res)
};

/**
 * 删除一个project
 * @param {*} _id 
 */
export const deleteProject = _id => {
  return axios.delete(apiName+"/project/"+_id).then(res => res)
};