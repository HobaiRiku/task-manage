import axios from 'axios'
import qs from 'qs';
let apiName = '/api'


/**
 * 获取所有chat列表
 * @param {*} query 
 */
export const listChat = query => {
  return axios.get(apiName+"/chat?" + qs.stringify(query)).then(res => res)
};
/**
 * 获取一个chat
 * @param {*} _id 
 */
export const getChat = _id => {
  return axios.get(apiName+"/chat/" + _id).then(res => res)
};
/**
 * 添加一个chat
 * @param {*} body 
 */
export const addChat = body => {
  return axios.post(apiName+"/chat",body).then(res => res)
};

/**
 * 更新一个chat
 * @param {*} _id 
 * @param {*} body 
 */
export const updateChat = (_id,body) => {
  return axios.put(apiName+"/chat/" + _id, body).then(res => res)
};

/**
 * 删除一个chat
 * @param {*} _id 
 */
export const deleteChat = _id => {
  return axios.delete(apiName+"/chat/"+_id).then(res => res)
};