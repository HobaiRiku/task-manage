import axios from 'axios'
import qs from 'qs';
let apiName = '/api'


/**
 * 获取所有role列表
 * @param {*} query 
 */
export const listRole = query => {
  return axios.get(apiName+"/role?" + qs.stringify(query)).then(res => res)
};
/**
 * 获取一个role
 * @param {*} _id 
 */
export const getRole = _id => {
  return axios.get(apiName+"/role/" + _id).then(res => res)
};
/**
 * 添加一个role
 * @param {*} body 
 */
export const addRole = body => {
  return axios.post(apiName+"/role",body).then(res => res)
};

/**
 * 更新一个role
 * @param {*} _id 
 * @param {*} body 
 */
export const updateRole = (_id,body) => {
  return axios.put(apiName+"/role/" + _id, body).then(res => res)
};

/**
 * 删除一个role
 * @param {*} _id 
 */
export const deleteRole = _id => {
  return axios.delete(apiName+"/role/"+_id).then(res => res)
};