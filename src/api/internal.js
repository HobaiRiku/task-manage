import axios from 'axios'
import qs from 'qs';
let apiName = '/api'

import router from '../router';

/**
 * 登录功能
 */
export const login = body => {
    return axios.post(apiName+"/internal/login", body).then(res => res)
};

/**
 * 登出功能
 */
export const logout = () => {
    return axios.post(apiName+"/internal//logout").then(res => res)
};

/**
 * 获取个人信息
 */
export const getProfile = () => {
    return axios.get(apiName+"/internal/Profile").then(res => res)
};

/**
 * 检测是否登录
 */
export const isLogin = () => {
    return axios.get(apiName+"/internal/isLogin").then(res => res)
};
/**
 * 检查是否为全局管理员
 */
export const isRootAdmin = () => {
    return axios.get(apiName+"/internal/isRootAdmin").then(res => res)
};