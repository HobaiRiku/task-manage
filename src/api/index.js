import * as internal from './internal';
import * as user from './user';
import * as role from './role';
import * as project from './project';
import * as chat from './chat';
import * as task from './task';


import axios from 'axios'
import router from '../router';
//请求拦截器
axios.interceptors.request.use(function(config){
    let token  = localStorage.getItem("myToken");
    if (token) { 
        config.headers.token = token;
    }
    return config;
  },function(error){
    return Promise.reject(error);
  });

//响应拦截器
axios.interceptors.response.use(function(response){
    if(response.status===211){
        localStorage.setItem('noLogin_message','获取会话失败，请登录');
        return router.push({
            path:'/login'
        })
    }
     return response;
  },function(error){
    return Promise.reject(error);
  });


  export {internal,user,role,project,chat,task};