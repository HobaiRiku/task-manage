import Vue from 'vue'
import Router from 'vue-router'
import Layout from '@/components/layout'
import ProjectList from '@/views/projectList'
import CreateProject from '@/views/createProject'
import TaskList from '@/views/taskList'
import TaskContent from '@/views/taskContent'
import CreateTask from '@/views/createTask'

import Login from '@/views/login'

import AdmLayout from '@/views/admin/admLayout'
import UserList from '@/views/admin/userList'
import CreateUser from '@/views/admin/createUser'
import AdmProjectList from '@/views/admin/projectList'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path:'/',
      redirect: '/login',
      component:Login,
    },
    {
      path:'/login',
      component:Login,
      name:'login',
    },
    // {
    //   path:'/',
    //   component:Layout,
    //   redirect: '/login',
    //   name:'login',
    //   children:[
    //     { path: '/login', component: Login }
    //   ]      
    // },
    {
      path: '/index',
      component: Layout,
      redirect: '/projectList',
      name: '控制台',
      children: [{ path: '/projectList', component: ProjectList }]
    },
    {
      path: '/createProject',
      component: Layout,
      redirect: '/createProject',
      name: '创建项目',      
      children: [{ path: '/createProject', component: CreateProject }]
    },
    {
      path: '/taskList',
      component: Layout,
      redirect: '/taskList',
      name: '任务详情',      
      children: [
        {
          path: '/taskList', 
          component: TaskList,
          redirect: '/taskContent',
          children:[
            {path: '/taskContent', component: TaskContent },
            {path: '/createTask', component: CreateTask }
          ]
        },
      ]
    },
    {
      path: '/admin',
      component: Layout,
      redirect: '/admin/admLayout',
      name: 'admLayout', 
      title:'人员列表',     
      children: [
        {
          path: '/admin/admLayout', 
          component: AdmLayout,
          redirect: '/admin/userList',
          children:[
            {path: '/admin/userList', component: UserList },
            {path: '/admin/createUser', component: CreateUser },
            {path: '/admin/projectList', component: AdmProjectList },   
          ]
        },
      ]
    },
    
  ]
})
