import Vue from 'vue'
import Router from 'vue-router'
import Layout from '@/components/layout'
import ProjectList from '@/views/projectList'
// import CreateProject from '@/views/createProject'
import TaskList from '@/views/taskList'
import TaskContent from '@/views/taskContent'
import CreateTask from '@/views/createTask'

import Login from '@/views/login'

import AdmLayout from '@/views/admin/admLayout'
import UserList from '@/views/admin/userList'
import CreateUser from '@/views/admin/createUser'
import AdmProjectList from '@/views/admin/projectList'
import * as api from '../api'
Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      redirect: '/login',
      component: Login,
    },
    {
      path: '/login',
      component: Login,
      name: 'login',
    },
    // {
    //   path:'/',
    //   component:Layout,
    //   redirect: '/login',
    //   name:'login',
    //   children:[
    //     { path: '/login', component: Login }
    //   ]      
    // },
    {
      path: '/index',
      name: 'index',
      title: '首页',
      component: Layout,
      // redirect: '/projectList',
      children: [{
        path: '/index',
        name: 'projectList',
        title: '首页',
        component: ProjectList
      }]
    },
    // {
    //   path: '/createProject',
    //   // redirect: '/createProject',
    //   name:'createProject',
    //   title: '创建项目',
    //   component: Layout,
    //   children: [{
    //      path: 'createProject', 
    //      name: 'createProject',
    //      title: '创建项目',
    //      component: CreateProject }]
    // },
    {
      path: '/taskList',

      name: 'taskList',
      title: '任务详情',
      component: Layout,
      // redirect: '/taskList',
      children: [
        {
          path: '/taskList',
          name: 'taskList',
          title: '任务详情',
          component: TaskList,
          // redirect: '/taskContent',
          children: [
            {
              path: '/taskContent',
              name: 'taskContent',
              title: '任务详情',
              component: TaskContent
            },
            {
              path: '/createTask',
              name: 'createTask',
              title: '创建任务',
              component: CreateTask
            }
          ]
        },
      ]
    },
    {
      path: '/admin',
      name: 'admin',
      title: '人员列表',
      component: Layout,
      redirect: '/admin/userList',
      children: [
        {
          path: '/admin/userList',
          name: '/admin/userList',
          title: '人员列表',
          component: AdmLayout,
          // redirect: '/admin/userList',
          children: [
            {
              path: '/admin/userList',
              name: '/admin/userList',
              title: '用户列表',
              component: UserList
            },
            {
              path: '/admin/createUser',
              name: '/admin/createUser',
              title: '创建用户',
              component: CreateUser
            },
            {
              path: '/admin/projectList',
              name: '/admin/projectList',
              title: '管理员项目列表',
              component: AdmProjectList
            },
          ]
        },
      ]
    },

  ]
})
router.beforeEach(async (to, from, next) => {
  if(to.path==='/login') return next();
  if(to.path.indexOf('/admin')!=-1){
    let res =await api.internal.isRootAdmin();
    if(res.data.isRootAdmin) return next();
    return next({
      path:from.path,
      query:from.path
    })
  }
  await api.internal.isLogin();
  next();
});

export default router