webpackJsonp([1],{

/***/ "5yBN":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "Db8+":
/***/ (function(module, exports, __webpack_require__) {

!function (t, e) {
   true ? module.exports = e() : "function" == typeof define && define.amd ? define([], e) : "object" == typeof exports ? exports.io = e() : t.io = e();
}(this, function () {
  return function (t) {
    function e(r) {
      if (n[r]) return n[r].exports;
      var o = n[r] = {
        exports: {},
        id: r,
        loaded: !1
      };
      return t[r].call(o.exports, o, o.exports, e), o.loaded = !0, o.exports;
    }
    var n = {};
    return e.m = t, e.c = n, e.p = "", e(0);
  }([function (t, e, n) {
    "use strict";

    function r(t, e) {
      "object" === ("undefined" == typeof t ? "undefined" : o(t)) && (e = t, t = void 0), e = e || {};
      var n,
          r = i(t),
          s = r.source,
          u = r.id,
          h = r.path,
          f = p[u] && h in p[u].nsps,
          l = e.forceNew || e["force new connection"] || !1 === e.multiplex || f;
      return l ? (c("ignoring socket cache for %s", s), n = a(s, e)) : (p[u] || (c("new io instance for %s", s), p[u] = a(s, e)), n = p[u]), r.query && !e.query && (e.query = r.query), n.socket(r.path, e);
    }
    var o = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (t) {
      return typeof t;
    } : function (t) {
      return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t;
    },
        i = n(1),
        s = n(7),
        a = n(13),
        c = n(3)("socket.io-client");
    t.exports = e = r;
    var p = e.managers = {};
    e.protocol = s.protocol, e.connect = r, e.Manager = n(13), e.Socket = n(39);
  }, function (t, e, n) {
    (function (e) {
      "use strict";

      function r(t, n) {
        var r = t;
        n = n || e.location, null == t && (t = n.protocol + "//" + n.host), "string" == typeof t && ("/" === t.charAt(0) && (t = "/" === t.charAt(1) ? n.protocol + t : n.host + t), /^(https?|wss?):\/\//.test(t) || (i("protocol-less url %s", t), t = "undefined" != typeof n ? n.protocol + "//" + t : "https://" + t), i("parse %s", t), r = o(t)), r.port || (/^(http|ws)$/.test(r.protocol) ? r.port = "80" : /^(http|ws)s$/.test(r.protocol) && (r.port = "443")), r.path = r.path || "/";
        var s = r.host.indexOf(":") !== -1,
            a = s ? "[" + r.host + "]" : r.host;
        return r.id = r.protocol + "://" + a + ":" + r.port, r.href = r.protocol + "://" + a + (n && n.port === r.port ? "" : ":" + r.port), r;
      }
      var o = n(2),
          i = n(3)("socket.io-client:url");
      t.exports = r;
    }).call(e, function () {
      return this;
    }());
  }, function (t, e) {
    var n = /^(?:(?![^:@]+:[^:@\/]*@)(http|https|ws|wss):\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?((?:[a-f0-9]{0,4}:){2,7}[a-f0-9]{0,4}|[^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/,
        r = ["source", "protocol", "authority", "userInfo", "user", "password", "host", "port", "relative", "path", "directory", "file", "query", "anchor"];
    t.exports = function (t) {
      var e = t,
          o = t.indexOf("["),
          i = t.indexOf("]");
      o != -1 && i != -1 && (t = t.substring(0, o) + t.substring(o, i).replace(/:/g, ";") + t.substring(i, t.length));
      for (var s = n.exec(t || ""), a = {}, c = 14; c--;) a[r[c]] = s[c] || "";
      return o != -1 && i != -1 && (a.source = e, a.host = a.host.substring(1, a.host.length - 1).replace(/;/g, ":"), a.authority = a.authority.replace("[", "").replace("]", "").replace(/;/g, ":"), a.ipv6uri = !0), a;
    };
  }, function (t, e, n) {
    (function (r) {
      function o() {
        return !("undefined" == typeof window || !window.process || "renderer" !== window.process.type) || "undefined" != typeof document && document.documentElement && document.documentElement.style && document.documentElement.style.WebkitAppearance || "undefined" != typeof window && window.console && (window.console.firebug || window.console.exception && window.console.table) || "undefined" != typeof navigator && navigator.userAgent && navigator.userAgent.toLowerCase().match(/firefox\/(\d+)/) && parseInt(RegExp.$1, 10) >= 31 || "undefined" != typeof navigator && navigator.userAgent && navigator.userAgent.toLowerCase().match(/applewebkit\/(\d+)/);
      }

      function i(t) {
        var n = this.useColors;
        if (t[0] = (n ? "%c" : "") + this.namespace + (n ? " %c" : " ") + t[0] + (n ? "%c " : " ") + "+" + e.humanize(this.diff), n) {
          var r = "color: " + this.color;
          t.splice(1, 0, r, "color: inherit");
          var o = 0,
              i = 0;
          t[0].replace(/%[a-zA-Z%]/g, function (t) {
            "%%" !== t && (o++, "%c" === t && (i = o));
          }), t.splice(i, 0, r);
        }
      }

      function s() {
        return "object" == typeof console && console.log && Function.prototype.apply.call(console.log, console, arguments);
      }

      function a(t) {
        try {
          null == t ? e.storage.removeItem("debug") : e.storage.debug = t;
        } catch (n) {}
      }

      function c() {
        var t;
        try {
          t = e.storage.debug;
        } catch (n) {}
        return !t && "undefined" != typeof r && "env" in r && (t = r.env.DEBUG), t;
      }

      function p() {
        try {
          return window.localStorage;
        } catch (t) {}
      }
      e = t.exports = n(5), e.log = s, e.formatArgs = i, e.save = a, e.load = c, e.useColors = o, e.storage = "undefined" != typeof chrome && "undefined" != typeof chrome.storage ? chrome.storage.local : p(), e.colors = ["lightseagreen", "forestgreen", "goldenrod", "dodgerblue", "darkorchid", "crimson"], e.formatters.j = function (t) {
        try {
          return JSON.stringify(t);
        } catch (e) {
          return "[UnexpectedJSONParseError]: " + e.message;
        }
      }, e.enable(c());
    }).call(e, n(4));
  }, function (t, e) {
    function n() {
      throw new Error("setTimeout has not been defined");
    }

    function r() {
      throw new Error("clearTimeout has not been defined");
    }

    function o(t) {
      if (u === setTimeout) return setTimeout(t, 0);
      if ((u === n || !u) && setTimeout) return u = setTimeout, setTimeout(t, 0);
      try {
        return u(t, 0);
      } catch (e) {
        try {
          return u.call(null, t, 0);
        } catch (e) {
          return u.call(this, t, 0);
        }
      }
    }

    function i(t) {
      if (h === clearTimeout) return clearTimeout(t);
      if ((h === r || !h) && clearTimeout) return h = clearTimeout, clearTimeout(t);
      try {
        return h(t);
      } catch (e) {
        try {
          return h.call(null, t);
        } catch (e) {
          return h.call(this, t);
        }
      }
    }

    function s() {
      y && l && (y = !1, l.length ? d = l.concat(d) : m = -1, d.length && a());
    }

    function a() {
      if (!y) {
        var t = o(s);
        y = !0;
        for (var e = d.length; e;) {
          for (l = d, d = []; ++m < e;) l && l[m].run();
          m = -1, e = d.length;
        }
        l = null, y = !1, i(t);
      }
    }

    function c(t, e) {
      this.fun = t, this.array = e;
    }

    function p() {}
    var u,
        h,
        f = t.exports = {};
    !function () {
      try {
        u = "function" == typeof setTimeout ? setTimeout : n;
      } catch (t) {
        u = n;
      }
      try {
        h = "function" == typeof clearTimeout ? clearTimeout : r;
      } catch (t) {
        h = r;
      }
    }();
    var l,
        d = [],
        y = !1,
        m = -1;
    f.nextTick = function (t) {
      var e = new Array(arguments.length - 1);
      if (arguments.length > 1) for (var n = 1; n < arguments.length; n++) e[n - 1] = arguments[n];
      d.push(new c(t, e)), 1 !== d.length || y || o(a);
    }, c.prototype.run = function () {
      this.fun.apply(null, this.array);
    }, f.title = "browser", f.browser = !0, f.env = {}, f.argv = [], f.version = "", f.versions = {}, f.on = p, f.addListener = p, f.once = p, f.off = p, f.removeListener = p, f.removeAllListeners = p, f.emit = p, f.prependListener = p, f.prependOnceListener = p, f.listeners = function (t) {
      return [];
    }, f.binding = function (t) {
      throw new Error("process.binding is not supported");
    }, f.cwd = function () {
      return "/";
    }, f.chdir = function (t) {
      throw new Error("process.chdir is not supported");
    }, f.umask = function () {
      return 0;
    };
  }, function (t, e, n) {
    function r(t) {
      var n,
          r = 0;
      for (n in t) r = (r << 5) - r + t.charCodeAt(n), r |= 0;
      return e.colors[Math.abs(r) % e.colors.length];
    }

    function o(t) {
      function n() {
        if (n.enabled) {
          var t = n,
              r = +new Date(),
              o = r - (p || r);
          t.diff = o, t.prev = p, t.curr = r, p = r;
          for (var i = new Array(arguments.length), s = 0; s < i.length; s++) i[s] = arguments[s];
          i[0] = e.coerce(i[0]), "string" != typeof i[0] && i.unshift("%O");
          var a = 0;
          i[0] = i[0].replace(/%([a-zA-Z%])/g, function (n, r) {
            if ("%%" === n) return n;
            a++;
            var o = e.formatters[r];
            if ("function" == typeof o) {
              var s = i[a];
              n = o.call(t, s), i.splice(a, 1), a--;
            }
            return n;
          }), e.formatArgs.call(t, i);
          var c = n.log || e.log || console.log.bind(console);
          c.apply(t, i);
        }
      }
      return n.namespace = t, n.enabled = e.enabled(t), n.useColors = e.useColors(), n.color = r(t), "function" == typeof e.init && e.init(n), n;
    }

    function i(t) {
      e.save(t), e.names = [], e.skips = [];
      for (var n = ("string" == typeof t ? t : "").split(/[\s,]+/), r = n.length, o = 0; o < r; o++) n[o] && (t = n[o].replace(/\*/g, ".*?"), "-" === t[0] ? e.skips.push(new RegExp("^" + t.substr(1) + "$")) : e.names.push(new RegExp("^" + t + "$")));
    }

    function s() {
      e.enable("");
    }

    function a(t) {
      var n, r;
      for (n = 0, r = e.skips.length; n < r; n++) if (e.skips[n].test(t)) return !1;
      for (n = 0, r = e.names.length; n < r; n++) if (e.names[n].test(t)) return !0;
      return !1;
    }

    function c(t) {
      return t instanceof Error ? t.stack || t.message : t;
    }
    e = t.exports = o.debug = o["default"] = o, e.coerce = c, e.disable = s, e.enable = i, e.enabled = a, e.humanize = n(6), e.names = [], e.skips = [], e.formatters = {};
    var p;
  }, function (t, e) {
    function n(t) {
      if (t = String(t), !(t.length > 100)) {
        var e = /^((?:\d+)?\.?\d+) *(milliseconds?|msecs?|ms|seconds?|secs?|s|minutes?|mins?|m|hours?|hrs?|h|days?|d|years?|yrs?|y)?$/i.exec(t);
        if (e) {
          var n = parseFloat(e[1]),
              r = (e[2] || "ms").toLowerCase();
          switch (r) {
            case "years":
            case "year":
            case "yrs":
            case "yr":
            case "y":
              return n * u;
            case "days":
            case "day":
            case "d":
              return n * p;
            case "hours":
            case "hour":
            case "hrs":
            case "hr":
            case "h":
              return n * c;
            case "minutes":
            case "minute":
            case "mins":
            case "min":
            case "m":
              return n * a;
            case "seconds":
            case "second":
            case "secs":
            case "sec":
            case "s":
              return n * s;
            case "milliseconds":
            case "millisecond":
            case "msecs":
            case "msec":
            case "ms":
              return n;
            default:
              return;
          }
        }
      }
    }

    function r(t) {
      return t >= p ? Math.round(t / p) + "d" : t >= c ? Math.round(t / c) + "h" : t >= a ? Math.round(t / a) + "m" : t >= s ? Math.round(t / s) + "s" : t + "ms";
    }

    function o(t) {
      return i(t, p, "day") || i(t, c, "hour") || i(t, a, "minute") || i(t, s, "second") || t + " ms";
    }

    function i(t, e, n) {
      if (!(t < e)) return t < 1.5 * e ? Math.floor(t / e) + " " + n : Math.ceil(t / e) + " " + n + "s";
    }
    var s = 1e3,
        a = 60 * s,
        c = 60 * a,
        p = 24 * c,
        u = 365.25 * p;
    t.exports = function (t, e) {
      e = e || {};
      var i = typeof t;
      if ("string" === i && t.length > 0) return n(t);
      if ("number" === i && isNaN(t) === !1) return e["long"] ? o(t) : r(t);
      throw new Error("val is not a non-empty string or a valid number. val=" + JSON.stringify(t));
    };
  }, function (t, e, n) {
    function r() {}

    function o(t) {
      var n = "" + t.type;
      return e.BINARY_EVENT !== t.type && e.BINARY_ACK !== t.type || (n += t.attachments + "-"), t.nsp && "/" !== t.nsp && (n += t.nsp + ","), null != t.id && (n += t.id), null != t.data && (n += JSON.stringify(t.data)), h("encoded %j as %s", t, n), n;
    }

    function i(t, e) {
      function n(t) {
        var n = d.deconstructPacket(t),
            r = o(n.packet),
            i = n.buffers;
        i.unshift(r), e(i);
      }
      d.removeBlobs(t, n);
    }

    function s() {
      this.reconstructor = null;
    }

    function a(t) {
      var n = 0,
          r = {
        type: Number(t.charAt(0))
      };
      if (null == e.types[r.type]) return u();
      if (e.BINARY_EVENT === r.type || e.BINARY_ACK === r.type) {
        for (var o = ""; "-" !== t.charAt(++n) && (o += t.charAt(n), n != t.length););
        if (o != Number(o) || "-" !== t.charAt(n)) throw new Error("Illegal attachments");
        r.attachments = Number(o);
      }
      if ("/" === t.charAt(n + 1)) for (r.nsp = ""; ++n;) {
        var i = t.charAt(n);
        if ("," === i) break;
        if (r.nsp += i, n === t.length) break;
      } else r.nsp = "/";
      var s = t.charAt(n + 1);
      if ("" !== s && Number(s) == s) {
        for (r.id = ""; ++n;) {
          var i = t.charAt(n);
          if (null == i || Number(i) != i) {
            --n;
            break;
          }
          if (r.id += t.charAt(n), n === t.length) break;
        }
        r.id = Number(r.id);
      }
      return t.charAt(++n) && (r = c(r, t.substr(n))), h("decoded %s as %j", t, r), r;
    }

    function c(t, e) {
      try {
        t.data = JSON.parse(e);
      } catch (n) {
        return u();
      }
      return t;
    }

    function p(t) {
      this.reconPack = t, this.buffers = [];
    }

    function u() {
      return {
        type: e.ERROR,
        data: "parser error"
      };
    }
    var h = n(3)("socket.io-parser"),
        f = n(8),
        l = n(9),
        d = n(11),
        y = n(12);
    e.protocol = 4, e.types = ["CONNECT", "DISCONNECT", "EVENT", "ACK", "ERROR", "BINARY_EVENT", "BINARY_ACK"], e.CONNECT = 0, e.DISCONNECT = 1, e.EVENT = 2, e.ACK = 3, e.ERROR = 4, e.BINARY_EVENT = 5, e.BINARY_ACK = 6, e.Encoder = r, e.Decoder = s, r.prototype.encode = function (t, n) {
      if (t.type !== e.EVENT && t.type !== e.ACK || !l(t.data) || (t.type = t.type === e.EVENT ? e.BINARY_EVENT : e.BINARY_ACK), h("encoding packet %j", t), e.BINARY_EVENT === t.type || e.BINARY_ACK === t.type) i(t, n);else {
        var r = o(t);
        n([r]);
      }
    }, f(s.prototype), s.prototype.add = function (t) {
      var n;
      if ("string" == typeof t) n = a(t), e.BINARY_EVENT === n.type || e.BINARY_ACK === n.type ? (this.reconstructor = new p(n), 0 === this.reconstructor.reconPack.attachments && this.emit("decoded", n)) : this.emit("decoded", n);else {
        if (!y(t) && !t.base64) throw new Error("Unknown type: " + t);
        if (!this.reconstructor) throw new Error("got binary data when not reconstructing a packet");
        n = this.reconstructor.takeBinaryData(t), n && (this.reconstructor = null, this.emit("decoded", n));
      }
    }, s.prototype.destroy = function () {
      this.reconstructor && this.reconstructor.finishedReconstruction();
    }, p.prototype.takeBinaryData = function (t) {
      if (this.buffers.push(t), this.buffers.length === this.reconPack.attachments) {
        var e = d.reconstructPacket(this.reconPack, this.buffers);
        return this.finishedReconstruction(), e;
      }
      return null;
    }, p.prototype.finishedReconstruction = function () {
      this.reconPack = null, this.buffers = [];
    };
  }, function (t, e, n) {
    function r(t) {
      if (t) return o(t);
    }

    function o(t) {
      for (var e in r.prototype) t[e] = r.prototype[e];
      return t;
    }
    t.exports = r, r.prototype.on = r.prototype.addEventListener = function (t, e) {
      return this._callbacks = this._callbacks || {}, (this._callbacks["$" + t] = this._callbacks["$" + t] || []).push(e), this;
    }, r.prototype.once = function (t, e) {
      function n() {
        this.off(t, n), e.apply(this, arguments);
      }
      return n.fn = e, this.on(t, n), this;
    }, r.prototype.off = r.prototype.removeListener = r.prototype.removeAllListeners = r.prototype.removeEventListener = function (t, e) {
      if (this._callbacks = this._callbacks || {}, 0 == arguments.length) return this._callbacks = {}, this;
      var n = this._callbacks["$" + t];
      if (!n) return this;
      if (1 == arguments.length) return delete this._callbacks["$" + t], this;
      for (var r, o = 0; o < n.length; o++) if (r = n[o], r === e || r.fn === e) {
        n.splice(o, 1);
        break;
      }
      return this;
    }, r.prototype.emit = function (t) {
      this._callbacks = this._callbacks || {};
      var e = [].slice.call(arguments, 1),
          n = this._callbacks["$" + t];
      if (n) {
        n = n.slice(0);
        for (var r = 0, o = n.length; r < o; ++r) n[r].apply(this, e);
      }
      return this;
    }, r.prototype.listeners = function (t) {
      return this._callbacks = this._callbacks || {}, this._callbacks["$" + t] || [];
    }, r.prototype.hasListeners = function (t) {
      return !!this.listeners(t).length;
    };
  }, function (t, e, n) {
    (function (e) {
      function r(t) {
        if (!t || "object" != typeof t) return !1;
        if (o(t)) {
          for (var n = 0, i = t.length; n < i; n++) if (r(t[n])) return !0;
          return !1;
        }
        if ("function" == typeof e.Buffer && e.Buffer.isBuffer && e.Buffer.isBuffer(t) || "function" == typeof e.ArrayBuffer && t instanceof ArrayBuffer || s && t instanceof Blob || a && t instanceof File) return !0;
        if (t.toJSON && "function" == typeof t.toJSON && 1 === arguments.length) return r(t.toJSON(), !0);
        for (var c in t) if (Object.prototype.hasOwnProperty.call(t, c) && r(t[c])) return !0;
        return !1;
      }
      var o = n(10),
          i = Object.prototype.toString,
          s = "function" == typeof e.Blob || "[object BlobConstructor]" === i.call(e.Blob),
          a = "function" == typeof e.File || "[object FileConstructor]" === i.call(e.File);
      t.exports = r;
    }).call(e, function () {
      return this;
    }());
  }, function (t, e) {
    var n = {}.toString;
    t.exports = Array.isArray || function (t) {
      return "[object Array]" == n.call(t);
    };
  }, function (t, e, n) {
    (function (t) {
      function r(t, e) {
        if (!t) return t;
        if (s(t)) {
          var n = {
            _placeholder: !0,
            num: e.length
          };
          return e.push(t), n;
        }
        if (i(t)) {
          for (var o = new Array(t.length), a = 0; a < t.length; a++) o[a] = r(t[a], e);
          return o;
        }
        if ("object" == typeof t && !(t instanceof Date)) {
          var o = {};
          for (var c in t) o[c] = r(t[c], e);
          return o;
        }
        return t;
      }

      function o(t, e) {
        if (!t) return t;
        if (t && t._placeholder) return e[t.num];
        if (i(t)) for (var n = 0; n < t.length; n++) t[n] = o(t[n], e);else if ("object" == typeof t) for (var r in t) t[r] = o(t[r], e);
        return t;
      }
      var i = n(10),
          s = n(12),
          a = Object.prototype.toString,
          c = "function" == typeof t.Blob || "[object BlobConstructor]" === a.call(t.Blob),
          p = "function" == typeof t.File || "[object FileConstructor]" === a.call(t.File);
      e.deconstructPacket = function (t) {
        var e = [],
            n = t.data,
            o = t;
        return o.data = r(n, e), o.attachments = e.length, {
          packet: o,
          buffers: e
        };
      }, e.reconstructPacket = function (t, e) {
        return t.data = o(t.data, e), t.attachments = void 0, t;
      }, e.removeBlobs = function (t, e) {
        function n(t, a, u) {
          if (!t) return t;
          if (c && t instanceof Blob || p && t instanceof File) {
            r++;
            var h = new FileReader();
            h.onload = function () {
              u ? u[a] = this.result : o = this.result, --r || e(o);
            }, h.readAsArrayBuffer(t);
          } else if (i(t)) for (var f = 0; f < t.length; f++) n(t[f], f, t);else if ("object" == typeof t && !s(t)) for (var l in t) n(t[l], l, t);
        }
        var r = 0,
            o = t;
        n(o), r || e(o);
      };
    }).call(e, function () {
      return this;
    }());
  }, function (t, e) {
    (function (e) {
      function n(t) {
        return e.Buffer && e.Buffer.isBuffer(t) || e.ArrayBuffer && t instanceof ArrayBuffer;
      }
      t.exports = n;
    }).call(e, function () {
      return this;
    }());
  }, function (t, e, n) {
    "use strict";

    function r(t, e) {
      if (!(this instanceof r)) return new r(t, e);
      t && "object" === ("undefined" == typeof t ? "undefined" : o(t)) && (e = t, t = void 0), e = e || {}, e.path = e.path || "/socket.io", this.nsps = {}, this.subs = [], this.opts = e, this.reconnection(e.reconnection !== !1), this.reconnectionAttempts(e.reconnectionAttempts || 1 / 0), this.reconnectionDelay(e.reconnectionDelay || 1e3), this.reconnectionDelayMax(e.reconnectionDelayMax || 5e3), this.randomizationFactor(e.randomizationFactor || .5), this.backoff = new l({
        min: this.reconnectionDelay(),
        max: this.reconnectionDelayMax(),
        jitter: this.randomizationFactor()
      }), this.timeout(null == e.timeout ? 2e4 : e.timeout), this.readyState = "closed", this.uri = t, this.connecting = [], this.lastPing = null, this.encoding = !1, this.packetBuffer = [];
      var n = e.parser || c;
      this.encoder = new n.Encoder(), this.decoder = new n.Decoder(), this.autoConnect = e.autoConnect !== !1, this.autoConnect && this.open();
    }
    var o = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (t) {
      return typeof t;
    } : function (t) {
      return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t;
    },
        i = n(14),
        s = n(39),
        a = n(8),
        c = n(7),
        p = n(41),
        u = n(42),
        h = n(3)("socket.io-client:manager"),
        f = n(37),
        l = n(43),
        d = Object.prototype.hasOwnProperty;
    t.exports = r, r.prototype.emitAll = function () {
      this.emit.apply(this, arguments);
      for (var t in this.nsps) d.call(this.nsps, t) && this.nsps[t].emit.apply(this.nsps[t], arguments);
    }, r.prototype.updateSocketIds = function () {
      for (var t in this.nsps) d.call(this.nsps, t) && (this.nsps[t].id = this.generateId(t));
    }, r.prototype.generateId = function (t) {
      return ("/" === t ? "" : t + "#") + this.engine.id;
    }, a(r.prototype), r.prototype.reconnection = function (t) {
      return arguments.length ? (this._reconnection = !!t, this) : this._reconnection;
    }, r.prototype.reconnectionAttempts = function (t) {
      return arguments.length ? (this._reconnectionAttempts = t, this) : this._reconnectionAttempts;
    }, r.prototype.reconnectionDelay = function (t) {
      return arguments.length ? (this._reconnectionDelay = t, this.backoff && this.backoff.setMin(t), this) : this._reconnectionDelay;
    }, r.prototype.randomizationFactor = function (t) {
      return arguments.length ? (this._randomizationFactor = t, this.backoff && this.backoff.setJitter(t), this) : this._randomizationFactor;
    }, r.prototype.reconnectionDelayMax = function (t) {
      return arguments.length ? (this._reconnectionDelayMax = t, this.backoff && this.backoff.setMax(t), this) : this._reconnectionDelayMax;
    }, r.prototype.timeout = function (t) {
      return arguments.length ? (this._timeout = t, this) : this._timeout;
    }, r.prototype.maybeReconnectOnOpen = function () {
      !this.reconnecting && this._reconnection && 0 === this.backoff.attempts && this.reconnect();
    }, r.prototype.open = r.prototype.connect = function (t, e) {
      if (h("readyState %s", this.readyState), ~this.readyState.indexOf("open")) return this;
      h("opening %s", this.uri), this.engine = i(this.uri, this.opts);
      var n = this.engine,
          r = this;
      this.readyState = "opening", this.skipReconnect = !1;
      var o = p(n, "open", function () {
        r.onopen(), t && t();
      }),
          s = p(n, "error", function (e) {
        if (h("connect_error"), r.cleanup(), r.readyState = "closed", r.emitAll("connect_error", e), t) {
          var n = new Error("Connection error");
          n.data = e, t(n);
        } else r.maybeReconnectOnOpen();
      });
      if (!1 !== this._timeout) {
        var a = this._timeout;
        h("connect attempt will timeout after %d", a);
        var c = setTimeout(function () {
          h("connect attempt timed out after %d", a), o.destroy(), n.close(), n.emit("error", "timeout"), r.emitAll("connect_timeout", a);
        }, a);
        this.subs.push({
          destroy: function () {
            clearTimeout(c);
          }
        });
      }
      return this.subs.push(o), this.subs.push(s), this;
    }, r.prototype.onopen = function () {
      h("open"), this.cleanup(), this.readyState = "open", this.emit("open");
      var t = this.engine;
      this.subs.push(p(t, "data", u(this, "ondata"))), this.subs.push(p(t, "ping", u(this, "onping"))), this.subs.push(p(t, "pong", u(this, "onpong"))), this.subs.push(p(t, "error", u(this, "onerror"))), this.subs.push(p(t, "close", u(this, "onclose"))), this.subs.push(p(this.decoder, "decoded", u(this, "ondecoded")));
    }, r.prototype.onping = function () {
      this.lastPing = new Date(), this.emitAll("ping");
    }, r.prototype.onpong = function () {
      this.emitAll("pong", new Date() - this.lastPing);
    }, r.prototype.ondata = function (t) {
      this.decoder.add(t);
    }, r.prototype.ondecoded = function (t) {
      this.emit("packet", t);
    }, r.prototype.onerror = function (t) {
      h("error", t), this.emitAll("error", t);
    }, r.prototype.socket = function (t, e) {
      function n() {
        ~f(o.connecting, r) || o.connecting.push(r);
      }
      var r = this.nsps[t];
      if (!r) {
        r = new s(this, t, e), this.nsps[t] = r;
        var o = this;
        r.on("connecting", n), r.on("connect", function () {
          r.id = o.generateId(t);
        }), this.autoConnect && n();
      }
      return r;
    }, r.prototype.destroy = function (t) {
      var e = f(this.connecting, t);
      ~e && this.connecting.splice(e, 1), this.connecting.length || this.close();
    }, r.prototype.packet = function (t) {
      h("writing packet %j", t);
      var e = this;
      t.query && 0 === t.type && (t.nsp += "?" + t.query), e.encoding ? e.packetBuffer.push(t) : (e.encoding = !0, this.encoder.encode(t, function (n) {
        for (var r = 0; r < n.length; r++) e.engine.write(n[r], t.options);
        e.encoding = !1, e.processPacketQueue();
      }));
    }, r.prototype.processPacketQueue = function () {
      if (this.packetBuffer.length > 0 && !this.encoding) {
        var t = this.packetBuffer.shift();
        this.packet(t);
      }
    }, r.prototype.cleanup = function () {
      h("cleanup");
      for (var t = this.subs.length, e = 0; e < t; e++) {
        var n = this.subs.shift();
        n.destroy();
      }
      this.packetBuffer = [], this.encoding = !1, this.lastPing = null, this.decoder.destroy();
    }, r.prototype.close = r.prototype.disconnect = function () {
      h("disconnect"), this.skipReconnect = !0, this.reconnecting = !1, "opening" === this.readyState && this.cleanup(), this.backoff.reset(), this.readyState = "closed", this.engine && this.engine.close();
    }, r.prototype.onclose = function (t) {
      h("onclose"), this.cleanup(), this.backoff.reset(), this.readyState = "closed", this.emit("close", t), this._reconnection && !this.skipReconnect && this.reconnect();
    }, r.prototype.reconnect = function () {
      if (this.reconnecting || this.skipReconnect) return this;
      var t = this;
      if (this.backoff.attempts >= this._reconnectionAttempts) h("reconnect failed"), this.backoff.reset(), this.emitAll("reconnect_failed"), this.reconnecting = !1;else {
        var e = this.backoff.duration();
        h("will wait %dms before reconnect attempt", e), this.reconnecting = !0;
        var n = setTimeout(function () {
          t.skipReconnect || (h("attempting reconnect"), t.emitAll("reconnect_attempt", t.backoff.attempts), t.emitAll("reconnecting", t.backoff.attempts), t.skipReconnect || t.open(function (e) {
            e ? (h("reconnect attempt error"), t.reconnecting = !1, t.reconnect(), t.emitAll("reconnect_error", e.data)) : (h("reconnect success"), t.onreconnect());
          }));
        }, e);
        this.subs.push({
          destroy: function () {
            clearTimeout(n);
          }
        });
      }
    }, r.prototype.onreconnect = function () {
      var t = this.backoff.attempts;
      this.reconnecting = !1, this.backoff.reset(), this.updateSocketIds(), this.emitAll("reconnect", t);
    };
  }, function (t, e, n) {
    t.exports = n(15);
  }, function (t, e, n) {
    t.exports = n(16), t.exports.parser = n(23);
  }, function (t, e, n) {
    (function (e) {
      function r(t, n) {
        if (!(this instanceof r)) return new r(t, n);
        n = n || {}, t && "object" == typeof t && (n = t, t = null), t ? (t = u(t), n.hostname = t.host, n.secure = "https" === t.protocol || "wss" === t.protocol, n.port = t.port, t.query && (n.query = t.query)) : n.host && (n.hostname = u(n.host).host), this.secure = null != n.secure ? n.secure : e.location && "https:" === location.protocol, n.hostname && !n.port && (n.port = this.secure ? "443" : "80"), this.agent = n.agent || !1, this.hostname = n.hostname || (e.location ? location.hostname : "localhost"), this.port = n.port || (e.location && location.port ? location.port : this.secure ? 443 : 80), this.query = n.query || {}, "string" == typeof this.query && (this.query = f.decode(this.query)), this.upgrade = !1 !== n.upgrade, this.path = (n.path || "/engine.io").replace(/\/$/, "") + "/", this.forceJSONP = !!n.forceJSONP, this.jsonp = !1 !== n.jsonp, this.forceBase64 = !!n.forceBase64, this.enablesXDR = !!n.enablesXDR, this.timestampParam = n.timestampParam || "t", this.timestampRequests = n.timestampRequests, this.transports = n.transports || ["polling", "websocket"], this.transportOptions = n.transportOptions || {}, this.readyState = "", this.writeBuffer = [], this.prevBufferLen = 0, this.policyPort = n.policyPort || 843, this.rememberUpgrade = n.rememberUpgrade || !1, this.binaryType = null, this.onlyBinaryUpgrades = n.onlyBinaryUpgrades, this.perMessageDeflate = !1 !== n.perMessageDeflate && (n.perMessageDeflate || {}), !0 === this.perMessageDeflate && (this.perMessageDeflate = {}), this.perMessageDeflate && null == this.perMessageDeflate.threshold && (this.perMessageDeflate.threshold = 1024), this.pfx = n.pfx || null, this.key = n.key || null, this.passphrase = n.passphrase || null, this.cert = n.cert || null, this.ca = n.ca || null, this.ciphers = n.ciphers || null, this.rejectUnauthorized = void 0 === n.rejectUnauthorized || n.rejectUnauthorized, this.forceNode = !!n.forceNode;
        var o = "object" == typeof e && e;
        o.global === o && (n.extraHeaders && Object.keys(n.extraHeaders).length > 0 && (this.extraHeaders = n.extraHeaders), n.localAddress && (this.localAddress = n.localAddress)), this.id = null, this.upgrades = null, this.pingInterval = null, this.pingTimeout = null, this.pingIntervalTimer = null, this.pingTimeoutTimer = null, this.open();
      }

      function o(t) {
        var e = {};
        for (var n in t) t.hasOwnProperty(n) && (e[n] = t[n]);
        return e;
      }
      var i = n(17),
          s = n(8),
          a = n(3)("engine.io-client:socket"),
          c = n(37),
          p = n(23),
          u = n(2),
          h = n(38),
          f = n(31);
      t.exports = r, r.priorWebsocketSuccess = !1, s(r.prototype), r.protocol = p.protocol, r.Socket = r, r.Transport = n(22), r.transports = n(17), r.parser = n(23), r.prototype.createTransport = function (t) {
        a('creating transport "%s"', t);
        var e = o(this.query);
        e.EIO = p.protocol, e.transport = t;
        var n = this.transportOptions[t] || {};
        this.id && (e.sid = this.id);
        var r = new i[t]({
          query: e,
          socket: this,
          agent: n.agent || this.agent,
          hostname: n.hostname || this.hostname,
          port: n.port || this.port,
          secure: n.secure || this.secure,
          path: n.path || this.path,
          forceJSONP: n.forceJSONP || this.forceJSONP,
          jsonp: n.jsonp || this.jsonp,
          forceBase64: n.forceBase64 || this.forceBase64,
          enablesXDR: n.enablesXDR || this.enablesXDR,
          timestampRequests: n.timestampRequests || this.timestampRequests,
          timestampParam: n.timestampParam || this.timestampParam,
          policyPort: n.policyPort || this.policyPort,
          pfx: n.pfx || this.pfx,
          key: n.key || this.key,
          passphrase: n.passphrase || this.passphrase,
          cert: n.cert || this.cert,
          ca: n.ca || this.ca,
          ciphers: n.ciphers || this.ciphers,
          rejectUnauthorized: n.rejectUnauthorized || this.rejectUnauthorized,
          perMessageDeflate: n.perMessageDeflate || this.perMessageDeflate,
          extraHeaders: n.extraHeaders || this.extraHeaders,
          forceNode: n.forceNode || this.forceNode,
          localAddress: n.localAddress || this.localAddress,
          requestTimeout: n.requestTimeout || this.requestTimeout,
          protocols: n.protocols || void 0
        });
        return r;
      }, r.prototype.open = function () {
        var t;
        if (this.rememberUpgrade && r.priorWebsocketSuccess && this.transports.indexOf("websocket") !== -1) t = "websocket";else {
          if (0 === this.transports.length) {
            var e = this;
            return void setTimeout(function () {
              e.emit("error", "No transports available");
            }, 0);
          }
          t = this.transports[0];
        }
        this.readyState = "opening";
        try {
          t = this.createTransport(t);
        } catch (n) {
          return this.transports.shift(), void this.open();
        }
        t.open(), this.setTransport(t);
      }, r.prototype.setTransport = function (t) {
        a("setting transport %s", t.name);
        var e = this;
        this.transport && (a("clearing existing transport %s", this.transport.name), this.transport.removeAllListeners()), this.transport = t, t.on("drain", function () {
          e.onDrain();
        }).on("packet", function (t) {
          e.onPacket(t);
        }).on("error", function (t) {
          e.onError(t);
        }).on("close", function () {
          e.onClose("transport close");
        });
      }, r.prototype.probe = function (t) {
        function e() {
          if (f.onlyBinaryUpgrades) {
            var e = !this.supportsBinary && f.transport.supportsBinary;
            h = h || e;
          }
          h || (a('probe transport "%s" opened', t), u.send([{
            type: "ping",
            data: "probe"
          }]), u.once("packet", function (e) {
            if (!h) if ("pong" === e.type && "probe" === e.data) {
              if (a('probe transport "%s" pong', t), f.upgrading = !0, f.emit("upgrading", u), !u) return;
              r.priorWebsocketSuccess = "websocket" === u.name, a('pausing current transport "%s"', f.transport.name), f.transport.pause(function () {
                h || "closed" !== f.readyState && (a("changing transport and sending upgrade packet"), p(), f.setTransport(u), u.send([{
                  type: "upgrade"
                }]), f.emit("upgrade", u), u = null, f.upgrading = !1, f.flush());
              });
            } else {
              a('probe transport "%s" failed', t);
              var n = new Error("probe error");
              n.transport = u.name, f.emit("upgradeError", n);
            }
          }));
        }

        function n() {
          h || (h = !0, p(), u.close(), u = null);
        }

        function o(e) {
          var r = new Error("probe error: " + e);
          r.transport = u.name, n(), a('probe transport "%s" failed because of error: %s', t, e), f.emit("upgradeError", r);
        }

        function i() {
          o("transport closed");
        }

        function s() {
          o("socket closed");
        }

        function c(t) {
          u && t.name !== u.name && (a('"%s" works - aborting "%s"', t.name, u.name), n());
        }

        function p() {
          u.removeListener("open", e), u.removeListener("error", o), u.removeListener("close", i), f.removeListener("close", s), f.removeListener("upgrading", c);
        }
        a('probing transport "%s"', t);
        var u = this.createTransport(t, {
          probe: 1
        }),
            h = !1,
            f = this;
        r.priorWebsocketSuccess = !1, u.once("open", e), u.once("error", o), u.once("close", i), this.once("close", s), this.once("upgrading", c), u.open();
      }, r.prototype.onOpen = function () {
        if (a("socket open"), this.readyState = "open", r.priorWebsocketSuccess = "websocket" === this.transport.name, this.emit("open"), this.flush(), "open" === this.readyState && this.upgrade && this.transport.pause) {
          a("starting upgrade probes");
          for (var t = 0, e = this.upgrades.length; t < e; t++) this.probe(this.upgrades[t]);
        }
      }, r.prototype.onPacket = function (t) {
        if ("opening" === this.readyState || "open" === this.readyState || "closing" === this.readyState) switch (a('socket receive: type "%s", data "%s"', t.type, t.data), this.emit("packet", t), this.emit("heartbeat"), t.type) {
          case "open":
            this.onHandshake(h(t.data));
            break;
          case "pong":
            this.setPing(), this.emit("pong");
            break;
          case "error":
            var e = new Error("server error");
            e.code = t.data, this.onError(e);
            break;
          case "message":
            this.emit("data", t.data), this.emit("message", t.data);
        } else a('packet received with socket readyState "%s"', this.readyState);
      }, r.prototype.onHandshake = function (t) {
        this.emit("handshake", t), this.id = t.sid, this.transport.query.sid = t.sid, this.upgrades = this.filterUpgrades(t.upgrades), this.pingInterval = t.pingInterval, this.pingTimeout = t.pingTimeout, this.onOpen(), "closed" !== this.readyState && (this.setPing(), this.removeListener("heartbeat", this.onHeartbeat), this.on("heartbeat", this.onHeartbeat));
      }, r.prototype.onHeartbeat = function (t) {
        clearTimeout(this.pingTimeoutTimer);
        var e = this;
        e.pingTimeoutTimer = setTimeout(function () {
          "closed" !== e.readyState && e.onClose("ping timeout");
        }, t || e.pingInterval + e.pingTimeout);
      }, r.prototype.setPing = function () {
        var t = this;
        clearTimeout(t.pingIntervalTimer), t.pingIntervalTimer = setTimeout(function () {
          a("writing ping packet - expecting pong within %sms", t.pingTimeout), t.ping(), t.onHeartbeat(t.pingTimeout);
        }, t.pingInterval);
      }, r.prototype.ping = function () {
        var t = this;
        this.sendPacket("ping", function () {
          t.emit("ping");
        });
      }, r.prototype.onDrain = function () {
        this.writeBuffer.splice(0, this.prevBufferLen), this.prevBufferLen = 0, 0 === this.writeBuffer.length ? this.emit("drain") : this.flush();
      }, r.prototype.flush = function () {
        "closed" !== this.readyState && this.transport.writable && !this.upgrading && this.writeBuffer.length && (a("flushing %d packets in socket", this.writeBuffer.length), this.transport.send(this.writeBuffer), this.prevBufferLen = this.writeBuffer.length, this.emit("flush"));
      }, r.prototype.write = r.prototype.send = function (t, e, n) {
        return this.sendPacket("message", t, e, n), this;
      }, r.prototype.sendPacket = function (t, e, n, r) {
        if ("function" == typeof e && (r = e, e = void 0), "function" == typeof n && (r = n, n = null), "closing" !== this.readyState && "closed" !== this.readyState) {
          n = n || {}, n.compress = !1 !== n.compress;
          var o = {
            type: t,
            data: e,
            options: n
          };
          this.emit("packetCreate", o), this.writeBuffer.push(o), r && this.once("flush", r), this.flush();
        }
      }, r.prototype.close = function () {
        function t() {
          r.onClose("forced close"), a("socket closing - telling transport to close"), r.transport.close();
        }

        function e() {
          r.removeListener("upgrade", e), r.removeListener("upgradeError", e), t();
        }

        function n() {
          r.once("upgrade", e), r.once("upgradeError", e);
        }
        if ("opening" === this.readyState || "open" === this.readyState) {
          this.readyState = "closing";
          var r = this;
          this.writeBuffer.length ? this.once("drain", function () {
            this.upgrading ? n() : t();
          }) : this.upgrading ? n() : t();
        }
        return this;
      }, r.prototype.onError = function (t) {
        a("socket error %j", t), r.priorWebsocketSuccess = !1, this.emit("error", t), this.onClose("transport error", t);
      }, r.prototype.onClose = function (t, e) {
        if ("opening" === this.readyState || "open" === this.readyState || "closing" === this.readyState) {
          a('socket close with reason: "%s"', t);
          var n = this;
          clearTimeout(this.pingIntervalTimer), clearTimeout(this.pingTimeoutTimer), this.transport.removeAllListeners("close"), this.transport.close(), this.transport.removeAllListeners(), this.readyState = "closed", this.id = null, this.emit("close", t, e), n.writeBuffer = [], n.prevBufferLen = 0;
        }
      }, r.prototype.filterUpgrades = function (t) {
        for (var e = [], n = 0, r = t.length; n < r; n++) ~c(this.transports, t[n]) && e.push(t[n]);
        return e;
      };
    }).call(e, function () {
      return this;
    }());
  }, function (t, e, n) {
    (function (t) {
      function r(e) {
        var n,
            r = !1,
            a = !1,
            c = !1 !== e.jsonp;
        if (t.location) {
          var p = "https:" === location.protocol,
              u = location.port;
          u || (u = p ? 443 : 80), r = e.hostname !== location.hostname || u !== e.port, a = e.secure !== p;
        }
        if (e.xdomain = r, e.xscheme = a, n = new o(e), "open" in n && !e.forceJSONP) return new i(e);
        if (!c) throw new Error("JSONP disabled");
        return new s(e);
      }
      var o = n(18),
          i = n(20),
          s = n(34),
          a = n(35);
      e.polling = r, e.websocket = a;
    }).call(e, function () {
      return this;
    }());
  }, function (t, e, n) {
    (function (e) {
      var r = n(19);
      t.exports = function (t) {
        var n = t.xdomain,
            o = t.xscheme,
            i = t.enablesXDR;
        try {
          if ("undefined" != typeof XMLHttpRequest && (!n || r)) return new XMLHttpRequest();
        } catch (s) {}
        try {
          if ("undefined" != typeof XDomainRequest && !o && i) return new XDomainRequest();
        } catch (s) {}
        if (!n) try {
          return new e[["Active"].concat("Object").join("X")]("Microsoft.XMLHTTP");
        } catch (s) {}
      };
    }).call(e, function () {
      return this;
    }());
  }, function (t, e) {
    try {
      t.exports = "undefined" != typeof XMLHttpRequest && "withCredentials" in new XMLHttpRequest();
    } catch (n) {
      t.exports = !1;
    }
  }, function (t, e, n) {
    (function (e) {
      function r() {}

      function o(t) {
        if (c.call(this, t), this.requestTimeout = t.requestTimeout, this.extraHeaders = t.extraHeaders, e.location) {
          var n = "https:" === location.protocol,
              r = location.port;
          r || (r = n ? 443 : 80), this.xd = t.hostname !== e.location.hostname || r !== t.port, this.xs = t.secure !== n;
        }
      }

      function i(t) {
        this.method = t.method || "GET", this.uri = t.uri, this.xd = !!t.xd, this.xs = !!t.xs, this.async = !1 !== t.async, this.data = void 0 !== t.data ? t.data : null, this.agent = t.agent, this.isBinary = t.isBinary, this.supportsBinary = t.supportsBinary, this.enablesXDR = t.enablesXDR, this.requestTimeout = t.requestTimeout, this.pfx = t.pfx, this.key = t.key, this.passphrase = t.passphrase, this.cert = t.cert, this.ca = t.ca, this.ciphers = t.ciphers, this.rejectUnauthorized = t.rejectUnauthorized, this.extraHeaders = t.extraHeaders, this.create();
      }

      function s() {
        for (var t in i.requests) i.requests.hasOwnProperty(t) && i.requests[t].abort();
      }
      var a = n(18),
          c = n(21),
          p = n(8),
          u = n(32),
          h = n(3)("engine.io-client:polling-xhr");
      t.exports = o, t.exports.Request = i, u(o, c), o.prototype.supportsBinary = !0, o.prototype.request = function (t) {
        return t = t || {}, t.uri = this.uri(), t.xd = this.xd, t.xs = this.xs, t.agent = this.agent || !1, t.supportsBinary = this.supportsBinary, t.enablesXDR = this.enablesXDR, t.pfx = this.pfx, t.key = this.key, t.passphrase = this.passphrase, t.cert = this.cert, t.ca = this.ca, t.ciphers = this.ciphers, t.rejectUnauthorized = this.rejectUnauthorized, t.requestTimeout = this.requestTimeout, t.extraHeaders = this.extraHeaders, new i(t);
      }, o.prototype.doWrite = function (t, e) {
        var n = "string" != typeof t && void 0 !== t,
            r = this.request({
          method: "POST",
          data: t,
          isBinary: n
        }),
            o = this;
        r.on("success", e), r.on("error", function (t) {
          o.onError("xhr post error", t);
        }), this.sendXhr = r;
      }, o.prototype.doPoll = function () {
        h("xhr poll");
        var t = this.request(),
            e = this;
        t.on("data", function (t) {
          e.onData(t);
        }), t.on("error", function (t) {
          e.onError("xhr poll error", t);
        }), this.pollXhr = t;
      }, p(i.prototype), i.prototype.create = function () {
        var t = {
          agent: this.agent,
          xdomain: this.xd,
          xscheme: this.xs,
          enablesXDR: this.enablesXDR
        };
        t.pfx = this.pfx, t.key = this.key, t.passphrase = this.passphrase, t.cert = this.cert, t.ca = this.ca, t.ciphers = this.ciphers, t.rejectUnauthorized = this.rejectUnauthorized;
        var n = this.xhr = new a(t),
            r = this;
        try {
          h("xhr open %s: %s", this.method, this.uri), n.open(this.method, this.uri, this.async);
          try {
            if (this.extraHeaders) {
              n.setDisableHeaderCheck && n.setDisableHeaderCheck(!0);
              for (var o in this.extraHeaders) this.extraHeaders.hasOwnProperty(o) && n.setRequestHeader(o, this.extraHeaders[o]);
            }
          } catch (s) {}
          if ("POST" === this.method) try {
            this.isBinary ? n.setRequestHeader("Content-type", "application/octet-stream") : n.setRequestHeader("Content-type", "text/plain;charset=UTF-8");
          } catch (s) {}
          try {
            n.setRequestHeader("Accept", "*/*");
          } catch (s) {}
          "withCredentials" in n && (n.withCredentials = !0), this.requestTimeout && (n.timeout = this.requestTimeout), this.hasXDR() ? (n.onload = function () {
            r.onLoad();
          }, n.onerror = function () {
            r.onError(n.responseText);
          }) : n.onreadystatechange = function () {
            if (2 === n.readyState) {
              var t;
              try {
                t = n.getResponseHeader("Content-Type");
              } catch (e) {}
              "application/octet-stream" === t && (n.responseType = "arraybuffer");
            }
            4 === n.readyState && (200 === n.status || 1223 === n.status ? r.onLoad() : setTimeout(function () {
              r.onError(n.status);
            }, 0));
          }, h("xhr data %s", this.data), n.send(this.data);
        } catch (s) {
          return void setTimeout(function () {
            r.onError(s);
          }, 0);
        }
        e.document && (this.index = i.requestsCount++, i.requests[this.index] = this);
      }, i.prototype.onSuccess = function () {
        this.emit("success"), this.cleanup();
      }, i.prototype.onData = function (t) {
        this.emit("data", t), this.onSuccess();
      }, i.prototype.onError = function (t) {
        this.emit("error", t), this.cleanup(!0);
      }, i.prototype.cleanup = function (t) {
        if ("undefined" != typeof this.xhr && null !== this.xhr) {
          if (this.hasXDR() ? this.xhr.onload = this.xhr.onerror = r : this.xhr.onreadystatechange = r, t) try {
            this.xhr.abort();
          } catch (n) {}
          e.document && delete i.requests[this.index], this.xhr = null;
        }
      }, i.prototype.onLoad = function () {
        var t;
        try {
          var e;
          try {
            e = this.xhr.getResponseHeader("Content-Type");
          } catch (n) {}
          t = "application/octet-stream" === e ? this.xhr.response || this.xhr.responseText : this.xhr.responseText;
        } catch (n) {
          this.onError(n);
        }
        null != t && this.onData(t);
      }, i.prototype.hasXDR = function () {
        return "undefined" != typeof e.XDomainRequest && !this.xs && this.enablesXDR;
      }, i.prototype.abort = function () {
        this.cleanup();
      }, i.requestsCount = 0, i.requests = {}, e.document && (e.attachEvent ? e.attachEvent("onunload", s) : e.addEventListener && e.addEventListener("beforeunload", s, !1));
    }).call(e, function () {
      return this;
    }());
  }, function (t, e, n) {
    function r(t) {
      var e = t && t.forceBase64;
      u && !e || (this.supportsBinary = !1), o.call(this, t);
    }
    var o = n(22),
        i = n(31),
        s = n(23),
        a = n(32),
        c = n(33),
        p = n(3)("engine.io-client:polling");
    t.exports = r;
    var u = function () {
      var t = n(18),
          e = new t({
        xdomain: !1
      });
      return null != e.responseType;
    }();
    a(r, o), r.prototype.name = "polling", r.prototype.doOpen = function () {
      this.poll();
    }, r.prototype.pause = function (t) {
      function e() {
        p("paused"), n.readyState = "paused", t();
      }
      var n = this;
      if (this.readyState = "pausing", this.polling || !this.writable) {
        var r = 0;
        this.polling && (p("we are currently polling - waiting to pause"), r++, this.once("pollComplete", function () {
          p("pre-pause polling complete"), --r || e();
        })), this.writable || (p("we are currently writing - waiting to pause"), r++, this.once("drain", function () {
          p("pre-pause writing complete"), --r || e();
        }));
      } else e();
    }, r.prototype.poll = function () {
      p("polling"), this.polling = !0, this.doPoll(), this.emit("poll");
    }, r.prototype.onData = function (t) {
      var e = this;
      p("polling got data %s", t);
      var n = function (t, n, r) {
        return "opening" === e.readyState && e.onOpen(), "close" === t.type ? (e.onClose(), !1) : void e.onPacket(t);
      };
      s.decodePayload(t, this.socket.binaryType, n), "closed" !== this.readyState && (this.polling = !1, this.emit("pollComplete"), "open" === this.readyState ? this.poll() : p('ignoring poll - transport state "%s"', this.readyState));
    }, r.prototype.doClose = function () {
      function t() {
        p("writing close packet"), e.write([{
          type: "close"
        }]);
      }
      var e = this;
      "open" === this.readyState ? (p("transport open - closing"), t()) : (p("transport not open - deferring close"), this.once("open", t));
    }, r.prototype.write = function (t) {
      var e = this;
      this.writable = !1;
      var n = function () {
        e.writable = !0, e.emit("drain");
      };
      s.encodePayload(t, this.supportsBinary, function (t) {
        e.doWrite(t, n);
      });
    }, r.prototype.uri = function () {
      var t = this.query || {},
          e = this.secure ? "https" : "http",
          n = "";
      !1 !== this.timestampRequests && (t[this.timestampParam] = c()), this.supportsBinary || t.sid || (t.b64 = 1), t = i.encode(t), this.port && ("https" === e && 443 !== Number(this.port) || "http" === e && 80 !== Number(this.port)) && (n = ":" + this.port), t.length && (t = "?" + t);
      var r = this.hostname.indexOf(":") !== -1;
      return e + "://" + (r ? "[" + this.hostname + "]" : this.hostname) + n + this.path + t;
    };
  }, function (t, e, n) {
    function r(t) {
      this.path = t.path, this.hostname = t.hostname, this.port = t.port, this.secure = t.secure, this.query = t.query, this.timestampParam = t.timestampParam, this.timestampRequests = t.timestampRequests, this.readyState = "", this.agent = t.agent || !1, this.socket = t.socket, this.enablesXDR = t.enablesXDR, this.pfx = t.pfx, this.key = t.key, this.passphrase = t.passphrase, this.cert = t.cert, this.ca = t.ca, this.ciphers = t.ciphers, this.rejectUnauthorized = t.rejectUnauthorized, this.forceNode = t.forceNode, this.extraHeaders = t.extraHeaders, this.localAddress = t.localAddress;
    }
    var o = n(23),
        i = n(8);
    t.exports = r, i(r.prototype), r.prototype.onError = function (t, e) {
      var n = new Error(t);
      return n.type = "TransportError", n.description = e, this.emit("error", n), this;
    }, r.prototype.open = function () {
      return "closed" !== this.readyState && "" !== this.readyState || (this.readyState = "opening", this.doOpen()), this;
    }, r.prototype.close = function () {
      return "opening" !== this.readyState && "open" !== this.readyState || (this.doClose(), this.onClose()), this;
    }, r.prototype.send = function (t) {
      if ("open" !== this.readyState) throw new Error("Transport not open");
      this.write(t);
    }, r.prototype.onOpen = function () {
      this.readyState = "open", this.writable = !0, this.emit("open");
    }, r.prototype.onData = function (t) {
      var e = o.decodePacket(t, this.socket.binaryType);
      this.onPacket(e);
    }, r.prototype.onPacket = function (t) {
      this.emit("packet", t);
    }, r.prototype.onClose = function () {
      this.readyState = "closed", this.emit("close");
    };
  }, function (t, e, n) {
    (function (t) {
      function r(t, n) {
        var r = "b" + e.packets[t.type] + t.data.data;
        return n(r);
      }

      function o(t, n, r) {
        if (!n) return e.encodeBase64Packet(t, r);
        var o = t.data,
            i = new Uint8Array(o),
            s = new Uint8Array(1 + o.byteLength);
        s[0] = v[t.type];
        for (var a = 0; a < i.length; a++) s[a + 1] = i[a];
        return r(s.buffer);
      }

      function i(t, n, r) {
        if (!n) return e.encodeBase64Packet(t, r);
        var o = new FileReader();
        return o.onload = function () {
          t.data = o.result, e.encodePacket(t, n, !0, r);
        }, o.readAsArrayBuffer(t.data);
      }

      function s(t, n, r) {
        if (!n) return e.encodeBase64Packet(t, r);
        if (g) return i(t, n, r);
        var o = new Uint8Array(1);
        o[0] = v[t.type];
        var s = new k([o.buffer, t.data]);
        return r(s);
      }

      function a(t) {
        try {
          t = d.decode(t, {
            strict: !1
          });
        } catch (e) {
          return !1;
        }
        return t;
      }

      function c(t, e, n) {
        for (var r = new Array(t.length), o = l(t.length, n), i = function (t, n, o) {
          e(n, function (e, n) {
            r[t] = n, o(e, r);
          });
        }, s = 0; s < t.length; s++) i(s, t[s], o);
      }
      var p,
          u = n(24),
          h = n(9),
          f = n(25),
          l = n(26),
          d = n(27);
      t && t.ArrayBuffer && (p = n(29));
      var y = "undefined" != typeof navigator && /Android/i.test(navigator.userAgent),
          m = "undefined" != typeof navigator && /PhantomJS/i.test(navigator.userAgent),
          g = y || m;
      e.protocol = 3;
      var v = e.packets = {
        open: 0,
        close: 1,
        ping: 2,
        pong: 3,
        message: 4,
        upgrade: 5,
        noop: 6
      },
          b = u(v),
          w = {
        type: "error",
        data: "parser error"
      },
          k = n(30);
      e.encodePacket = function (e, n, i, a) {
        "function" == typeof n && (a = n, n = !1), "function" == typeof i && (a = i, i = null);
        var c = void 0 === e.data ? void 0 : e.data.buffer || e.data;
        if (t.ArrayBuffer && c instanceof ArrayBuffer) return o(e, n, a);
        if (k && c instanceof t.Blob) return s(e, n, a);
        if (c && c.base64) return r(e, a);
        var p = v[e.type];
        return void 0 !== e.data && (p += i ? d.encode(String(e.data), {
          strict: !1
        }) : String(e.data)), a("" + p);
      }, e.encodeBase64Packet = function (n, r) {
        var o = "b" + e.packets[n.type];
        if (k && n.data instanceof t.Blob) {
          var i = new FileReader();
          return i.onload = function () {
            var t = i.result.split(",")[1];
            r(o + t);
          }, i.readAsDataURL(n.data);
        }
        var s;
        try {
          s = String.fromCharCode.apply(null, new Uint8Array(n.data));
        } catch (a) {
          for (var c = new Uint8Array(n.data), p = new Array(c.length), u = 0; u < c.length; u++) p[u] = c[u];
          s = String.fromCharCode.apply(null, p);
        }
        return o += t.btoa(s), r(o);
      }, e.decodePacket = function (t, n, r) {
        if (void 0 === t) return w;
        if ("string" == typeof t) {
          if ("b" === t.charAt(0)) return e.decodeBase64Packet(t.substr(1), n);
          if (r && (t = a(t), t === !1)) return w;
          var o = t.charAt(0);
          return Number(o) == o && b[o] ? t.length > 1 ? {
            type: b[o],
            data: t.substring(1)
          } : {
            type: b[o]
          } : w;
        }
        var i = new Uint8Array(t),
            o = i[0],
            s = f(t, 1);
        return k && "blob" === n && (s = new k([s])), {
          type: b[o],
          data: s
        };
      }, e.decodeBase64Packet = function (t, e) {
        var n = b[t.charAt(0)];
        if (!p) return {
          type: n,
          data: {
            base64: !0,
            data: t.substr(1)
          }
        };
        var r = p.decode(t.substr(1));
        return "blob" === e && k && (r = new k([r])), {
          type: n,
          data: r
        };
      }, e.encodePayload = function (t, n, r) {
        function o(t) {
          return t.length + ":" + t;
        }

        function i(t, r) {
          e.encodePacket(t, !!s && n, !1, function (t) {
            r(null, o(t));
          });
        }
        "function" == typeof n && (r = n, n = null);
        var s = h(t);
        return n && s ? k && !g ? e.encodePayloadAsBlob(t, r) : e.encodePayloadAsArrayBuffer(t, r) : t.length ? void c(t, i, function (t, e) {
          return r(e.join(""));
        }) : r("0:");
      }, e.decodePayload = function (t, n, r) {
        if ("string" != typeof t) return e.decodePayloadAsBinary(t, n, r);
        "function" == typeof n && (r = n, n = null);
        var o;
        if ("" === t) return r(w, 0, 1);
        for (var i, s, a = "", c = 0, p = t.length; c < p; c++) {
          var u = t.charAt(c);
          if (":" === u) {
            if ("" === a || a != (i = Number(a))) return r(w, 0, 1);
            if (s = t.substr(c + 1, i), a != s.length) return r(w, 0, 1);
            if (s.length) {
              if (o = e.decodePacket(s, n, !1), w.type === o.type && w.data === o.data) return r(w, 0, 1);
              var h = r(o, c + i, p);
              if (!1 === h) return;
            }
            c += i, a = "";
          } else a += u;
        }
        return "" !== a ? r(w, 0, 1) : void 0;
      }, e.encodePayloadAsArrayBuffer = function (t, n) {
        function r(t, n) {
          e.encodePacket(t, !0, !0, function (t) {
            return n(null, t);
          });
        }
        return t.length ? void c(t, r, function (t, e) {
          var r = e.reduce(function (t, e) {
            var n;
            return n = "string" == typeof e ? e.length : e.byteLength, t + n.toString().length + n + 2;
          }, 0),
              o = new Uint8Array(r),
              i = 0;
          return e.forEach(function (t) {
            var e = "string" == typeof t,
                n = t;
            if (e) {
              for (var r = new Uint8Array(t.length), s = 0; s < t.length; s++) r[s] = t.charCodeAt(s);
              n = r.buffer;
            }
            e ? o[i++] = 0 : o[i++] = 1;
            for (var a = n.byteLength.toString(), s = 0; s < a.length; s++) o[i++] = parseInt(a[s]);
            o[i++] = 255;
            for (var r = new Uint8Array(n), s = 0; s < r.length; s++) o[i++] = r[s];
          }), n(o.buffer);
        }) : n(new ArrayBuffer(0));
      }, e.encodePayloadAsBlob = function (t, n) {
        function r(t, n) {
          e.encodePacket(t, !0, !0, function (t) {
            var e = new Uint8Array(1);
            if (e[0] = 1, "string" == typeof t) {
              for (var r = new Uint8Array(t.length), o = 0; o < t.length; o++) r[o] = t.charCodeAt(o);
              t = r.buffer, e[0] = 0;
            }
            for (var i = t instanceof ArrayBuffer ? t.byteLength : t.size, s = i.toString(), a = new Uint8Array(s.length + 1), o = 0; o < s.length; o++) a[o] = parseInt(s[o]);
            if (a[s.length] = 255, k) {
              var c = new k([e.buffer, a.buffer, t]);
              n(null, c);
            }
          });
        }
        c(t, r, function (t, e) {
          return n(new k(e));
        });
      }, e.decodePayloadAsBinary = function (t, n, r) {
        "function" == typeof n && (r = n, n = null);
        for (var o = t, i = []; o.byteLength > 0;) {
          for (var s = new Uint8Array(o), a = 0 === s[0], c = "", p = 1; 255 !== s[p]; p++) {
            if (c.length > 310) return r(w, 0, 1);
            c += s[p];
          }
          o = f(o, 2 + c.length), c = parseInt(c);
          var u = f(o, 0, c);
          if (a) try {
            u = String.fromCharCode.apply(null, new Uint8Array(u));
          } catch (h) {
            var l = new Uint8Array(u);
            u = "";
            for (var p = 0; p < l.length; p++) u += String.fromCharCode(l[p]);
          }
          i.push(u), o = f(o, c);
        }
        var d = i.length;
        i.forEach(function (t, o) {
          r(e.decodePacket(t, n, !0), o, d);
        });
      };
    }).call(e, function () {
      return this;
    }());
  }, function (t, e) {
    t.exports = Object.keys || function (t) {
      var e = [],
          n = Object.prototype.hasOwnProperty;
      for (var r in t) n.call(t, r) && e.push(r);
      return e;
    };
  }, function (t, e) {
    t.exports = function (t, e, n) {
      var r = t.byteLength;
      if (e = e || 0, n = n || r, t.slice) return t.slice(e, n);
      if (e < 0 && (e += r), n < 0 && (n += r), n > r && (n = r), e >= r || e >= n || 0 === r) return new ArrayBuffer(0);
      for (var o = new Uint8Array(t), i = new Uint8Array(n - e), s = e, a = 0; s < n; s++, a++) i[a] = o[s];
      return i.buffer;
    };
  }, function (t, e) {
    function n(t, e, n) {
      function o(t, r) {
        if (o.count <= 0) throw new Error("after called too many times");
        --o.count, t ? (i = !0, e(t), e = n) : 0 !== o.count || i || e(null, r);
      }
      var i = !1;
      return n = n || r, o.count = t, 0 === t ? e() : o;
    }

    function r() {}
    t.exports = n;
  }, function (t, e, n) {
    var r;
    (function (t, o) {
      !function (i) {
        function s(t) {
          for (var e, n, r = [], o = 0, i = t.length; o < i;) e = t.charCodeAt(o++), e >= 55296 && e <= 56319 && o < i ? (n = t.charCodeAt(o++), 56320 == (64512 & n) ? r.push(((1023 & e) << 10) + (1023 & n) + 65536) : (r.push(e), o--)) : r.push(e);
          return r;
        }

        function a(t) {
          for (var e, n = t.length, r = -1, o = ""; ++r < n;) e = t[r], e > 65535 && (e -= 65536, o += w(e >>> 10 & 1023 | 55296), e = 56320 | 1023 & e), o += w(e);
          return o;
        }

        function c(t, e) {
          if (t >= 55296 && t <= 57343) {
            if (e) throw Error("Lone surrogate U+" + t.toString(16).toUpperCase() + " is not a scalar value");
            return !1;
          }
          return !0;
        }

        function p(t, e) {
          return w(t >> e & 63 | 128);
        }

        function u(t, e) {
          if (0 == (4294967168 & t)) return w(t);
          var n = "";
          return 0 == (4294965248 & t) ? n = w(t >> 6 & 31 | 192) : 0 == (4294901760 & t) ? (c(t, e) || (t = 65533), n = w(t >> 12 & 15 | 224), n += p(t, 6)) : 0 == (4292870144 & t) && (n = w(t >> 18 & 7 | 240), n += p(t, 12), n += p(t, 6)), n += w(63 & t | 128);
        }

        function h(t, e) {
          e = e || {};
          for (var n, r = !1 !== e.strict, o = s(t), i = o.length, a = -1, c = ""; ++a < i;) n = o[a], c += u(n, r);
          return c;
        }

        function f() {
          if (b >= v) throw Error("Invalid byte index");
          var t = 255 & g[b];
          if (b++, 128 == (192 & t)) return 63 & t;
          throw Error("Invalid continuation byte");
        }

        function l(t) {
          var e, n, r, o, i;
          if (b > v) throw Error("Invalid byte index");
          if (b == v) return !1;
          if (e = 255 & g[b], b++, 0 == (128 & e)) return e;
          if (192 == (224 & e)) {
            if (n = f(), i = (31 & e) << 6 | n, i >= 128) return i;
            throw Error("Invalid continuation byte");
          }
          if (224 == (240 & e)) {
            if (n = f(), r = f(), i = (15 & e) << 12 | n << 6 | r, i >= 2048) return c(i, t) ? i : 65533;
            throw Error("Invalid continuation byte");
          }
          if (240 == (248 & e) && (n = f(), r = f(), o = f(), i = (7 & e) << 18 | n << 12 | r << 6 | o, i >= 65536 && i <= 1114111)) return i;
          throw Error("Invalid UTF-8 detected");
        }

        function d(t, e) {
          e = e || {};
          var n = !1 !== e.strict;
          g = s(t), v = g.length, b = 0;
          for (var r, o = []; (r = l(n)) !== !1;) o.push(r);
          return a(o);
        }
        var y = "object" == typeof e && e,
            m = ("object" == typeof t && t && t.exports == y && t, "object" == typeof o && o);
        m.global !== m && m.window !== m || (i = m);
        var g,
            v,
            b,
            w = String.fromCharCode,
            k = {
          version: "2.1.2",
          encode: h,
          decode: d
        };
        r = function () {
          return k;
        }.call(e, n, e, t), !(void 0 !== r && (t.exports = r));
      }(this);
    }).call(e, n(28)(t), function () {
      return this;
    }());
  }, function (t, e) {
    t.exports = function (t) {
      return t.webpackPolyfill || (t.deprecate = function () {}, t.paths = [], t.children = [], t.webpackPolyfill = 1), t;
    };
  }, function (t, e) {
    !function () {
      "use strict";

      for (var t = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", n = new Uint8Array(256), r = 0; r < t.length; r++) n[t.charCodeAt(r)] = r;
      e.encode = function (e) {
        var n,
            r = new Uint8Array(e),
            o = r.length,
            i = "";
        for (n = 0; n < o; n += 3) i += t[r[n] >> 2], i += t[(3 & r[n]) << 4 | r[n + 1] >> 4], i += t[(15 & r[n + 1]) << 2 | r[n + 2] >> 6], i += t[63 & r[n + 2]];
        return o % 3 === 2 ? i = i.substring(0, i.length - 1) + "=" : o % 3 === 1 && (i = i.substring(0, i.length - 2) + "=="), i;
      }, e.decode = function (t) {
        var e,
            r,
            o,
            i,
            s,
            a = .75 * t.length,
            c = t.length,
            p = 0;
        "=" === t[t.length - 1] && (a--, "=" === t[t.length - 2] && a--);
        var u = new ArrayBuffer(a),
            h = new Uint8Array(u);
        for (e = 0; e < c; e += 4) r = n[t.charCodeAt(e)], o = n[t.charCodeAt(e + 1)], i = n[t.charCodeAt(e + 2)], s = n[t.charCodeAt(e + 3)], h[p++] = r << 2 | o >> 4, h[p++] = (15 & o) << 4 | i >> 2, h[p++] = (3 & i) << 6 | 63 & s;
        return u;
      };
    }();
  }, function (t, e) {
    (function (e) {
      function n(t) {
        for (var e = 0; e < t.length; e++) {
          var n = t[e];
          if (n.buffer instanceof ArrayBuffer) {
            var r = n.buffer;
            if (n.byteLength !== r.byteLength) {
              var o = new Uint8Array(n.byteLength);
              o.set(new Uint8Array(r, n.byteOffset, n.byteLength)), r = o.buffer;
            }
            t[e] = r;
          }
        }
      }

      function r(t, e) {
        e = e || {};
        var r = new i();
        n(t);
        for (var o = 0; o < t.length; o++) r.append(t[o]);
        return e.type ? r.getBlob(e.type) : r.getBlob();
      }

      function o(t, e) {
        return n(t), new Blob(t, e || {});
      }
      var i = e.BlobBuilder || e.WebKitBlobBuilder || e.MSBlobBuilder || e.MozBlobBuilder,
          s = function () {
        try {
          var t = new Blob(["hi"]);
          return 2 === t.size;
        } catch (e) {
          return !1;
        }
      }(),
          a = s && function () {
        try {
          var t = new Blob([new Uint8Array([1, 2])]);
          return 2 === t.size;
        } catch (e) {
          return !1;
        }
      }(),
          c = i && i.prototype.append && i.prototype.getBlob;
      t.exports = function () {
        return s ? a ? e.Blob : o : c ? r : void 0;
      }();
    }).call(e, function () {
      return this;
    }());
  }, function (t, e) {
    e.encode = function (t) {
      var e = "";
      for (var n in t) t.hasOwnProperty(n) && (e.length && (e += "&"), e += encodeURIComponent(n) + "=" + encodeURIComponent(t[n]));
      return e;
    }, e.decode = function (t) {
      for (var e = {}, n = t.split("&"), r = 0, o = n.length; r < o; r++) {
        var i = n[r].split("=");
        e[decodeURIComponent(i[0])] = decodeURIComponent(i[1]);
      }
      return e;
    };
  }, function (t, e) {
    t.exports = function (t, e) {
      var n = function () {};
      n.prototype = e.prototype, t.prototype = new n(), t.prototype.constructor = t;
    };
  }, function (t, e) {
    "use strict";

    function n(t) {
      var e = "";
      do e = s[t % a] + e, t = Math.floor(t / a); while (t > 0);
      return e;
    }

    function r(t) {
      var e = 0;
      for (u = 0; u < t.length; u++) e = e * a + c[t.charAt(u)];
      return e;
    }

    function o() {
      var t = n(+new Date());
      return t !== i ? (p = 0, i = t) : t + "." + n(p++);
    }
    for (var i, s = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_".split(""), a = 64, c = {}, p = 0, u = 0; u < a; u++) c[s[u]] = u;
    o.encode = n, o.decode = r, t.exports = o;
  }, function (t, e, n) {
    (function (e) {
      function r() {}

      function o(t) {
        i.call(this, t), this.query = this.query || {}, a || (e.___eio || (e.___eio = []), a = e.___eio), this.index = a.length;
        var n = this;
        a.push(function (t) {
          n.onData(t);
        }), this.query.j = this.index, e.document && e.addEventListener && e.addEventListener("beforeunload", function () {
          n.script && (n.script.onerror = r);
        }, !1);
      }
      var i = n(21),
          s = n(32);
      t.exports = o;
      var a,
          c = /\n/g,
          p = /\\n/g;
      s(o, i), o.prototype.supportsBinary = !1, o.prototype.doClose = function () {
        this.script && (this.script.parentNode.removeChild(this.script), this.script = null), this.form && (this.form.parentNode.removeChild(this.form), this.form = null, this.iframe = null), i.prototype.doClose.call(this);
      }, o.prototype.doPoll = function () {
        var t = this,
            e = document.createElement("script");
        this.script && (this.script.parentNode.removeChild(this.script), this.script = null), e.async = !0, e.src = this.uri(), e.onerror = function (e) {
          t.onError("jsonp poll error", e);
        };
        var n = document.getElementsByTagName("script")[0];
        n ? n.parentNode.insertBefore(e, n) : (document.head || document.body).appendChild(e), this.script = e;
        var r = "undefined" != typeof navigator && /gecko/i.test(navigator.userAgent);
        r && setTimeout(function () {
          var t = document.createElement("iframe");
          document.body.appendChild(t), document.body.removeChild(t);
        }, 100);
      }, o.prototype.doWrite = function (t, e) {
        function n() {
          r(), e();
        }

        function r() {
          if (o.iframe) try {
            o.form.removeChild(o.iframe);
          } catch (t) {
            o.onError("jsonp polling iframe removal error", t);
          }
          try {
            var e = '<iframe src="javascript:0" name="' + o.iframeId + '">';
            i = document.createElement(e);
          } catch (t) {
            i = document.createElement("iframe"), i.name = o.iframeId, i.src = "javascript:0";
          }
          i.id = o.iframeId, o.form.appendChild(i), o.iframe = i;
        }
        var o = this;
        if (!this.form) {
          var i,
              s = document.createElement("form"),
              a = document.createElement("textarea"),
              u = this.iframeId = "eio_iframe_" + this.index;
          s.className = "socketio", s.style.position = "absolute", s.style.top = "-1000px", s.style.left = "-1000px", s.target = u, s.method = "POST", s.setAttribute("accept-charset", "utf-8"), a.name = "d", s.appendChild(a), document.body.appendChild(s), this.form = s, this.area = a;
        }
        this.form.action = this.uri(), r(), t = t.replace(p, "\\\n"), this.area.value = t.replace(c, "\\n");
        try {
          this.form.submit();
        } catch (h) {}
        this.iframe.attachEvent ? this.iframe.onreadystatechange = function () {
          "complete" === o.iframe.readyState && n();
        } : this.iframe.onload = n;
      };
    }).call(e, function () {
      return this;
    }());
  }, function (t, e, n) {
    (function (e) {
      function r(t) {
        var e = t && t.forceBase64;
        e && (this.supportsBinary = !1), this.perMessageDeflate = t.perMessageDeflate, this.usingBrowserWebSocket = h && !t.forceNode, this.protocols = t.protocols, this.usingBrowserWebSocket || (l = o), i.call(this, t);
      }
      var o,
          i = n(22),
          s = n(23),
          a = n(31),
          c = n(32),
          p = n(33),
          u = n(3)("engine.io-client:websocket"),
          h = e.WebSocket || e.MozWebSocket;
      if ("undefined" == typeof window) try {
        o = n(36);
      } catch (f) {}
      var l = h;
      l || "undefined" != typeof window || (l = o), t.exports = r, c(r, i), r.prototype.name = "websocket", r.prototype.supportsBinary = !0, r.prototype.doOpen = function () {
        if (this.check()) {
          var t = this.uri(),
              e = this.protocols,
              n = {
            agent: this.agent,
            perMessageDeflate: this.perMessageDeflate
          };
          n.pfx = this.pfx, n.key = this.key, n.passphrase = this.passphrase, n.cert = this.cert, n.ca = this.ca, n.ciphers = this.ciphers, n.rejectUnauthorized = this.rejectUnauthorized, this.extraHeaders && (n.headers = this.extraHeaders), this.localAddress && (n.localAddress = this.localAddress);
          try {
            this.ws = this.usingBrowserWebSocket ? e ? new l(t, e) : new l(t) : new l(t, e, n);
          } catch (r) {
            return this.emit("error", r);
          }
          void 0 === this.ws.binaryType && (this.supportsBinary = !1), this.ws.supports && this.ws.supports.binary ? (this.supportsBinary = !0, this.ws.binaryType = "nodebuffer") : this.ws.binaryType = "arraybuffer", this.addEventListeners();
        }
      }, r.prototype.addEventListeners = function () {
        var t = this;
        this.ws.onopen = function () {
          t.onOpen();
        }, this.ws.onclose = function () {
          t.onClose();
        }, this.ws.onmessage = function (e) {
          t.onData(e.data);
        }, this.ws.onerror = function (e) {
          t.onError("websocket error", e);
        };
      }, r.prototype.write = function (t) {
        function n() {
          r.emit("flush"), setTimeout(function () {
            r.writable = !0, r.emit("drain");
          }, 0);
        }
        var r = this;
        this.writable = !1;
        for (var o = t.length, i = 0, a = o; i < a; i++) !function (t) {
          s.encodePacket(t, r.supportsBinary, function (i) {
            if (!r.usingBrowserWebSocket) {
              var s = {};
              if (t.options && (s.compress = t.options.compress), r.perMessageDeflate) {
                var a = "string" == typeof i ? e.Buffer.byteLength(i) : i.length;
                a < r.perMessageDeflate.threshold && (s.compress = !1);
              }
            }
            try {
              r.usingBrowserWebSocket ? r.ws.send(i) : r.ws.send(i, s);
            } catch (c) {
              u("websocket closed before onclose event");
            }--o || n();
          });
        }(t[i]);
      }, r.prototype.onClose = function () {
        i.prototype.onClose.call(this);
      }, r.prototype.doClose = function () {
        "undefined" != typeof this.ws && this.ws.close();
      }, r.prototype.uri = function () {
        var t = this.query || {},
            e = this.secure ? "wss" : "ws",
            n = "";
        this.port && ("wss" === e && 443 !== Number(this.port) || "ws" === e && 80 !== Number(this.port)) && (n = ":" + this.port), this.timestampRequests && (t[this.timestampParam] = p()), this.supportsBinary || (t.b64 = 1), t = a.encode(t), t.length && (t = "?" + t);
        var r = this.hostname.indexOf(":") !== -1;
        return e + "://" + (r ? "[" + this.hostname + "]" : this.hostname) + n + this.path + t;
      }, r.prototype.check = function () {
        return !(!l || "__initialize" in l && this.name === r.prototype.name);
      };
    }).call(e, function () {
      return this;
    }());
  }, function (t, e) {}, function (t, e) {
    var n = [].indexOf;
    t.exports = function (t, e) {
      if (n) return t.indexOf(e);
      for (var r = 0; r < t.length; ++r) if (t[r] === e) return r;
      return -1;
    };
  }, function (t, e) {
    (function (e) {
      var n = /^[\],:{}\s]*$/,
          r = /\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,
          o = /"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,
          i = /(?:^|:|,)(?:\s*\[)+/g,
          s = /^\s+/,
          a = /\s+$/;
      t.exports = function (t) {
        return "string" == typeof t && t ? (t = t.replace(s, "").replace(a, ""), e.JSON && JSON.parse ? JSON.parse(t) : n.test(t.replace(r, "@").replace(o, "]").replace(i, "")) ? new Function("return " + t)() : void 0) : null;
      };
    }).call(e, function () {
      return this;
    }());
  }, function (t, e, n) {
    "use strict";

    function r(t, e, n) {
      this.io = t, this.nsp = e, this.json = this, this.ids = 0, this.acks = {}, this.receiveBuffer = [], this.sendBuffer = [], this.connected = !1, this.disconnected = !0, n && n.query && (this.query = n.query), this.io.autoConnect && this.open();
    }
    var o = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (t) {
      return typeof t;
    } : function (t) {
      return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t;
    },
        i = n(7),
        s = n(8),
        a = n(40),
        c = n(41),
        p = n(42),
        u = n(3)("socket.io-client:socket"),
        h = n(31);
    t.exports = e = r;
    var f = {
      connect: 1,
      connect_error: 1,
      connect_timeout: 1,
      connecting: 1,
      disconnect: 1,
      error: 1,
      reconnect: 1,
      reconnect_attempt: 1,
      reconnect_failed: 1,
      reconnect_error: 1,
      reconnecting: 1,
      ping: 1,
      pong: 1
    },
        l = s.prototype.emit;
    s(r.prototype), r.prototype.subEvents = function () {
      if (!this.subs) {
        var t = this.io;
        this.subs = [c(t, "open", p(this, "onopen")), c(t, "packet", p(this, "onpacket")), c(t, "close", p(this, "onclose"))];
      }
    }, r.prototype.open = r.prototype.connect = function () {
      return this.connected ? this : (this.subEvents(), this.io.open(), "open" === this.io.readyState && this.onopen(), this.emit("connecting"), this);
    }, r.prototype.send = function () {
      var t = a(arguments);
      return t.unshift("message"), this.emit.apply(this, t), this;
    }, r.prototype.emit = function (t) {
      if (f.hasOwnProperty(t)) return l.apply(this, arguments), this;
      var e = a(arguments),
          n = {
        type: i.EVENT,
        data: e
      };
      return n.options = {}, n.options.compress = !this.flags || !1 !== this.flags.compress, "function" == typeof e[e.length - 1] && (u("emitting packet with ack id %d", this.ids), this.acks[this.ids] = e.pop(), n.id = this.ids++), this.connected ? this.packet(n) : this.sendBuffer.push(n), delete this.flags, this;
    }, r.prototype.packet = function (t) {
      t.nsp = this.nsp, this.io.packet(t);
    }, r.prototype.onopen = function () {
      if (u("transport is open - connecting"), "/" !== this.nsp) if (this.query) {
        var t = "object" === o(this.query) ? h.encode(this.query) : this.query;
        u("sending connect packet with query %s", t), this.packet({
          type: i.CONNECT,
          query: t
        });
      } else this.packet({
        type: i.CONNECT
      });
    }, r.prototype.onclose = function (t) {
      u("close (%s)", t), this.connected = !1, this.disconnected = !0, delete this.id, this.emit("disconnect", t);
    }, r.prototype.onpacket = function (t) {
      if (t.nsp === this.nsp) switch (t.type) {
        case i.CONNECT:
          this.onconnect();
          break;
        case i.EVENT:
          this.onevent(t);
          break;
        case i.BINARY_EVENT:
          this.onevent(t);
          break;
        case i.ACK:
          this.onack(t);
          break;
        case i.BINARY_ACK:
          this.onack(t);
          break;
        case i.DISCONNECT:
          this.ondisconnect();
          break;
        case i.ERROR:
          this.emit("error", t.data);
      }
    }, r.prototype.onevent = function (t) {
      var e = t.data || [];
      u("emitting event %j", e), null != t.id && (u("attaching ack callback to event"), e.push(this.ack(t.id))), this.connected ? l.apply(this, e) : this.receiveBuffer.push(e);
    }, r.prototype.ack = function (t) {
      var e = this,
          n = !1;
      return function () {
        if (!n) {
          n = !0;
          var r = a(arguments);
          u("sending ack %j", r), e.packet({
            type: i.ACK,
            id: t,
            data: r
          });
        }
      };
    }, r.prototype.onack = function (t) {
      var e = this.acks[t.id];
      "function" == typeof e ? (u("calling ack %s with %j", t.id, t.data), e.apply(this, t.data), delete this.acks[t.id]) : u("bad ack %s", t.id);
    }, r.prototype.onconnect = function () {
      this.connected = !0, this.disconnected = !1, this.emit("connect"), this.emitBuffered();
    }, r.prototype.emitBuffered = function () {
      var t;
      for (t = 0; t < this.receiveBuffer.length; t++) l.apply(this, this.receiveBuffer[t]);
      for (this.receiveBuffer = [], t = 0; t < this.sendBuffer.length; t++) this.packet(this.sendBuffer[t]);
      this.sendBuffer = [];
    }, r.prototype.ondisconnect = function () {
      u("server disconnect (%s)", this.nsp), this.destroy(), this.onclose("io server disconnect");
    }, r.prototype.destroy = function () {
      if (this.subs) {
        for (var t = 0; t < this.subs.length; t++) this.subs[t].destroy();
        this.subs = null;
      }
      this.io.destroy(this);
    }, r.prototype.close = r.prototype.disconnect = function () {
      return this.connected && (u("performing disconnect (%s)", this.nsp), this.packet({
        type: i.DISCONNECT
      })), this.destroy(), this.connected && this.onclose("io client disconnect"), this;
    }, r.prototype.compress = function (t) {
      return this.flags = this.flags || {}, this.flags.compress = t, this;
    };
  }, function (t, e) {
    function n(t, e) {
      var n = [];
      e = e || 0;
      for (var r = e || 0; r < t.length; r++) n[r - e] = t[r];
      return n;
    }
    t.exports = n;
  }, function (t, e) {
    "use strict";

    function n(t, e, n) {
      return t.on(e, n), {
        destroy: function () {
          t.removeListener(e, n);
        }
      };
    }
    t.exports = n;
  }, function (t, e) {
    var n = [].slice;
    t.exports = function (t, e) {
      if ("string" == typeof e && (e = t[e]), "function" != typeof e) throw new Error("bind() requires a function");
      var r = n.call(arguments, 2);
      return function () {
        return e.apply(t, r.concat(n.call(arguments)));
      };
    };
  }, function (t, e) {
    function n(t) {
      t = t || {}, this.ms = t.min || 100, this.max = t.max || 1e4, this.factor = t.factor || 2, this.jitter = t.jitter > 0 && t.jitter <= 1 ? t.jitter : 0, this.attempts = 0;
    }
    t.exports = n, n.prototype.duration = function () {
      var t = this.ms * Math.pow(this.factor, this.attempts++);
      if (this.jitter) {
        var e = Math.random(),
            n = Math.floor(e * this.jitter * t);
        t = 0 == (1 & Math.floor(10 * e)) ? t - n : t + n;
      }
      return 0 | Math.min(t, this.max);
    }, n.prototype.reset = function () {
      this.attempts = 0;
    }, n.prototype.setMin = function (t) {
      this.ms = t;
    }, n.prototype.setMax = function (t) {
      this.max = t;
    }, n.prototype.setJitter = function (t) {
      this.jitter = t;
    };
  }]);
});
//# sourceMappingURL=socket.io.js.map

/***/ }),

/***/ "E51W":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "IckW":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "JbHr":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "NHnr":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
var internal_namespaceObject = {};
__webpack_require__.d(internal_namespaceObject, "login", function() { return login; });
__webpack_require__.d(internal_namespaceObject, "logout", function() { return logout; });
__webpack_require__.d(internal_namespaceObject, "getProfile", function() { return getProfile; });
__webpack_require__.d(internal_namespaceObject, "isLogin", function() { return isLogin; });
__webpack_require__.d(internal_namespaceObject, "isRootAdmin", function() { return isRootAdmin; });
var user_namespaceObject = {};
__webpack_require__.d(user_namespaceObject, "listUser", function() { return listUser; });
__webpack_require__.d(user_namespaceObject, "getUser", function() { return getUser; });
__webpack_require__.d(user_namespaceObject, "addUser", function() { return addUser; });
__webpack_require__.d(user_namespaceObject, "updateUser", function() { return updateUser; });
__webpack_require__.d(user_namespaceObject, "deleteUser", function() { return deleteUser; });
var role_namespaceObject = {};
__webpack_require__.d(role_namespaceObject, "listRole", function() { return listRole; });
__webpack_require__.d(role_namespaceObject, "getRole", function() { return getRole; });
__webpack_require__.d(role_namespaceObject, "addRole", function() { return addRole; });
__webpack_require__.d(role_namespaceObject, "updateRole", function() { return updateRole; });
__webpack_require__.d(role_namespaceObject, "deleteRole", function() { return deleteRole; });
var project_namespaceObject = {};
__webpack_require__.d(project_namespaceObject, "listProject", function() { return listProject; });
__webpack_require__.d(project_namespaceObject, "getProject", function() { return getProject; });
__webpack_require__.d(project_namespaceObject, "addProject", function() { return addProject; });
__webpack_require__.d(project_namespaceObject, "updateProject", function() { return updateProject; });
__webpack_require__.d(project_namespaceObject, "deleteProject", function() { return deleteProject; });
var chat_namespaceObject = {};
__webpack_require__.d(chat_namespaceObject, "listChat", function() { return listChat; });
__webpack_require__.d(chat_namespaceObject, "getChat", function() { return getChat; });
__webpack_require__.d(chat_namespaceObject, "addChat", function() { return addChat; });
__webpack_require__.d(chat_namespaceObject, "updateChat", function() { return updateChat; });
__webpack_require__.d(chat_namespaceObject, "deleteChat", function() { return deleteChat; });
var task_namespaceObject = {};
__webpack_require__.d(task_namespaceObject, "listTask", function() { return listTask; });
__webpack_require__.d(task_namespaceObject, "getTask", function() { return getTask; });
__webpack_require__.d(task_namespaceObject, "addTask", function() { return addTask; });
__webpack_require__.d(task_namespaceObject, "updateTask", function() { return updateTask; });
__webpack_require__.d(task_namespaceObject, "deleteTask", function() { return deleteTask; });
__webpack_require__.d(task_namespaceObject, "uploadFile", function() { return uploadFile; });

// EXTERNAL MODULE: ./node_modules/vue/dist/vue.esm.js
var vue_esm = __webpack_require__("7+uW");

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/App.vue
//
//
//
//
//
//

/* harmony default export */ var App = ({
  name: 'app'
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-6b952c66","hasScoped":false,"transformToRequire":{"video":"src","source":"src","img":"src","image":"xlink:href"},"buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/App.vue
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{attrs:{"id":"app"}},[_c('router-view')],1)}
var staticRenderFns = []
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ var selectortype_template_index_0_src_App = (esExports);
// CONCATENATED MODULE: ./src/App.vue
function injectStyle (ssrContext) {
  __webpack_require__("5yBN")
}
var normalizeComponent = __webpack_require__("VU/8")
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  App,
  selectortype_template_index_0_src_App,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ var src_App = (Component.exports);

// EXTERNAL MODULE: ./node_modules/vue-router/dist/vue-router.esm.js
var vue_router_esm = __webpack_require__("/ocq");

// EXTERNAL MODULE: ./node_modules/axios/index.js
var axios = __webpack_require__("mtWM");
var axios_default = /*#__PURE__*/__webpack_require__.n(axios);

// EXTERNAL MODULE: ./node_modules/qs/lib/index.js
var lib = __webpack_require__("mw3O");
var lib_default = /*#__PURE__*/__webpack_require__.n(lib);

// CONCATENATED MODULE: ./src/api/internal.js


let apiName = '/api';



/**
 * 登录功能
 */
const login = body => {
  return axios_default.a.post(apiName + "/internal/login", body).then(res => res);
};

/**
 * 登出功能
 */
const logout = () => {
  return axios_default.a.post(apiName + "/internal//logout").then(res => res);
};

/**
 * 获取个人信息
 */
const getProfile = () => {
  return axios_default.a.get(apiName + "/internal/Profile").then(res => res);
};

/**
 * 检测是否登录
 */
const isLogin = () => {
  return axios_default.a.get(apiName + "/internal/isLogin").then(res => res);
};
/**
 * 检查是否为全局管理员
 */
const isRootAdmin = () => {
  return axios_default.a.get(apiName + "/internal/isRootAdmin").then(res => res);
};
// CONCATENATED MODULE: ./src/api/user.js


let user_apiName = '/api';

/**
 * 获取所有用户列表
 * @param {*} query 
 */
const listUser = query => {
  return axios_default.a.get(user_apiName + "/user?" + lib_default.a.stringify(query)).then(res => res);
};
/**
 * 获取一个用户
 * @param {*} _id 
 */
const getUser = _id => {
  return axios_default.a.get(user_apiName + "/user/" + _id).then(res => res);
};
/**
 * 添加一个用户
 * @param {*} body 
 */
const addUser = body => {
  return axios_default.a.post(user_apiName + "/user", body).then(res => res);
};

/**
 * 更新一个用户
 * @param {*} _id 
 * @param {*} body 
 */
const updateUser = (_id, body) => {
  return axios_default.a.put(user_apiName + "/user/" + _id, body).then(res => res);
};

/**
 * 删除一个用户
 * @param {*} _id 
 */
const deleteUser = _id => {
  return axios_default.a.delete(user_apiName + "/user/" + _id).then(res => res);
};
// CONCATENATED MODULE: ./src/api/role.js


let role_apiName = '/api';

/**
 * 获取所有role列表
 * @param {*} query 
 */
const listRole = query => {
  return axios_default.a.get(role_apiName + "/role?" + lib_default.a.stringify(query)).then(res => res);
};
/**
 * 获取一个role
 * @param {*} _id 
 */
const getRole = _id => {
  return axios_default.a.get(role_apiName + "/role/" + _id).then(res => res);
};
/**
 * 添加一个role
 * @param {*} body 
 */
const addRole = body => {
  return axios_default.a.post(role_apiName + "/role", body).then(res => res);
};

/**
 * 更新一个role
 * @param {*} _id 
 * @param {*} body 
 */
const updateRole = (_id, body) => {
  return axios_default.a.put(role_apiName + "/role/" + _id, body).then(res => res);
};

/**
 * 删除一个role
 * @param {*} _id 
 */
const deleteRole = _id => {
  return axios_default.a.delete(role_apiName + "/role/" + _id).then(res => res);
};
// CONCATENATED MODULE: ./src/api/project.js


let project_apiName = '/api';

/**
 * 获取所有peoject列表
 * @param {*} query 
 */
const listProject = query => {
  return axios_default.a.get(project_apiName + "/project?" + lib_default.a.stringify(query)).then(res => res);
};
/**
 * 获取一个project
 * @param {*} _id 
 */
const getProject = _id => {
  return axios_default.a.get(project_apiName + "/project/" + _id).then(res => res);
};
/**
 * 添加一个project
 * @param {*} body 
 */
const addProject = body => {
  return axios_default.a.post(project_apiName + "/project", body).then(res => res);
};

/**
 * 更新一个project
 * @param {*} _id 
 * @param {*} body 
 */
const updateProject = (_id, body) => {
  return axios_default.a.put(project_apiName + "/project/" + _id, body).then(res => res);
};

/**
 * 删除一个project
 * @param {*} _id 
 */
const deleteProject = _id => {
  return axios_default.a.delete(project_apiName + "/project/" + _id).then(res => res);
};
// CONCATENATED MODULE: ./src/api/chat.js


let chat_apiName = '/api';

/**
 * 获取所有chat列表
 * @param {*} query 
 */
const listChat = query => {
  return axios_default.a.get(chat_apiName + "/chat?" + lib_default.a.stringify(query)).then(res => res);
};
/**
 * 获取一个chat
 * @param {*} _id 
 */
const getChat = _id => {
  return axios_default.a.get(chat_apiName + "/chat/" + _id).then(res => res);
};
/**
 * 添加一个chat
 * @param {*} body 
 */
const addChat = body => {
  return axios_default.a.post(chat_apiName + "/chat", body).then(res => res);
};

/**
 * 更新一个chat
 * @param {*} _id 
 * @param {*} body 
 */
const updateChat = (_id, body) => {
  return axios_default.a.put(chat_apiName + "/chat/" + _id, body).then(res => res);
};

/**
 * 删除一个chat
 * @param {*} _id 
 */
const deleteChat = _id => {
  return axios_default.a.delete(chat_apiName + "/chat/" + _id).then(res => res);
};
// CONCATENATED MODULE: ./src/api/task.js


let task_apiName = '/api';

/**
 * 获取所有task列表
 * @param {*} query 
 */
const listTask = query => {
  return axios_default.a.get(task_apiName + "/task?" + lib_default.a.stringify(query)).then(res => res);
};
/**
 * 获取一个task
 * @param {*} _id 
 */
const getTask = _id => {
  return axios_default.a.get(task_apiName + "/task/" + _id).then(res => res);
};
/**
 * 添加一个task
 * @param {*} body 
 */
const addTask = body => {
  return axios_default.a.post(task_apiName + "/task", body).then(res => res);
};

/**
 * 更新一个task
 * @param {*} _id 
 * @param {*} body 
 */
const updateTask = (_id, body) => {
  return axios_default.a.put(task_apiName + "/task/" + _id, body).then(res => res);
};

/**
 * 删除一个task
 * @param {*} _id 
 */
const deleteTask = _id => {
  return axios_default.a.delete(task_apiName + "/task/" + _id).then(res => res);
};
/**
 * 向一个任务上传一个文件
 * @param {*} _id 
 * @param {*} file 
 */
const uploadFile = (_id, file) => {
  let config = {
    headers: {}
  };
  let token = localStorage.getItem("myToken");
  if (token) {
    config.headers.token = token;
  }
  config.headers['Content-Type'] = 'multipart/form-data';
  let formData = new FormData();
  formData.append('taskFile', file);
  return axios_default.a.post(task_apiName + "/task/" + _id + '/file', formData, config).then(res => res);
};
// CONCATENATED MODULE: ./src/api/index.js









//请求拦截器
axios_default.a.interceptors.request.use(function (config) {
    let token = localStorage.getItem("myToken");
    if (token) {
        config.headers.token = token;
    }
    return config;
}, function (error) {
    return Promise.reject(error);
});

//响应拦截器
axios_default.a.interceptors.response.use(function (response) {
    if (response.status === 211) {
        localStorage.setItem('noLogin_message', '获取会话失败，请登录');
        return src_router.push({
            path: '/login'
        });
    }
    return response;
}, function (error) {
    return Promise.reject(error);
});


// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/header.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ var header = ({
  data() {
    return {
      name: '加载中...',
      department: '加载中...',
      roleName: "加载中...",
      role_arr: ['rootadmin', 'admin', 'user'],
      roleName_arr: ['全局管理员', '普通管理员', '普通用户'],
      isAdmin: false
    };
  },
  computed: {},
  mounted() {
    let profile = JSON.parse(localStorage.getItem('myProfile'));
    if (profile.role.name === 'rootadmin') this.isAdmin = true;
    console.log("路由是" + this.$route.path);
    let user = JSON.parse(localStorage.getItem('myProfile'));
    let roleName;
    try {
      roleName = this.roleName_arr[this.role_arr.indexOf(user.role.name)];
    } catch (error) {
      roleName = null;
    }
    this.name = user.name ? user.name : '未知的名字';
    this.department = user.department ? user.department : "未知的部门";
    this.roleName = roleName ? roleName : "未知角色";
  },
  methods: {
    logoutFun: async function () {
      await internal_namespaceObject.logout();
      localStorage.removeItem('myToken');
      localStorage.removeItem('myProfile');
      this.$router.push({ path: "/login" });
    },
    goToAdmin: function () {
      this.$router.push({
        path: '/admin'
      });
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-0066e3fa","hasScoped":false,"transformToRequire":{"video":"src","source":"src","img":"src","image":"xlink:href"},"buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/header.vue
var header_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('mu-flexbox',{staticClass:"header"},[_c('mu-flexbox-item',[_c('div',{staticClass:"logobox"},[_c('router-link',{attrs:{"to":"/index"}},[_c('img',{attrs:{"src":__webpack_require__("iQH9")}}),_vm._v(" 任务管理系统")])],1)]),_vm._v(" "),_c('mu-flexbox-item',[( _vm.$route.path != '/login' )?_c('ul',{staticClass:"userbox"},[_c('li',{on:{"click":_vm.logoutFun}},[_c('mu-badge',{staticClass:"logoutbtn",attrs:{"slot":"after","content":"退出登录"},slot:"after"})],1),_vm._v(" "),(_vm.isAdmin)?_c('li',{on:{"click":_vm.goToAdmin}},[_c('mu-badge',{staticClass:"logoutbtn",attrs:{"slot":"right","content":"前往后台管理"},slot:"right"})],1):_vm._e(),_vm._v(" "),_c('li',[_c('mu-badge',{attrs:{"slot":"right","content":_vm.roleName,"primary":""},slot:"right"})],1),_vm._v(" "),_c('li',[_vm._v("欢迎您！\n        "),_c('em',{staticClass:"username"},[_vm._v(_vm._s(_vm.name))]),_vm._v(" "+_vm._s(_vm.department))])]):_vm._e()])],1)}
var header_staticRenderFns = []
var header_esExports = { render: header_render, staticRenderFns: header_staticRenderFns }
/* harmony default export */ var components_header = (header_esExports);
// CONCATENATED MODULE: ./src/components/header.vue
var header_normalizeComponent = __webpack_require__("VU/8")
/* script */


/* template */

/* template functional */
var header___vue_template_functional__ = false
/* styles */
var header___vue_styles__ = null
/* scopeId */
var header___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var header___vue_module_identifier__ = null
var header_Component = header_normalizeComponent(
  header,
  components_header,
  header___vue_template_functional__,
  header___vue_styles__,
  header___vue_scopeId__,
  header___vue_module_identifier__
)

/* harmony default export */ var src_components_header = (header_Component.exports);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/components/layout.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ var layout = ({
	components: {
		vHeader: src_components_header
	},
	data() {
		return {};
	},
	computed: {},
	mounted() {},
	methods: {}
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-69fc7820","hasScoped":false,"transformToRequire":{"video":"src","source":"src","img":"src","image":"xlink:href"},"buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/components/layout.vue
var layout_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"layout"},[_c('div',{staticClass:"topbox"},[_c('v-header')],1),_vm._v(" "),_c('div',{staticClass:"wrapper"},[_c('transition',{attrs:{"name":"fade","mode":"out-in"}},[_c('router-view')],1)],1)])}
var layout_staticRenderFns = []
var layout_esExports = { render: layout_render, staticRenderFns: layout_staticRenderFns }
/* harmony default export */ var components_layout = (layout_esExports);
// CONCATENATED MODULE: ./src/components/layout.vue
var layout_normalizeComponent = __webpack_require__("VU/8")
/* script */


/* template */

/* template functional */
var layout___vue_template_functional__ = false
/* styles */
var layout___vue_styles__ = null
/* scopeId */
var layout___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var layout___vue_module_identifier__ = null
var layout_Component = layout_normalizeComponent(
  layout,
  components_layout,
  layout___vue_template_functional__,
  layout___vue_styles__,
  layout___vue_scopeId__,
  layout___vue_module_identifier__
)

/* harmony default export */ var src_components_layout = (layout_Component.exports);

// CONCATENATED MODULE: ./src/assets/js/bus.js

/* harmony default export */ var bus = (new vue_esm["default"]());
// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/views/createProject.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ var createProject = ({
	data() {
		return {
			name: '',
			desc: '',
			projectId: "",
			isUpdate: false
		};
	},
	computed: {},
	mounted() {
		let updateProject = JSON.parse(localStorage.getItem('openUpdateProject'));
		if (updateProject) {
			this.isUpdate = true;
			this.name = updateProject.name;
			this.desc = updateProject.desc;
			this.projectId = updateProject._id;
			localStorage.removeItem('openUpdateProject');
		}
	},
	methods: {
		close() {
			this.$emit("closeCreatePro", false);
		},
		addOrUpdateProject: async function () {
			let msg = {
				topPopup: true,
				popup_message: '',
				popup_class: 'popup-success'
			};
			let project = {
				name: this.name,
				desc: this.desc
			};
			if (project.name === '') {
				msg.popup_class = 'popup-error';
				msg.popup_message = '项目名称不能为空';
				return this.$emit('popup', msg);
			}
			if (this.isUpdate) {
				let res = await project_namespaceObject.updateProject(this.projectId, project);
				if (res.status != 200) {
					msg.popup_class = 'popup-error';
					msg.popup_message = res.data.message;

					return this.$emit('popup', msg);
				}
				msg.popup_message = res.data.message;
				this.$emit('popup', msg);
				this.$emit('projectUpdate', 'a project update');
				return this.$emit("closeCreatePro", false);
			}
			let res = await project_namespaceObject.addProject(project);
			if (res.status != 201) {
				msg.popup_class = 'popup-error';
				msg.popup_message = res.data.message;
				return this.$emit('popup', msg);
			}
			msg.popup_message = '操作成功:' + res.data.message;
			this.$emit('popup', msg);
			this.$emit('projectAdd', 'a project added');
			this.$emit("closeCreatePro", false);
		}
	}

});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-bf00d8de","hasScoped":false,"transformToRequire":{"video":"src","source":"src","img":"src","image":"xlink:href"},"buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/views/createProject.vue
var createProject_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"createproject"},[_c('mu-dialog',{staticClass:"dialogbox",attrs:{"open":true,"title":_vm.isUpdate?'更新项目':'创建新项目'},on:{"close":_vm.close}},[_c('mu-text-field',{attrs:{"label":"项目名称","labelFloat":""},model:{value:(_vm.name),callback:function ($$v) {_vm.name=$$v},expression:"name"}}),_c('br'),_vm._v(" "),_c('mu-text-field',{attrs:{"label":"项目简介（选填）","labelFloat":""},model:{value:(_vm.desc),callback:function ($$v) {_vm.desc=$$v},expression:"desc"}}),_c('br'),_vm._v(" "),_c('mu-flat-button',{attrs:{"slot":"actions","primary":"","label":"取消"},on:{"click":_vm.close},slot:"actions"}),_vm._v(" "),_c('mu-flat-button',{attrs:{"slot":"actions","primary":"","label":"确定"},on:{"click":_vm.addOrUpdateProject},slot:"actions"})],1)],1)}
var createProject_staticRenderFns = []
var createProject_esExports = { render: createProject_render, staticRenderFns: createProject_staticRenderFns }
/* harmony default export */ var views_createProject = (createProject_esExports);
// CONCATENATED MODULE: ./src/views/createProject.vue
var createProject_normalizeComponent = __webpack_require__("VU/8")
/* script */


/* template */

/* template functional */
var createProject___vue_template_functional__ = false
/* styles */
var createProject___vue_styles__ = null
/* scopeId */
var createProject___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var createProject___vue_module_identifier__ = null
var createProject_Component = createProject_normalizeComponent(
  createProject,
  views_createProject,
  createProject___vue_template_functional__,
  createProject___vue_styles__,
  createProject___vue_scopeId__,
  createProject___vue_module_identifier__
)

/* harmony default export */ var src_views_createProject = (createProject_Component.exports);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/views/projectList.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ var projectList = ({
	components: {
		createProject: src_views_createProject
	},
	data() {
		return {
			flag: false,
			list: [{
				name: '',
				creator: {
					name: ''
				}
				//featured: true
			}],
			isAdmin: true,
			topPopup: false,
			popup_message: "",
			popup_class: ''

		};
	},
	computed: {},

	methods: {
		projectAdd: function () {
			this.getProjectList();
		},
		sub_popup: function (msg) {
			this.popup_message = msg.popup_message;
			this.popup_class = msg.popup_class;
			this.topPopup = msg.topPopup;
		},
		openCreateProject: function () {
			this.flag = true;
		},
		closeCreateProject: function (data) {
			this.flag = data;
		},
		toTaskList: function (project_id) {
			let query = {
				project: project_id,
				label: ""
			};
			this.$router.push({ path: "/taskList", query: query });
		},
		getProjectList: async function () {
			let res = await project_namespaceObject.listProject();
			this.list = res.data.list;
		}

	},
	mounted() {
		let roleName = JSON.parse(localStorage.getItem('myProfile')).role.name;
		if (roleName == 'user') this.isAdmin = false;
		this.getProjectList();
	},
	watch: {
		topPopup(val) {
			if (val) {
				setTimeout(() => {
					this.topPopup = false;
				}, 2000);
			}
		}
	}
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-ccea5f42","hasScoped":false,"transformToRequire":{"video":"src","source":"src","img":"src","image":"xlink:href"},"buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/views/projectList.vue
var projectList_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"projectslist"},[_c('div',{staticClass:"gridlist-demo-container"},[_c('mu-grid-list',{staticClass:"gridlist-demo",attrs:{"cols":4,"padding":28}},[_c('mu-sub-header',{staticClass:"project-title"},[_vm._v("我拥有的项目")]),_vm._v(" "),_vm._l((_vm.list),function(tile){return _c('mu-grid-tile',{key:tile._id,staticClass:"project-item"},[_c('img',{attrs:{"src":"static/project01.png"},on:{"click":function($event){_vm.toTaskList(tile._id)}}}),_vm._v(" "),_c('span',{attrs:{"slot":"title"},on:{"click":function($event){_vm.toTaskList(tile._id)}},slot:"title"},[_vm._v(_vm._s(tile.name))]),_vm._v(" "),_c('span',{attrs:{"slot":"subTitle"},on:{"click":function($event){_vm.toTaskList(tile._id)}},slot:"subTitle"},[_vm._v("by\n\t\t\t\t\t\t\t"),_c('b',[_vm._v(_vm._s(tile.creator.name))])]),_vm._v(" "),_c('mu-icon-button',{attrs:{"slot":"action","icon":"star_border"},on:{"click":_vm.toTaskList},slot:"action"})],1)}),_vm._v(" "),(_vm.isAdmin)?_c('mu-grid-tile',{staticClass:"project-item manager-addpro"},[_c('div',{staticClass:"m-addpro",on:{"click":_vm.openCreateProject}},[_c('p',{staticClass:"addicon"},[_c('mu-icon',{attrs:{"value":"control_point","color":"#a6a6a6","size":36}})],1)]),_vm._v(" "),_c('span',{attrs:{"slot":"title"},slot:"title"},[_vm._v("创建新项目")]),_vm._v(" "),_c('span',{attrs:{"slot":"subTitle"},slot:"subTitle"},[_vm._v("\n\t\t\t\t\t\t管理员\n\t\t\t\t\t")])]):_vm._e()],2)],1),_vm._v(" "),(_vm.flag)?_c('create-project',{on:{"projectAdd":_vm.projectAdd,"popup":_vm.sub_popup,"closeCreatePro":_vm.closeCreateProject}}):_vm._e(),_vm._v(" "),_c('mu-popup',{attrs:{"position":"top","overlay":false,"popupClass":['popup-default',_vm.popup_class],"open":_vm.topPopup}},[_vm._v("\n        "+_vm._s(_vm.popup_message)+"\n    ")])],1)}
var projectList_staticRenderFns = []
var projectList_esExports = { render: projectList_render, staticRenderFns: projectList_staticRenderFns }
/* harmony default export */ var views_projectList = (projectList_esExports);
// CONCATENATED MODULE: ./src/views/projectList.vue
function projectList_injectStyle (ssrContext) {
  __webpack_require__("JbHr")
}
var projectList_normalizeComponent = __webpack_require__("VU/8")
/* script */


/* template */

/* template functional */
var projectList___vue_template_functional__ = false
/* styles */
var projectList___vue_styles__ = projectList_injectStyle
/* scopeId */
var projectList___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var projectList___vue_module_identifier__ = null
var projectList_Component = projectList_normalizeComponent(
  projectList,
  views_projectList,
  projectList___vue_template_functional__,
  projectList___vue_styles__,
  projectList___vue_scopeId__,
  projectList___vue_module_identifier__
)

/* harmony default export */ var src_views_projectList = (projectList_Component.exports);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/views/taskList.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ var taskList = ({
		data() {
				return {
						projectList: [{
								name: "加载中...",
								_id: "1"
						}],
						projectId: "1",
						taskList: [],
						label: "",
						isUser: true
				};
		},
		computed: {},
		async mounted() {
				let roleName = JSON.parse(localStorage.getItem('myProfile')).role.name;
				if (roleName == 'user') this.isUser = false;

				this.label = this.$route.query.label;
				let _this = this;
				bus.$on('complete', function (msg) {
						console.log('kown some task complete');
						_this.getTaskList(_this.projectId);
				});
				bus.$on('delete', function (msg) {
						console.log('kown some task delete');
						_this.getTaskList(_this.projectId);
				});
				bus.$on('taskAdd', function (msg) {
						console.log('kown a task added');
						_this.getTaskList(_this.projectId);
				});
				bus.$on('update', function (msg) {
						console.log('kown a task update');
						_this.getTaskList(_this.projectId);
				});
				await this.getProjectList();
				await this.getTaskList(this.projectId);
		},
		methods: {
				getTaskList: async function (projectId) {
						let query = {
								project: projectId,
								label: this.label
						};
						let res = await task_namespaceObject.listTask(query);
						this.taskList = res.data.list;
				},
				getProjectList: async function () {
						let res = await project_namespaceObject.listProject();
						this.projectList = res.data.list;
						this.projectId = this.$route.query.project;
				},
				goCreateTask: function () {
						this.$router.push({
								path: "createTask",
								query: {
										project: this.$route.query.project
								}
						});
				},
				handleChange(projectId) {
						this.projectId = projectId;
						this.$router.push({
								name: "taskList",
								query: { project: projectId }
						});
						this.getTaskList(this.projectId);
				},
				sendMsgToContent: function (id) {
						bus.$emit('taskChange', id);
				},
				chanegLabel: function (label) {
						this.label = label;
						this.$router.push({
								path: '/taskList',
								query: {
										project: this.projectId,
										label: this.label
								}
						});
						this.getTaskList(this.projectId);
				}
		}
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-0cf63a11","hasScoped":false,"transformToRequire":{"video":"src","source":"src","img":"src","image":"xlink:href"},"buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/views/taskList.vue
var taskList_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"taskwrap"},[_c('mu-flexbox',{staticClass:"subheader"},[_c('mu-flexbox-item',[_c('div',{staticClass:"breadbox"},[_c('div',{staticClass:"breadspan"},[_c('router-link',{attrs:{"to":"/index"}},[_vm._v("项目  >")])],1),_vm._v(" "),_c('mu-dropDown-menu',{attrs:{"value":_vm.projectId},on:{"change":_vm.handleChange}},_vm._l((_vm.projectList),function(item){return _c('mu-menu-item',{key:item._id,attrs:{"value":item._id,"title":item.name}})}))],1)]),_vm._v(" "),_c('mu-flexbox-item',[(_vm.isUser)?_c('mu-flat-button',{staticClass:"fright",attrs:{"label":"添加任务","icon":"description","primary":""},on:{"click":_vm.goCreateTask}}):_vm._e()],1)],1),_vm._v(" "),_c('div',{staticClass:"taskbox"},[_c('div',{staticClass:"sidebox"},[_c('div',{staticClass:"toolbar"},[_c('mu-chip',{attrs:{"backgroundColor":"#2196f3","color":"#fff"},on:{"click":function($event){_vm.chanegLabel('')}}},[_vm._v("全部 ")]),_vm._v(" "),_c('mu-chip',{attrs:{"backgroundColor":"#2196f3","color":"#fff"},on:{"click":function($event){_vm.chanegLabel('沟通中')}}},[_vm._v("沟通中 ")]),_vm._v(" "),_c('mu-chip',{attrs:{"backgroundColor":"#2196f3","color":"#fff"},on:{"click":function($event){_vm.chanegLabel('已完成')}}},[_vm._v("已完成 ")]),_vm._v(" "),_c('mu-chip',{attrs:{"backgroundColor":"#2196f3","color":"#fff"},on:{"click":function($event){_vm.chanegLabel('超时')}}},[_vm._v("超时 ")])],1),_vm._v(" "),_c('div',{staticClass:"tasklist"},[_c('mu-list',_vm._l((_vm.taskList),function(task){return _c('mu-list-item',{key:task._id,attrs:{"to":{path:'/taskContent',query:{project:task.project._id,task:task._id,label:_vm.label}},"title":task.topic,"describeText":_vm._f("timeFilter")(task.createAt)},on:{"click":function($event){_vm.sendMsgToContent(task._id)}}},[_c('mu-icon',{attrs:{"slot":"left","value":"inbox"},slot:"left"}),_vm._v(" "),_c('mu-badge',{attrs:{"slot":"right","primary":"","content":task.label},slot:"right"})],1)}))],1)]),_vm._v(" "),_c('div',{staticClass:"mainbox"},[_c('router-view')],1)])],1)}
var taskList_staticRenderFns = []
var taskList_esExports = { render: taskList_render, staticRenderFns: taskList_staticRenderFns }
/* harmony default export */ var views_taskList = (taskList_esExports);
// CONCATENATED MODULE: ./src/views/taskList.vue
var taskList_normalizeComponent = __webpack_require__("VU/8")
/* script */


/* template */

/* template functional */
var taskList___vue_template_functional__ = false
/* styles */
var taskList___vue_styles__ = null
/* scopeId */
var taskList___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var taskList___vue_module_identifier__ = null
var taskList_Component = taskList_normalizeComponent(
  taskList,
  views_taskList,
  taskList___vue_template_functional__,
  taskList___vue_styles__,
  taskList___vue_scopeId__,
  taskList___vue_module_identifier__
)

/* harmony default export */ var src_views_taskList = (taskList_Component.exports);

// EXTERNAL MODULE: ./src/assets/js/socket.io.js
var socket_io = __webpack_require__("Db8+");
var socket_io_default = /*#__PURE__*/__webpack_require__.n(socket_io);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/views/taskContent.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ var taskContent = ({
  data() {
    return {
      thisTask: {},
      projectId: '',
      taskId: "",
      noComplete: true,
      taskLabel: "",
      taskFile: '',
      taskTopic: "加载中...",
      creatorName: "加载中...",
      executorName: "加载中...",
      deadLine: "加载中...",
      taskRemark: "加载中",
      task_priority: 0,
      priority_arr: ["普通", "紧急", "非常紧急"],
      chatList: [],
      delete_dialog: false,
      complete_dialog: false,
      label_dialog: false,
      topPopup: false,
      popup_message: "",
      popup_class: '',
      role_arr: ['rootadmin', 'admin', 'user'],
      roleName_arr: ['全局管理员', '普通管理员', '普通用户'],
      msg: "",
      isAdmin: true,
      isRootAdmin: true,
      hasFile: false,
      changeLabel: '',
      file: ''
    };
  },
  computed: {},
  async mounted() {
    let roleName = JSON.parse(localStorage.getItem('myProfile')).role.name;
    if (roleName != 'admin') this.isAdmin = false;
    if (roleName != "rootadmin") this.isRootAdmin = false;
    this.projectId = this.$route.query.projectId;
    let _this = this;
    bus.$on('taskChange', async taskId => {
      _this.taskId = taskId;
      await _this.getTask(taskId);
      _this.getChatList(taskId);
      if (_this.taskLabel === '已完成') _this.noComplete = false;else _this.noComplete = true;
    });
    this.taskId = this.$route.query.task;
    await this.getTask(this.taskId);
    if (this.taskFile !== '') this.hasFile = true;
    this.getChatList(this.taskId);
    this.syncChat();
  },
  methods: {
    syncChat() {
      let socket = socket_io_default.a.connect(":3000");
      let _this = this;
      socket.on('task/' + _this.taskId, function (msg) {
        _this.getChatList(_this.taskId);
      });
    },
    getTask: async function (taskId) {
      let res = await task_namespaceObject.getTask(taskId);
      this.thisTask = res.data;
      this.taskTopic = res.data.topic;
      this.task_priority = res.data.priority;
      this.creatorName = res.data.creator.name;
      this.executorName = res.data.executor.name;
      this.taskRemark = res.data.remark;
      this.deadLine = res.data.deadLine;
      this.taskLabel = res.data.label;
      this.taskFile = res.data.file ? res.data.file : '';
      if (res.data.taskLabel == '已完成') this.noComplete = false;
    },
    getFileAndUpload: async function (event) {
      this.file = event.target.files[0];
      console.log(this.file);
      let res = await task_namespaceObject.uploadFile(this.taskId, this.file);
      if (res.status !== 201) {
        return this.showPopup('操作失败：' + res.data.message, 'popup-error');
      }
      this.getTask(this.taskId);
      this.hasFile = true;
      return this.showPopup('操作成功：' + res.data.message, 'popup-success');
    },
    getChatList: async function (taskId) {
      let query = {
        task: taskId
      };
      let res = await chat_namespaceObject.listChat(query);
      this.chatList = res.data.list;
    },
    deleteTask: async function (taskId) {
      this.delete_dialog = false;
      let res = await task_namespaceObject.deleteTask(taskId);
      if (res.status !== 200) {
        return this.showPopup('操作失败：' + res.data.message, 'popup-error');
      } else {
        this.showPopup('操作成功：' + res.data.message, 'popup-success');
        bus.$emit('delete', 'some task delete');
        this.$router.push({
          path: '/taskList',
          project: this.projectId
        });
      }
    },
    completeTask: async function (taskId) {
      this.complete_dialog = false;
      let body = {
        label: '已完成'
      };
      let res = await task_namespaceObject.updateTask(taskId, body);
      if (res.status !== 200) {
        return this.showPopup('操作失败：' + res.data.message, 'popup-error');
      } else {
        this.showPopup('操作成功：' + res.data.message, 'popup-success');
        bus.$emit('complete', 'some task complete');
        this.getTask(this.taskId);
        this.getChatList(this.taskId);
        this.noComplete = false;
      }
    },
    updateLabel: async function (label) {
      this.label_dialog = false;
      let body = {
        label: label
      };
      let res = await task_namespaceObject.updateTask(this.taskId, body);
      if (res.status !== 200) {
        return this.showPopup('操作失败：' + res.data.message, 'popup-error');
      } else {
        this.showPopup('操作成功：' + res.data.message, 'popup-success');
        bus.$emit('update', 'some task complete');
        this.getTask(this.taskId);
        this.getChatList(this.taskId);
      }
    },
    open_dialog: function (which) {
      if (which === 'delete') return this.delete_dialog = true;
      if (which === 'complete') return this.complete_dialog = true;
      if (which === 'label') return this.label_dialog = true;
    },
    cancel_dialog: function () {
      if (this.complete_dialog) return this.complete_dialog = false;
      if (this.delete_dialog) return this.delete_dialog = false;
      if (this.label_dialog) return this.label_dialog = false;
    },
    sendMsg: async function (msg) {
      if (msg === '' || !msg) {
        return this.showPopup('输入消息不能为空', 'popup-error');
      }
      let body = {
        task: this.taskId,
        text: msg
      };
      let res = await chat_namespaceObject.addChat(body);
      if (res.status !== 201) {
        return this.showPopup('操作失败：' + res.data.message, 'popup-error');
      } else {
        this.showPopup('操作成功：' + res.data.message, 'popup-success');
        this.msg = '';
        this.getChatList(this.taskId);
      }
    },
    toUpdateTask(thisTask) {
      localStorage.setItem('updateTask', JSON.stringify(thisTask));
      this.$router.push({
        path: '/createTask'
      });
    },
    showPopup(msg, type) {
      this.popup_message = msg;
      this.popup_class = type;
      this.topPopup = true;
    }
  },
  watch: {
    topPopup(val) {
      if (val) {
        setTimeout(() => {
          this.topPopup = false;
        }, 2000);
      }
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-e342a7fc","hasScoped":false,"transformToRequire":{"video":"src","source":"src","img":"src","image":"xlink:href"},"buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/views/taskContent.vue
var taskContent_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"taskContent"},[_c('div',{staticClass:"taskinfo"},[_c('h3',{staticClass:"title"},[_vm._v("任务主题："+_vm._s(_vm.taskTopic))]),_vm._v(" "),_c('p',{staticClass:"info"},[_c('span',[_vm._v("发布者："+_vm._s(_vm.creatorName))]),_vm._v(" "),_c('span',[_vm._v(_vm._s(_vm._f("timeFilter")(_vm.deadLine))+" 截止 ")]),_vm._v(" "),_c('mu-badge',{attrs:{"slot":"right","primary":"","content":_vm.priority_arr[_vm.task_priority]},slot:"right"}),_vm._v(" "),_c('br'),_vm._v(" "),_c('span',[_vm._v("执行者："+_vm._s(_vm.executorName))])],1),_vm._v(" "),_c('p',{staticClass:"cont"},[_vm._v(_vm._s(_vm.taskRemark)+" ")]),_vm._v(" "),(_vm.hasFile)?_c('a',{staticClass:"download",attrs:{"href":'/storage/'+_vm.taskFile}},[_vm._v("下载附件")]):_vm._e(),_vm._v(" "),_c('div',{staticClass:"infobtn"},[(_vm.isAdmin)?_c('mu-raised-button',{attrs:{"label":"上传文件"}},[_c('input',{staticClass:"file-button",attrs:{"type":"file","primary":""},on:{"change":function($event){_vm.getFileAndUpload($event)}}})]):_vm._e(),_vm._v(" "),(_vm.noComplete&&_vm.isAdmin)?_c('mu-raised-button',{attrs:{"label":"已完成","primary":""},on:{"click":function($event){_vm.open_dialog('complete')}}}):_vm._e(),_vm._v(" "),(_vm.isRootAdmin)?_c('mu-raised-button',{attrs:{"label":"更变状态","primary":""},on:{"click":function($event){_vm.open_dialog('label')}}}):_vm._e(),_vm._v(" "),(_vm.isRootAdmin)?_c('mu-raised-button',{attrs:{"label":"编辑","primary":""},on:{"click":function($event){_vm.toUpdateTask(_vm.thisTask)}}}):_vm._e(),_vm._v(" "),(_vm.isAdmin||_vm.isRootAdmin)?_c('mu-raised-button',{attrs:{"label":"删除","secondary":""},on:{"click":function($event){_vm.open_dialog('delete')}}}):_vm._e()],1),_vm._v(" "),_c('mu-dialog',{attrs:{"open":_vm.label_dialog,"title":"更变状态"},on:{"close":_vm.cancel_dialog}},[_c('mu-menu',[_c('mu-menu-item',{attrs:{"title":"沟通中"},on:{"click":function($event){_vm.updateLabel('沟通中')}}}),_vm._v(" "),_c('mu-menu-item',{attrs:{"title":"已完成"},on:{"click":function($event){_vm.updateLabel('已完成')}}})],1),_vm._v(" "),_c('mu-flat-button',{attrs:{"slot":"actions","primary":"","label":"取消"},on:{"click":_vm.cancel_dialog},slot:"actions"})],1),_vm._v(" "),_c('mu-dialog',{attrs:{"open":_vm.complete_dialog,"title":"完成确认"},on:{"close":_vm.cancel_dialog}},[_vm._v("\n        确定将任务\""+_vm._s(_vm.taskTopic)+"\"标记为完成?\n        "),_c('mu-flat-button',{attrs:{"slot":"actions","primary":"","label":"取消"},on:{"click":_vm.cancel_dialog},slot:"actions"}),_vm._v(" "),_c('mu-flat-button',{attrs:{"slot":"actions","primary":"","label":"确定"},on:{"click":function($event){_vm.completeTask(_vm.taskId)}},slot:"actions"})],1),_vm._v(" "),_c('mu-dialog',{attrs:{"open":_vm.delete_dialog,"title":"删除确认"},on:{"close":_vm.cancel_dialog}},[_vm._v("\n        确定删除任务:"+_vm._s(_vm.taskTopic)+"？\n        "),_c('mu-flat-button',{attrs:{"slot":"actions","primary":"","label":"取消"},on:{"click":_vm.cancel_dialog},slot:"actions"}),_vm._v(" "),_c('mu-flat-button',{attrs:{"slot":"actions","primary":"","label":"确定"},on:{"click":function($event){_vm.deleteTask(_vm.taskId)}},slot:"actions"})],1)],1),_vm._v(" "),_c('div',{staticClass:"taskcont"},[_c('div',{staticClass:"taskcont-list"},_vm._l((_vm.chatList),function(chat){return _c('mu-content-block',{key:chat._id},[_c('div',{staticClass:"itemhead"},[_c('span',{staticClass:"itemspan"},[_vm._v(_vm._s(chat.userName))]),_vm._v(" "),_c('mu-badge',{attrs:{"primary":"","content":_vm.roleName_arr[_vm.role_arr.indexOf(chat.roleName)]}}),_vm._v(" "),_c('span',{staticClass:"itemspan time"},[_vm._v(_vm._s(_vm._f("timeFilter")(chat.createAt)))])],1),_vm._v(" "),_c('div',{staticClass:"itemtxt"},[_vm._v(_vm._s(chat.text))])])})),_vm._v(" "),_c('div',{staticClass:"taskcont-text"},[_c('mu-text-field',{attrs:{"hintText":"请输入回复内容","multiLine":"","rows":3,"rowsMax":6,"fullWidth":"","icon":"comment"},model:{value:(_vm.msg),callback:function ($$v) {_vm.msg=$$v},expression:"msg"}}),_vm._v(" "),_c('mu-raised-button',{staticClass:"sentbtn",attrs:{"label":"发送","primary":""},on:{"click":function($event){_vm.sendMsg(_vm.msg)}}})],1),_vm._v(" "),_c('mu-popup',{attrs:{"position":"top","overlay":false,"popupClass":['popup-default',_vm.popup_class],"open":_vm.topPopup}},[_vm._v("\n        "+_vm._s(_vm.popup_message)+"\n    ")])],1)])}
var taskContent_staticRenderFns = []
var taskContent_esExports = { render: taskContent_render, staticRenderFns: taskContent_staticRenderFns }
/* harmony default export */ var views_taskContent = (taskContent_esExports);
// CONCATENATED MODULE: ./src/views/taskContent.vue
var taskContent_normalizeComponent = __webpack_require__("VU/8")
/* script */


/* template */

/* template functional */
var taskContent___vue_template_functional__ = false
/* styles */
var taskContent___vue_styles__ = null
/* scopeId */
var taskContent___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var taskContent___vue_module_identifier__ = null
var taskContent_Component = taskContent_normalizeComponent(
  taskContent,
  views_taskContent,
  taskContent___vue_template_functional__,
  taskContent___vue_styles__,
  taskContent___vue_scopeId__,
  taskContent___vue_module_identifier__
)

/* harmony default export */ var src_views_taskContent = (taskContent_Component.exports);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/views/createTask.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ var createTask = ({
  data() {
    return {
      userList: [],
      _id: '',
      project: "",
      topic: "",
      remark: "",
      deadLine_date: "",
      deadLine_time: "",
      priority: "0",
      executor: "",
      file: "",
      topPopup: false,
      popup_message: "",
      popup_class: "",
      isUpdate: false
    };
  },
  computed: {},
  async mounted() {
    this.project = this.$route.query.project;
    await this.getUserList();
    let updateTask = JSON.parse(localStorage.getItem("updateTask"));
    if (updateTask) {
      console.log(updateTask);
      let deadLine = new Date(updateTask.deadLine);
      this._id = updateTask._id, this.project = updateTask.project._id, this.topic = updateTask.topic, this.remark = updateTask.remark, this.priority = updateTask.priority.toString(), this.executor = updateTask.executor._id;
      let month = (deadLine.getMonth() + 1).toString();
      if (month.length < 2) month = '0' + month;
      let date = deadLine.getDate().toString();
      if (date.length < 2) date = '0' + date;
      let hours = deadLine.getHours().toString();
      if (hours.length < 2) hours = '0' + hours;
      let minutes = deadLine.getMinutes().toString();
      if (minutes.length < 2) date = '0' + minutes;
      this.deadLine_date = deadLine.getFullYear() + "-" + month + "-" + date;
      this.deadLine_time = hours + ":" + minutes;
      this.isUpdate = true;
      localStorage.removeItem("updateTask");
    }
  },
  methods: {
    getUserList: async function () {
      let res = await user_namespaceObject.listUser();
      this.userList = res.data.list;
    },
    getFile: function (event) {
      this.file = event.target.files[0];
      console.log(this.file);
    },
    submit: async function () {
      let task_add = {
        project: this.project,
        topic: this.topic,
        deadLine: new Date(this.deadLine_date + " " + this.deadLine_time),
        priority: parseInt(this.priority),
        executor: this.executor,
        remark: this.remark
      };
      let task_update = {
        topic: this.topic,
        deadLine: new Date(this.deadLine_date + " " + this.deadLine_time),
        priority: parseInt(this.priority),
        executor: this.executor,
        remark: this.remark
      };
      console.log(task_update.priority);
      let res;
      if (this.isUpdate) {
        res = await task_namespaceObject.updateTask(this._id, task_update);
        if (res.status !== 200) {
          return this.showPopup("操作失败：" + res.data.message, "popup-error");
        }
      }
      if (!this.isUpdate) {

        res = await task_namespaceObject.addTask(task_add);
        if (res.status !== 201) {
          return this.showPopup("操作失败：" + res.data.message, "popup-error");
        }
        this._id = res.data._id;
      }

      if (this.file && this.file != "") await task_namespaceObject.uploadFile(this._id, this.file);
      this.showPopup("操作成功：" + res.data.message, "popup-success");
      this.$router.push({
        path: "/taskContent",
        query: {
          project: this.project,
          task: this._id
        }
      });
      bus.$emit("taskAdd", "a task added");
    },
    showPopup(msg, type) {
      this.popup_message = msg;
      this.popup_class = type;
      this.topPopup = true;
    }
  },
  topPopup(val) {
    if (val) {
      setTimeout(() => {
        this.topPopup = false;
      }, 2000);
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-2314ba47","hasScoped":false,"transformToRequire":{"video":"src","source":"src","img":"src","image":"xlink:href"},"buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/views/createTask.vue
var createTask_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"createtask"},[_c('div',{staticClass:"createtask-form"},[_c('ul',[_c('li',[_c('label',{staticClass:"labelbox"},[_vm._v("任务主题")]),_vm._v(" "),_c('mu-text-field',{attrs:{"hintText":"请输入任务主题"},model:{value:(_vm.topic),callback:function ($$v) {_vm.topic=$$v},expression:"topic"}})],1),_vm._v(" "),_c('li',[_c('label',{staticClass:"labelbox"},[_vm._v("截止日期")]),_vm._v(" "),_c('mu-date-picker',{attrs:{"hintText":"请选择日期"},model:{value:(_vm.deadLine_date),callback:function ($$v) {_vm.deadLine_date=$$v},expression:"deadLine_date"}})],1),_vm._v(" "),_c('li',[_c('label',{staticClass:"labelbox"},[_vm._v("截止时间")]),_vm._v(" "),_c('mu-time-picker',{attrs:{"hintText":"请选择时间","format":"24hr"},model:{value:(_vm.deadLine_time),callback:function ($$v) {_vm.deadLine_time=$$v},expression:"deadLine_time"}})],1),_vm._v(" "),_c('li',{staticClass:"radiobox"},[_c('label',{staticClass:"labelbox"},[_vm._v("优先级")]),_vm._v(" "),_c('mu-radio',{staticClass:"createtask-radio",attrs:{"label":"普通","name":"group","nativeValue":"0"},model:{value:(_vm.priority),callback:function ($$v) {_vm.priority=$$v},expression:"priority"}}),_vm._v(" "),_c('mu-radio',{staticClass:"createtask-radio",attrs:{"label":"紧急","name":"group","nativeValue":"1"},model:{value:(_vm.priority),callback:function ($$v) {_vm.priority=$$v},expression:"priority"}}),_vm._v(" "),_c('mu-radio',{staticClass:"createtask-radio",attrs:{"label":"非常紧急","name":"group","nativeValue":"2"},model:{value:(_vm.priority),callback:function ($$v) {_vm.priority=$$v},expression:"priority"}})],1),_vm._v(" "),_c('li',{staticClass:"note"},[_c('label',{staticClass:"labelbox labelnote"},[_vm._v("备注")]),_vm._v(" "),_c('mu-text-field',{attrs:{"hintText":"请输入备注信息","multiLine":"","rows":3,"rowsMax":6},model:{value:(_vm.remark),callback:function ($$v) {_vm.remark=$$v},expression:"remark"}})],1),_vm._v(" "),_c('li',[_c('label',{staticClass:"labelbox"},[_vm._v("上传文件")]),_vm._v(" "),_c('mu-raised-button',{attrs:{"label":"选择文件"}},[_c('input',{staticClass:"file-button",attrs:{"type":"file"},on:{"change":function($event){_vm.getFile($event)}}})])],1),_vm._v(" "),_c('li',{staticClass:"note"},[_c('label',{staticClass:"labelbox labelworker"},[_vm._v("执行人员")]),_vm._v(" "),_c('mu-select-field',{attrs:{"hintText":"选择人员","maxHeight":300},model:{value:(_vm.executor),callback:function ($$v) {_vm.executor=$$v},expression:"executor"}},_vm._l((_vm.userList),function(item){return _c('mu-menu-item',{key:item._id,attrs:{"title":item.name,"value":item._id}})}))],1),_vm._v(" "),_c('li',[_c('mu-raised-button',{staticClass:"createtask-btn",attrs:{"label":_vm.isUpdate?'更新':'确定',"primary":""},on:{"click":_vm.submit}})],1)]),_vm._v(" "),_c('br')]),_vm._v(" "),_c('mu-popup',{attrs:{"position":"top","overlay":false,"popupClass":['popup-default',_vm.popup_class],"open":_vm.topPopup}},[_vm._v("\n\t\t"+_vm._s(_vm.popup_message)+"\n\t")])],1)}
var createTask_staticRenderFns = []
var createTask_esExports = { render: createTask_render, staticRenderFns: createTask_staticRenderFns }
/* harmony default export */ var views_createTask = (createTask_esExports);
// CONCATENATED MODULE: ./src/views/createTask.vue
function createTask_injectStyle (ssrContext) {
  __webpack_require__("Udrb")
}
var createTask_normalizeComponent = __webpack_require__("VU/8")
/* script */


/* template */

/* template functional */
var createTask___vue_template_functional__ = false
/* styles */
var createTask___vue_styles__ = createTask_injectStyle
/* scopeId */
var createTask___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var createTask___vue_module_identifier__ = null
var createTask_Component = createTask_normalizeComponent(
  createTask,
  views_createTask,
  createTask___vue_template_functional__,
  createTask___vue_styles__,
  createTask___vue_scopeId__,
  createTask___vue_module_identifier__
)

/* harmony default export */ var src_views_createTask = (createTask_Component.exports);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/views/login.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ var views_login = ({
  data() {
    return {

      account: "",
      password: "",
      account_error: "",
      password_error: "",
      topPopup: false,
      popup_message: "",
      popup_class: ''
    };
  },
  computed: {},
  mounted() {
    let noLogin_message = localStorage.getItem('noLogin_message');
    if (!noLogin_message || noLogin_message === '') return;
    this.popup_message = noLogin_message;
    this.popup_class = 'popup-error';
    localStorage.removeItem('noLogin_message');
    return this["topPopup"] = true;
  },
  methods: {
    handel_account_focus: function () {
      if (this.account === "") return this.account_error = "请输入账号";
    },
    handel_account_input: function (value) {
      if (value === "") return this.account_error = "请输入账号";else return this.account_error = "";
    },
    handel_password_focus: function () {
      if (this.password === "") return this.password_error = "请输入密码";
    },
    handel_password_input: function (value) {
      if (value === "") return this.password_error = "请输密码";else return this.password_error = "";
    },

    LoginFun: async function () {
      let password = this.password;
      let account = this.account;
      if (password === "" || account === "") {
        if (account === "") return this.account_error = "账号不能为空";
        if (password === "") return this.password_error = "密码不能为空";
      }
      let res = await internal_namespaceObject.login({ account: account, password: password });
      //登录失败
      if (res.status !== 200 && !res.data.token) {
        this.popup_message = res.data.message;
        this.popup_class = 'popup-error';
        return this["topPopup"] = true;
        //登录成功
      } else {
        //保存token
        localStorage.setItem('myToken', res.data.token);
        //访问个人数据
        let profile_res = await internal_namespaceObject.getProfile();
        if (profile_res.status !== 200) {
          this.popup_message = '服务器出错';
          this.popup_class = 'popup-error';
          return this["topPopup"] = true;
        }
        localStorage.setItem('myProfile', JSON.stringify(profile_res.data));
        this.$router.push("/index");
      }
    }
  },
  watch: {
    topPopup(val) {
      if (val) {
        setTimeout(() => {
          this.topPopup = false;
        }, 2000);
      }
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-2e432c3a","hasScoped":false,"transformToRequire":{"video":"src","source":"src","img":"src","image":"xlink:href"},"buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/views/login.vue
var login_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"loginwrap"},[_vm._m(0),_vm._v(" "),_c('div',{staticClass:"loginform"},[_c('mu-text-field',{attrs:{"label":"E-mail","errorText":_vm.account_error,"hintText":"账户名／邮箱／手机","labelFloat":"","fullWidth":""},on:{"focus":_vm.handel_account_focus,"input":_vm.handel_account_input},model:{value:(_vm.account),callback:function ($$v) {_vm.account=$$v},expression:"account"}}),_c('br'),_vm._v(" "),_c('mu-text-field',{attrs:{"label":"密码","type":"password","errorText":_vm.password_error,"labelFloat":"","fullWidth":""},on:{"focus":_vm.handel_password_focus,"input":_vm.handel_password_input},model:{value:(_vm.password),callback:function ($$v) {_vm.password=$$v},expression:"password"}}),_c('br'),_vm._v(" "),_c('mu-raised-button',{staticClass:"loginbtn",attrs:{"label":"登录","primary":"","fullWidth":""},on:{"click":_vm.LoginFun}}),_vm._v(" "),_c('mu-popup',{attrs:{"position":"top","overlay":false,"popupClass":['popup-default',_vm.popup_class],"open":_vm.topPopup}},[_vm._v("\n      "+_vm._s(_vm.popup_message)+"\n  ")])],1)])}
var login_staticRenderFns = [function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"logintop"},[_c('div',{staticClass:"brand"},[_vm._v("任务管理系统")])])}]
var login_esExports = { render: login_render, staticRenderFns: login_staticRenderFns }
/* harmony default export */ var selectortype_template_index_0_src_views_login = (login_esExports);
// CONCATENATED MODULE: ./src/views/login.vue
function login_injectStyle (ssrContext) {
  __webpack_require__("SSNF")
}
var login_normalizeComponent = __webpack_require__("VU/8")
/* script */


/* template */

/* template functional */
var login___vue_template_functional__ = false
/* styles */
var login___vue_styles__ = login_injectStyle
/* scopeId */
var login___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var login___vue_module_identifier__ = null
var login_Component = login_normalizeComponent(
  views_login,
  selectortype_template_index_0_src_views_login,
  login___vue_template_functional__,
  login___vue_styles__,
  login___vue_scopeId__,
  login___vue_module_identifier__
)

/* harmony default export */ var src_views_login = (login_Component.exports);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/views/admin/admLayout.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ var admLayout = ({
  data() {
    return {
      value: 1

    };
  },
  computed: {},
  mounted() {},
  methods: {
    handleChange(val) {
      this.value = val;
    }

  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-b9912634","hasScoped":false,"transformToRequire":{"video":"src","source":"src","img":"src","image":"xlink:href"},"buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/views/admin/admLayout.vue
var admLayout_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"adm-wrapper"},[_c('div',{staticClass:"adm-sidebox"},[_c('mu-list',{attrs:{"value":_vm.value},on:{"change":_vm.handleChange}},[_c('mu-list-item',{attrs:{"value":1,"title":"权限管理","toggleNested":""}},[_c('mu-icon',{attrs:{"slot":"left","value":"inbox"},slot:"left"}),_vm._v(" "),_c('mu-list-item',{attrs:{"slot":"nested","to":"userList","value":2,"title":"人员列表","inset":""},slot:"nested"}),_vm._v(" "),_c('mu-list-item',{attrs:{"slot":"nested","to":"createUser","value":3,"title":"添加人员","inset":""},slot:"nested"})],1),_vm._v(" "),_c('mu-list-item',{attrs:{"value":4,"title":"项目管理","toggleNested":""}},[_c('mu-icon',{attrs:{"slot":"left","value":"inbox"},slot:"left"}),_vm._v(" "),_c('mu-list-item',{attrs:{"slot":"nested","to":"projectList","value":5,"title":"项目列表","inset":""},slot:"nested"})],1)],1)],1),_vm._v(" "),_c('div',{staticClass:"adm-mainbox"},[_c('router-view')],1)])}
var admLayout_staticRenderFns = []
var admLayout_esExports = { render: admLayout_render, staticRenderFns: admLayout_staticRenderFns }
/* harmony default export */ var admin_admLayout = (admLayout_esExports);
// CONCATENATED MODULE: ./src/views/admin/admLayout.vue
var admLayout_normalizeComponent = __webpack_require__("VU/8")
/* script */


/* template */

/* template functional */
var admLayout___vue_template_functional__ = false
/* styles */
var admLayout___vue_styles__ = null
/* scopeId */
var admLayout___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var admLayout___vue_module_identifier__ = null
var admLayout_Component = admLayout_normalizeComponent(
  admLayout,
  admin_admLayout,
  admLayout___vue_template_functional__,
  admLayout___vue_styles__,
  admLayout___vue_scopeId__,
  admLayout___vue_module_identifier__
)

/* harmony default export */ var views_admin_admLayout = (admLayout_Component.exports);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/views/admin/userList.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ var userList = ({
  data() {
    return {
      userList: [],
      total: 100,
      current: 1,
      pageSize: 2,
      pageSizeOption: [5, 10, 15, 20],
      topPopup: false,
      popup_message: "",
      popup_class: "",
      delete_dialog: false,
      selectUserName: "",
      selectUserId: ""
    };
  },
  computed: {},
  mounted() {
    this.getUserList();
    let msg = localStorage.getItem("msg");
    if (msg) {
      this.showPopup(msg, "popup-success");
      localStorage.removeItem("msg");
    }
  },
  methods: {
    async changePage(newPage) {
      this.current = newPage;
      let query = {
        size: this.pageSize,
        page: this.current
      };
      let res = await user_namespaceObject.listUser(query);
      this.userList = res.data.list;
      this.total = res.data.count;
    },
    async changePageSize(newSize) {
      this.pageSize = newSize;
      let query = {
        size: this.pageSize,
        page: this.current
      };
      let res = await user_namespaceObject.listUser(query);
      this.userList = res.data.list;
      this.total = res.data.count;
    },
    async getUserList() {
      let query = {
        size: this.pageSize,
        page: this.current
      };
      let res = await user_namespaceObject.listUser(query);
      this.userList = res.data.list;
      this.total = res.data.count;
    },
    async goToCreate(userId) {
      this.$router.push({
        path: "/admin/createUser",
        query: {
          _id: userId
        }
      });
    },
    open_dialog: function (id, name) {
      this.delete_dialog = true;
      this.selectUserName = name;
      this.selectUserId = id;
    },
    cancel_dialog: function () {
      if (this.delete_dialog) return this.delete_dialog = false;
    },
    async deleteUser(userId) {
      let res = await user_namespaceObject.deleteUser(userId);
      if (res.status != 200) {
        this.cancel_dialog();
        return this.showPopup("操作失败:" + res.data.message, "popup-error");
      }
      this.cancel_dialog();
      this.showPopup("操作成功:" + res.data.message, "popup-success");
      this.getUserList();
    },
    showPopup(msg, type) {
      this.popup_message = msg;
      this.popup_class = type;
      this.topPopup = true;
    }
  },
  watch: {
    topPopup(val) {
      if (val) {
        setTimeout(() => {
          this.topPopup = false;
        }, 2000);
      }
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-685ca9e3","hasScoped":false,"transformToRequire":{"video":"src","source":"src","img":"src","image":"xlink:href"},"buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/views/admin/userList.vue
var userList_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"userlist adm-common"},[_c('h3',{staticClass:"title"},[_vm._v("人员列表")]),_vm._v(" "),_c('mu-table',{attrs:{"showCheckbox":false}},[_c('mu-thead',{attrs:{"slot":"header"},slot:"header"},[_c('mu-tr',[_c('mu-th',{attrs:{"tooltip":"ID"}},[_vm._v("序号")]),_vm._v(" "),_c('mu-th',{attrs:{"tooltip":"名称"}},[_vm._v("名字")]),_vm._v(" "),_c('mu-th',{attrs:{"tooltip":"状态"}},[_vm._v("Email")]),_vm._v(" "),_c('mu-th',{attrs:{"tooltip":"状态"}},[_vm._v("角色")]),_vm._v(" "),_c('mu-th',{attrs:{"tooltip":"状态"}},[_vm._v("部门")]),_vm._v(" "),_c('mu-th',{attrs:{"tooltip":"状态"}},[_vm._v("创建时间")]),_vm._v(" "),_c('mu-th',{attrs:{"tooltip":"状态"}},[_vm._v("操作")])],1)],1),_vm._v(" "),_c('mu-tbody',_vm._l((_vm.userList),function(item,index){return _c('mu-tr',{key:item._id},[_c('mu-td',[_vm._v(_vm._s(index + 1))]),_vm._v(" "),_c('mu-td',[_vm._v(_vm._s(item.name))]),_vm._v(" "),_c('mu-td',[_vm._v(_vm._s(item.email))]),_vm._v(" "),_c('mu-td',[_vm._v(_vm._s(item.role.name))]),_vm._v(" "),_c('mu-td',[_vm._v(_vm._s(item.department))]),_vm._v(" "),_c('mu-td',[_vm._v(_vm._s(_vm._f("timeFilter")(item.createAt)))]),_vm._v(" "),_c('mu-td',[_c('mu-chip',{attrs:{"backgroundColor":"#2196f3","color":"#fff"},on:{"click":function($event){_vm.goToCreate(item._id)}}},[_vm._v("\n            修改\n          ")]),_vm._v(" "),_c('mu-chip',{attrs:{"backgroundColor":"#ff4081","color":"#fff"},on:{"click":function($event){_vm.open_dialog(item._id,item.name)}}},[_vm._v("\n            删除\n          ")]),_vm._v(" "),_c('mu-dialog',{attrs:{"open":_vm.delete_dialog,"title":"删除确认"},on:{"close":_vm.cancel_dialog}},[_vm._v("\n      确定删除用户:"+_vm._s(_vm.selectUserName)+"？\n      "),_c('mu-flat-button',{attrs:{"slot":"actions","primary":"","label":"取消"},on:{"click":_vm.cancel_dialog},slot:"actions"}),_vm._v(" "),_c('mu-flat-button',{attrs:{"slot":"actions","primary":"","label":"确定"},on:{"click":function($event){_vm.deleteUser(_vm.selectUserId)}},slot:"actions"})],1)],1)],1)}))],1),_vm._v(" "),_c('div',{staticClass:"pagebox"},[_c('mu-pagination',{staticClass:"page",attrs:{"total":_vm.total,"showSizeChanger":true,"current":_vm.current,"pageSizeOption":_vm.pageSizeOption},on:{"pageSizeChange":_vm.changePageSize,"pageChange":_vm.changePage}})],1),_vm._v(" "),_c('mu-popup',{attrs:{"position":"top","overlay":false,"popupClass":['popup-default',_vm.popup_class],"open":_vm.topPopup}},[_vm._v("\n      "+_vm._s(_vm.popup_message)+"\n  ")])],1)}
var userList_staticRenderFns = []
var userList_esExports = { render: userList_render, staticRenderFns: userList_staticRenderFns }
/* harmony default export */ var admin_userList = (userList_esExports);
// CONCATENATED MODULE: ./src/views/admin/userList.vue
var userList_normalizeComponent = __webpack_require__("VU/8")
/* script */


/* template */

/* template functional */
var userList___vue_template_functional__ = false
/* styles */
var userList___vue_styles__ = null
/* scopeId */
var userList___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var userList___vue_module_identifier__ = null
var userList_Component = userList_normalizeComponent(
  userList,
  admin_userList,
  userList___vue_template_functional__,
  userList___vue_styles__,
  userList___vue_scopeId__,
  userList___vue_module_identifier__
)

/* harmony default export */ var views_admin_userList = (userList_Component.exports);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/views/admin/createUser.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ var createUser = ({
  data() {

    return {
      roleList: [],
      role: '',
      name: '',
      email: '',
      password: '',
      department: '',
      isUpdate: false,
      topPopup: false,
      popup_message: "",
      popup_class: '',
      userId: ''
    };
  },
  computed: {},
  async mounted() {
    await this.getRoleList();
    this.userId = this.$route.query._id;
    if (this.userId) {
      this.isUpdate = true;
      await this.getUser();
    }
  },
  methods: {
    async getRoleList() {
      let res = await role_namespaceObject.listRole();
      this.roleList = res.data.list;
    },
    async getUser() {
      let res = await user_namespaceObject.getUser(this.userId);
      let user = res.data;
      this.name = user.name, this.password = user.password, this.role = user.role._id, this.email = user.email, this.department = user.department;
    },
    async submit() {
      if (!this.name || this.name === '') return this.showPopup('名字不能为空', 'popup-error');
      if (!this.password || this.password === '') return this.showPopup('密码不能为空', 'popup-error');
      if (!this.role || this.role === '') return this.showPopup('未选择角色', 'popup-error');
      let body = {
        name: this.name,
        password: this.password,
        role: this.role,
        department: this.department,
        email: this.email
      };
      if (this.isUpdate) {
        let res = await user_namespaceObject.updateUser(this.userId, body);
        if (res.status !== 200) {
          return this.showPopup('操作失败：' + res.data.message, 'popup-error');
        }
        localStorage.setItem('msg', '操作成功：' + res.data.message);
        return this.$router.push({
          path: "/admin/userList"
        });
      }
      let res = await user_namespaceObject.addUser(body);
      if (res.status !== 201) {
        return this.showPopup('操作失败：' + res.data.message, 'popup-error');
      }
      localStorage.setItem('msg', '操作成功：' + res.data.message);
      return this.$router.push({
        path: "/admin/userList"
      });
    },
    showPopup(msg, type) {
      this.popup_message = msg;
      this.popup_class = type;
      this.topPopup = true;
    },
    cancelSubmit() {
      return this.$router.push({
        path: "/admin/userList"
      });
    }

  },
  watch: {
    topPopup(val) {
      if (val) {
        setTimeout(() => {
          this.topPopup = false;
        }, 1000);
      }
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-91be3a08","hasScoped":false,"transformToRequire":{"video":"src","source":"src","img":"src","image":"xlink:href"},"buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/views/admin/createUser.vue
var createUser_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"adm-common"},[_c('h3',{staticClass:"title"},[_vm._v("添加人员")]),_vm._v(" "),_c('div',{staticClass:"createtask-form createuser-form"},[_c('ul',[_c('li',[_c('label',{staticClass:"labelbox"},[_vm._v("名字")]),_vm._v(" "),_c('mu-text-field',{attrs:{"hintText":"请输入用户名"},model:{value:(_vm.name),callback:function ($$v) {_vm.name=$$v},expression:"name"}})],1),_vm._v(" "),_c('li',[_c('label',{staticClass:"labelbox"},[_vm._v("E-mail")]),_vm._v(" "),_c('mu-text-field',{attrs:{"hintText":"请输入E-mail"},model:{value:(_vm.email),callback:function ($$v) {_vm.email=$$v},expression:"email"}})],1),_vm._v(" "),_c('li',[_c('label',{staticClass:"labelbox"},[_vm._v("设置密码")]),_vm._v(" "),_c('mu-text-field',{attrs:{"hintText":"请输入设置密码"},model:{value:(_vm.password),callback:function ($$v) {_vm.password=$$v},expression:"password"}})],1),_vm._v(" "),_c('li',[_c('label',{staticClass:"labelbox"},[_vm._v("所属部门")]),_vm._v(" "),_c('mu-text-field',{attrs:{"hintText":"请输入所属部门"},model:{value:(_vm.department),callback:function ($$v) {_vm.department=$$v},expression:"department"}})],1),_vm._v(" "),_c('li',[_c('label',{staticClass:"labelbox labelrole"},[_vm._v("角色选择")]),_vm._v(" "),_c('mu-select-field',{attrs:{"hintText":"选择角色选择","maxHeight":300},model:{value:(_vm.role),callback:function ($$v) {_vm.role=$$v},expression:"role"}},_vm._l((_vm.roleList),function(item){return _c('mu-menu-item',{key:item._id,attrs:{"title":item.name,"value":item._id}})}))],1),_vm._v(" "),_c('li',[_c('mu-raised-button',{staticClass:"createtask-btn",attrs:{"label":_vm.isUpdate?'更新':'添加',"primary":""},on:{"click":_vm.submit}}),_vm._v(" "),_c('mu-raised-button',{staticClass:"createtask-btn",attrs:{"label":"取消","primary":""},on:{"click":_vm.cancelSubmit}})],1)]),_vm._v(" "),_c('br'),_vm._v(" "),_c('mu-popup',{attrs:{"position":"top","overlay":false,"popupClass":['popup-default',_vm.popup_class],"open":_vm.topPopup}},[_vm._v("\n      "+_vm._s(_vm.popup_message)+"\n  ")])],1)])}
var createUser_staticRenderFns = []
var createUser_esExports = { render: createUser_render, staticRenderFns: createUser_staticRenderFns }
/* harmony default export */ var admin_createUser = (createUser_esExports);
// CONCATENATED MODULE: ./src/views/admin/createUser.vue
var createUser_normalizeComponent = __webpack_require__("VU/8")
/* script */


/* template */

/* template functional */
var createUser___vue_template_functional__ = false
/* styles */
var createUser___vue_styles__ = null
/* scopeId */
var createUser___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var createUser___vue_module_identifier__ = null
var createUser_Component = createUser_normalizeComponent(
  createUser,
  admin_createUser,
  createUser___vue_template_functional__,
  createUser___vue_styles__,
  createUser___vue_scopeId__,
  createUser___vue_module_identifier__
)

/* harmony default export */ var views_admin_createUser = (createUser_Component.exports);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/views/admin/projectList.vue
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ var admin_projectList = ({
  components: {
    createProject: src_views_createProject
  },
  data() {
    return {
      flag: false,
      total: 10,
      current: 1,
      pageSize: 2,
      pageSizeOption: [5, 10, 15, 20],
      projectList: [],
      delete_dialog: false,
      selectProjectName: "",
      selectProjectId: "",
      topPopup: false,
      popup_message: "",
      popup_class: ""

    };
  },
  computed: {},
  mounted() {
    this.getProjectList();
  },
  methods: {
    projectUpdate: function () {
      console.log(1234);
      this.getProjectList();
    },
    sub_popup: function (msg) {
      this.popup_message = msg.popup_message;
      this.popup_class = msg.popup_class;
      this.topPopup = msg.topPopup;
    },
    openCreateProject: function (id, name, desc) {
      this.flag = true;
      let body = {
        _id: id,
        name: name,
        desc: desc
      };
      localStorage.setItem('openUpdateProject', JSON.stringify(body));
    },
    closeCreateProject: function (data) {
      this.flag = data;
    },
    open_dialog: function (id, name) {
      this.delete_dialog = true;
      this.selectProjectName = name;
      this.selectProjectId = id;
    },
    cancel_dialog: function () {
      if (this.delete_dialog) return this.delete_dialog = false;
    },
    async changePage(newPage) {
      this.current = newPage;
      let query = {
        size: this.pageSize,
        page: this.current
      };
      let res = await project_namespaceObject.listProject(query);
      this.projectList = res.data.list;
      this.total = res.data.count;
    },
    async changePageSize(newSize) {
      this.pageSize = newSize;
      let query = {
        size: this.pageSize,
        page: this.current
      };
      let res = await project_namespaceObject.listProject(query);
      this.projectList = res.data.list;
      this.total = res.data.count;
    },
    async getProjectList() {
      let query = {
        size: this.pageSize,
        page: this.current
      };
      let res = await project_namespaceObject.listProject();
      this.projectList = res.data.list;
      this.total = res.data.count;
    },
    toTaskList(id) {
      this.$router.push({
        path: '/taskList',
        query: {
          project: id,
          label: ''
        }
      });
    },
    async deleteProject(projectId) {
      let res = await project_namespaceObject.deleteProject(projectId);
      if (res.status != 200) {
        this.cancel_dialog();
        return this.showPopup("操作失败:" + res.data.message, "popup-error");
      }
      this.cancel_dialog();
      this.showPopup("操作成功:" + res.data.message, "popup-success");
      this.getProjectList();
    },
    showPopup(msg, type) {
      this.popup_message = msg;
      this.popup_class = type;
      this.topPopup = true;
    }

  },
  watch: {
    topPopup(val) {
      if (val) {
        setTimeout(() => {
          this.topPopup = false;
        }, 1000);
      }
    }
  }
});
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/template-compiler?{"id":"data-v-6c2aa654","hasScoped":false,"transformToRequire":{"video":"src","source":"src","img":"src","image":"xlink:href"},"buble":{"transforms":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./src/views/admin/projectList.vue
var admin_projectList_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"userlist adm-common"},[_c('h3',{staticClass:"title"},[_vm._v("项目列表")]),_vm._v(" "),_c('mu-table',{attrs:{"showCheckbox":false}},[_c('mu-thead',{attrs:{"slot":"header"},slot:"header"},[_c('mu-tr',[_c('mu-th',{attrs:{"tooltip":"ID"}},[_vm._v("序号")]),_vm._v(" "),_c('mu-th',{attrs:{"tooltip":"添加时间"}},[_vm._v("创建时间")]),_vm._v(" "),_c('mu-th',{attrs:{"tooltip":"添加时间"}},[_vm._v("创建人")]),_vm._v(" "),_c('mu-th',{attrs:{"tooltip":"任务主题"}},[_vm._v("项目名称")]),_vm._v(" "),_c('mu-th',{attrs:{"tooltip":"状态"}},[_vm._v("项目描述")]),_vm._v(" "),_c('mu-th',{attrs:{"tooltip":"操作"}},[_vm._v("操作")])],1)],1),_vm._v(" "),_c('mu-tbody',_vm._l((_vm.projectList),function(item,index){return _c('mu-tr',{key:item._id},[_c('mu-td',[_vm._v(_vm._s(index + 1))]),_vm._v(" "),_c('mu-td',[_vm._v(_vm._s(_vm._f("timeFilter")(item.createAt)))]),_vm._v(" "),_c('mu-td',[_vm._v(_vm._s(item.creator.name))]),_vm._v(" "),_c('mu-td',[_vm._v(_vm._s(item.name))]),_vm._v(" "),_c('mu-td',[_vm._v(_vm._s(item.desc))]),_vm._v(" "),_c('mu-td',[_c('mu-chip',{attrs:{"backgroundColor":"#2196f3","color":"#fff"},on:{"click":function($event){_vm.openCreateProject(item._id,item.name,item.desc)}}},[_vm._v("\n            修改\n          ")]),_vm._v(" "),_c('mu-chip',{attrs:{"backgroundColor":"#2196f3","color":"#fff"},on:{"click":function($event){_vm.toTaskList(item._id)}}},[_vm._v("\n            查看\n          ")]),_vm._v(" "),_c('mu-chip',{attrs:{"backgroundColor":"#ff4081","color":"#fff"},on:{"click":function($event){_vm.open_dialog(item._id,item.name)}}},[_vm._v("\n            删除\n          ")]),_vm._v(" "),_c('mu-dialog',{attrs:{"open":_vm.delete_dialog,"title":"删除确认"},on:{"close":_vm.cancel_dialog}},[_vm._v("\n      确定删除项目:"+_vm._s(_vm.selectProjectName)+"？\n      "),_c('mu-flat-button',{attrs:{"slot":"actions","primary":"","label":"取消"},on:{"click":_vm.cancel_dialog},slot:"actions"}),_vm._v(" "),_c('mu-flat-button',{attrs:{"slot":"actions","primary":"","label":"确定"},on:{"click":function($event){_vm.deleteProject(_vm.selectProjectId)}},slot:"actions"})],1)],1)],1)}))],1),_vm._v(" "),_c('div',{staticClass:"pagebox"},[_c('mu-pagination',{staticClass:"page",attrs:{"total":_vm.total,"showSizeChanger":true,"current":_vm.current,"pageSizeOption":_vm.pageSizeOption},on:{"pageSizeChange":_vm.changePageSize,"pageChange":_vm.changePage}})],1),_vm._v(" "),_c('mu-popup',{attrs:{"position":"top","overlay":false,"popupClass":['popup-default',_vm.popup_class],"open":_vm.topPopup}},[_vm._v("\n      "+_vm._s(_vm.popup_message)+"\n  ")]),_vm._v(" "),(_vm.flag)?_c('create-project',{on:{"projectUpdate":_vm.projectUpdate,"popup":_vm.sub_popup,"closeCreatePro":_vm.closeCreateProject}}):_vm._e()],1)}
var admin_projectList_staticRenderFns = []
var admin_projectList_esExports = { render: admin_projectList_render, staticRenderFns: admin_projectList_staticRenderFns }
/* harmony default export */ var views_admin_projectList = (admin_projectList_esExports);
// CONCATENATED MODULE: ./src/views/admin/projectList.vue
var admin_projectList_normalizeComponent = __webpack_require__("VU/8")
/* script */


/* template */

/* template functional */
var admin_projectList___vue_template_functional__ = false
/* styles */
var admin_projectList___vue_styles__ = null
/* scopeId */
var admin_projectList___vue_scopeId__ = null
/* moduleIdentifier (server only) */
var admin_projectList___vue_module_identifier__ = null
var admin_projectList_Component = admin_projectList_normalizeComponent(
  admin_projectList,
  views_admin_projectList,
  admin_projectList___vue_template_functional__,
  admin_projectList___vue_styles__,
  admin_projectList___vue_scopeId__,
  admin_projectList___vue_module_identifier__
)

/* harmony default export */ var src_views_admin_projectList = (admin_projectList_Component.exports);

// CONCATENATED MODULE: ./src/router/index.js




// import CreateProject from '@/views/createProject'











vue_esm["default"].use(vue_router_esm["a" /* default */]);

const router = new vue_router_esm["a" /* default */]({
  routes: [{
    path: '/',
    redirect: '/login',
    component: src_views_login
  }, {
    path: '/login',
    component: src_views_login,
    name: 'login'
  },
  // {
  //   path:'/',
  //   component:Layout,
  //   redirect: '/login',
  //   name:'login',
  //   children:[
  //     { path: '/login', component: Login }
  //   ]      
  // },
  {
    path: '/index',
    name: 'index',
    title: '首页',
    component: src_components_layout,
    // redirect: '/projectList',
    children: [{
      path: '/index',
      name: 'projectList',
      title: '首页',
      component: src_views_projectList
    }]
  },
  // {
  //   path: '/createProject',
  //   // redirect: '/createProject',
  //   name:'createProject',
  //   title: '创建项目',
  //   component: Layout,
  //   children: [{
  //      path: 'createProject', 
  //      name: 'createProject',
  //      title: '创建项目',
  //      component: CreateProject }]
  // },
  {
    path: '/taskList',

    name: 'taskList',
    title: '任务详情',
    component: src_components_layout,
    // redirect: '/taskList',
    children: [{
      path: '/taskList',
      name: 'taskList',
      title: '任务详情',
      component: src_views_taskList,
      // redirect: '/taskContent',
      children: [{
        path: '/taskContent',
        name: 'taskContent',
        title: '任务详情',
        component: src_views_taskContent
      }, {
        path: '/createTask',
        name: 'createTask',
        title: '创建任务',
        component: src_views_createTask
      }]
    }]
  }, {
    path: '/admin',
    name: 'admin',
    title: '人员列表',
    component: src_components_layout,
    redirect: '/admin/userList',
    children: [{
      path: '/admin/userList',
      name: '/admin/userList',
      title: '人员列表',
      component: views_admin_admLayout,
      // redirect: '/admin/userList',
      children: [{
        path: '/admin/userList',
        name: '/admin/userList',
        title: '用户列表',
        component: views_admin_userList
      }, {
        path: '/admin/createUser',
        name: '/admin/createUser',
        title: '创建用户',
        component: views_admin_createUser
      }, {
        path: '/admin/projectList',
        name: '/admin/projectList',
        title: '管理员项目列表',
        component: src_views_admin_projectList
      }]
    }]
  }]
});
router.beforeEach(async (to, from, next) => {
  if (to.path === '/login') return next();
  if (to.path.indexOf('/admin') != -1) {
    let res = await internal_namespaceObject.isRootAdmin();
    if (res.data.isRootAdmin) return next();
    return next({
      path: from.path,
      query: from.path
    });
  }
  await internal_namespaceObject.isLogin();
  next();
});

/* harmony default export */ var src_router = (router);
// EXTERNAL MODULE: ./node_modules/muse-ui/dist/muse-ui.js
var muse_ui = __webpack_require__("u64Q");
var muse_ui_default = /*#__PURE__*/__webpack_require__.n(muse_ui);

// EXTERNAL MODULE: ./node_modules/muse-ui/dist/muse-ui.css
var dist_muse_ui = __webpack_require__("E51W");
var dist_muse_ui_default = /*#__PURE__*/__webpack_require__.n(dist_muse_ui);

// EXTERNAL MODULE: ./node_modules/muse-ui/dist/theme-light.css
var theme_light = __webpack_require__("kVx/");
var theme_light_default = /*#__PURE__*/__webpack_require__.n(theme_light);

// EXTERNAL MODULE: ./src/assets/css/styles.css
var styles = __webpack_require__("IckW");
var styles_default = /*#__PURE__*/__webpack_require__.n(styles);

// CONCATENATED MODULE: ./src/assets/js/timeFilter.js
function timeFilter(time_str) {
    Date.prototype.pattern = function (fmt) {
        var o = {
            "M+": this.getMonth() + 1, //月份         
            "d+": this.getDate(), //日         
            "h+": this.getHours() % 12 == 0 ? 12 : this.getHours() % 12, //小时         
            "H+": this.getHours(), //小时         
            "m+": this.getMinutes(), //分         
            "s+": this.getSeconds(), //秒         
            "q+": Math.floor((this.getMonth() + 3) / 3), //季度         
            "S": this.getMilliseconds() //毫秒         
        };
        var week = {
            "0": "/u65e5",
            "1": "/u4e00",
            "2": "/u4e8c",
            "3": "/u4e09",
            "4": "/u56db",
            "5": "/u4e94",
            "6": "/u516d"
        };
        if (/(y+)/.test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
        }
        if (/(E+)/.test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length > 1 ? RegExp.$1.length > 2 ? "/u661f/u671f" : "/u5468" : "") + week[this.getDay() + ""]);
        }
        for (var k in o) {
            if (new RegExp("(" + k + ")").test(fmt)) {
                fmt = fmt.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
            }
        }
        return fmt;
    };

    try {
        let date = new Date(time_str);
        if (date == 'Invalid Date') throw new Error('Invalid Date');
        return date.pattern('yyyy-MM-dd HH:mm:ss');;
    } catch (error) {
        return "--";
    }
}
// CONCATENATED MODULE: ./src/main.js
// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.








vue_esm["default"].use(muse_ui_default.a);
vue_esm["default"].filter('timeFilter', timeFilter);
vue_esm["default"].config.productionTip = false;

/* eslint-disable no-new */
new vue_esm["default"]({
  el: '#app',
  router: src_router,
  template: '<App/>',
  components: { App: src_App }
});

/***/ }),

/***/ "SSNF":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "Udrb":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "iQH9":
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAFM0lEQVR4Xu1bUXITORBVW/5fOAHhBDBl+XvDDeAEm5wAcwKyJwBOgHOChROQ/R65JjnBek+wzr/HvfVcEqWMPXhG0+1AJVNFUYVnJPVTq/v1a0HmgT/0wO03jwBoe0BVVSfM/CxnHiK6KYpilfNt12/UPKCqqpd1XX8wxpx2XUzLe3Nr7TstIFQACMZ/M8Y8GWj89nNmvh6Px680QFABwHsP44fu/B3smPnP6XR6IQFoOoY4AFVVPanr+j/phRpjls6559LjigNQluUpEcEDxB/nnPh6xQdU9IBb55xITFE9Ahi8LMsrIvpd2AUunXNnwmPqEKGQBa6MMb8JLfjWWnvyy2QBGB1A+GKMySJBEThm/ns8Hp8VRbEUAvPOMOIxoLlIALHZbPDnBL8REf7+Y58xMNYYA8/Be9fW2mVRFNcahscx1QGIEyE4bjabt8w8A0GKxo5GoyUzgzO8Dkfmylp7rrXjTTCPAkBIjZ+NMagLsMsX0+l0u9MpQHVdfwzeAf5/7pzDEVJ91AFYLBbvmRkM7paILiaTCYxsfQJYMBwBdO6cO9dEQA2AwAew63DtG2stAlmn8xy+hYe8QEyw1r7RyADbWKOBLgxYr9ffiOilMebSWjvLMcB7P8eR+KWKoZD+/sJ5N8Z8cs4h6GU/i8Vixswoq1fWWlSEnbyo64SiHpCWwUR0PplMsIODn8ViccbMOE7iIIgBoGV8RE8LBBEAtI3XBGEwAInxxMyvm/l9sP83BpD2hEEANKSvN8cgLsBDEoRsAEKu/ge0VjLgdfWYBISltbbISbODeEBZlhXy/H0Yn8SEbYoET5hOp0VX8NL3Wj0gUFJUbdsqLn2YGbsOkoPnDqdP3gOfh4gxiM9778EksY42NWgrvgIEItrXQ0CxddkWm/YCkLhXDqjNb7L5vPceuV9EBWrz1B0ANETNHEm7LMsLInovsQNxDGZ+1fSEfQBo6Hkr59zTPsZ47yGtS4ugX51zOFLfnx0AvPfcZ6Fd3w2RuhOPD+m16jp2n/ea0vojAE30lCTt3pq+9x4RXUpVjmYePgIPPgg2qGaf49X2bnZDIwoiEovonAbjZMETUIfvEKEgbUPvvwUB2bdAkBJm/ji0OApECOvYyQhEhH+DbAYiBLF15yEiEKF5LyJ0CPFQB8DwZ/dFhVPZjYjeHRJb22waUgzhBghoMGtIVYc2wXsP2Q05PfuIYY5sAPBx0gM8KggJRR5k/GAA7gOExPgb51wsyA45TOvvgzwgjpp6gmZHJzXeWnuaqwH8kArnQpm2xKUDY6PJkt1n2Jslcg3e950GCM0mi/QlCZEjkIKBi5F1XUMEQX7O1gJifFmv159jh0naeJEg2OIJuCkWe3tfQru7143PQMSQ6qA+Zef5Qx4u7gFJYAQI23Z36O2h59+pHG50lGdSHSb1GLBvgkTZgQdcOOc+te3KkI7yoZ0WZ4J9JgzujD4h6oe9N0C89xA+4THg96KR/kdrVTsCzUnD7sYbIPgZgEDJxXUZCJ8ouv41xsyGKsl9NkcVABjNzC/CHaDtupgZ7O2OLhcWjOpxPhqNtsGSmVej0QgAqV6ZVwOgLMsPRDTobkAE5lDs6LPjzXdVAJDU8+OCtVKhOAAakloEwVr7XPr6nAYA4g2NxAvEbp18H3PI+WnJ+xqNlRhAxf/TxKMHSHuAYldH5ca4uAcAUO89CM9bSXClNQa1GBAHDjUAeMDQ7o4qO1TxgHTnkRZzPWE8Hq+6VpC5c6gDkLuwY333CMCxkP5Z5/kfShAAbvoUBesAAAAASUVORK5CYII="

/***/ }),

/***/ "kVx/":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })

},["NHnr"]);
//# sourceMappingURL=app.b2b1b50b8e5e8d77d32e.js.map